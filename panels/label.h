#pragma once
#include <string>
#include "glad/glad.h"
#include "core/panel.h"
#include "core/font.h"
#include "core/texture2d.h"
#include "glm/ext/vector_float4.hpp"

namespace glp {

class Label;
using LabelSP = std::shared_ptr<Label>;

class Label : public Panel {

    public:
        struct Style: Panel::Style {
            FontSP      font;
            float       fontSize;
            glm::vec4   color;
            float       lineSpacing;
            int         imgX;
        };

    public:
        static LabelSP makeSP(const std::string &text, bool icon=false);
        explicit Label(const std::string& text, bool icon=false);
        ~Label() = default;

        FT_Size_Metrics fontMetrics() const;
        std::string text() const;
        void textSize(int& width, int& height) const;
        void measure(const std::string& text, int& width, int& height) const;

        void setText(const std::string& text, int imgx = 0);
        void setFont(FontSP font);
        void setColor(const glm::vec3& color, float alpha=1.0f);
        void setColor(const glm::vec4& color);
        void setFontSize(float size);
        void setCoords(std::vector<Font::Coord>* coords);
        void render(const RenderInfo& ri) override;

    private:
        Style mStyle;
        std::string mText;
        FT_Size_Metrics mMetrics;
        Texture2DSP mTexture;
        std::vector<Font::Coord>* mCoords;
};


} // namespace

