#include <iostream>
#include "line_graph.h"
#include "core/array_float.h"
#include "core/colors.h"
#include "core/error.h"
#include "glm/geometric.hpp"

namespace glp {

/**
 * Creates and returns shared pointer to a new LineGraph panel
 * @param width initial width in pixels
 * @param height  initial height in pixels
 * @return shared pointer to new LineGraph panel
 */
LineGraphSP LineGraph::makeSP(int width, int height) {

    return std::make_shared<LineGraph>(width, height);
}

/**
 * Constructor
 * @param width initial width in pixels
 * @param height  initial height in pixels
 */
LineGraph::LineGraph(int width, int height):
    Grid(width, height),
    mLineColor{ColorBlue, 1},
    mFillColor{ColorRed, 1},
    mVScaleStart(-1),
    mVScaleStop(1) {

    setName("LineGraph:" + std::to_string(id()));

    // Creates shader program the first time a LineGraph object is created
    if (mProg.handle() == 0) {
        mProg.addShader(GL_VERTEX_SHADER, vertexShader);
        mProg.addShader(GL_FRAGMENT_SHADER, fragShader);
        auto res = mProg.build();
        if (res) {
            std::cout << res << "\n";
            std::cout << mProg.log();
            throw std::runtime_error("Error compiling LineGraph shader program");
        }

        // Get uniform locations
        mLocModelMatrix = mProg.getUniformLocation("ModelMatrix");
        if (mLocModelMatrix < 0) {
            throw std::runtime_error("Error getting 'ModelMatrix' uniform location");
        }
        mLocColor = mProg.getUniformLocation("Color");
        if (mLocColor < 0) {
            throw std::runtime_error("Error getting 'Color' uniform location");
        }
    }

    // Creates geometry for graph lines
    auto vboLines = new Vbo();
    vboLines->addAttrib("VertexPosition", 3);
    mGeomLines = Geometry::makeUP();
    mGeomLines->addVbo(vboLines);

    // Creates geometry for graph fill
    auto vboFill = new Vbo();
    vboFill->addAttrib("VertexPosition", 3);
    mGeomFill = Geometry::makeUP();
    mGeomFill->addVbo(vboFill);
}

void LineGraph::setLineColor(const glm::vec3& color, float alpha) {

    mLineColor = glm::vec4(color, alpha);
}

void LineGraph::setLineColor(const glm::vec4& color) {

    mLineColor = color;
}

void LineGraph::setFillColor(const glm::vec3& color, float alpha) {

    mFillColor = glm::vec4(color, alpha);
}

void LineGraph::setFillColor(const glm::vec4& color) {

    mFillColor = color;
}

void LineGraph::setVScale(float start, float stop) {

    if (stop < start)  {
        throw std::runtime_error("Invalid LineGraph vertical scale");
    }
    mVScaleStart = start;
    mVScaleStop = stop;
}

/**
 * Sets the data to graph.
 * The data must have at least 2 elements, otherwise it will be ignored.
 * @param data vector with data to graph
 */
void LineGraph::setData(const std::vector<float>& data) {

    if (data.size() < 2) {
        return;
    }

    // Generates graph lines using a quad for each line segment
    mBufferLines.clear();
    float x = 0;
    float z = 0;
    float deltaX = 1.0 / (data.size() - 1);
    for (size_t i = 0; i < data.size()-1; i++) {
        auto dx = deltaX;
        auto dy = data[i+1] - data[i];

        glm::vec3 normal{-dy, dx, z};
        normal = glm::normalize(normal);
        normal = normal * 0.002f * 1.0f;
        glm::vec3 p1{x, data[i], z};
        glm::vec3 p2{x+deltaX, data[i+1], z};

        mBufferLines.append(p1 + normal);
        mBufferLines.append(p2 + normal);
        mBufferLines.append(p2 - normal);

        mBufferLines.append(p1 + normal);
        mBufferLines.append(p2 - normal);
        mBufferLines.append(p1 - normal);
        x += deltaX;
    }
    mGeomLines->vboAt(0)->setBuffer(&mBufferLines);

    // Generates triangle strip to fill graph area
    // This has a problem when the lines crossed y=0.
    // To be fixed later.
    mBufferFill.clear();
    x = 0;
    z = 0;
    for (size_t i = 0; i < data.size(); i++) {
        mBufferFill.append(x, 0, z);
        mBufferFill.append(x, data[i], z);
        x += deltaX;
    }
    mGeomFill->vboAt(0)->setBuffer(&mBufferFill);
}

void LineGraph::render(const glp::RenderInfo &ri) {

    // Render base Grid panel
    Grid::render(ri);

    // Use LineGraph shader
    glUseProgram(mProg.handle());

    // Calculates horizontal scaling to fit the graph geometry in the content area.
    float hScale = (2 * contentWidth()) / ri.viewWidth;
    float vScale = (2 * contentHeight()) / ri.viewHeight;
    vScale /= (mVScaleStop - mVScaleStart);

    // Calculates x translation from pixel coordinates to NDC for the graph geometry
    // to position the graph at the left corner of the content area.
    auto leftBorders = margin().left + border().left + padding().left;
    float transX = (posAbs().x + leftBorders - float(ri.viewWidth) / 2) / (float(ri.viewWidth) / 2);

    // Calculates y translation from pixel coordinates to NDC for the graph geometry
    // to position the graph at the vertical middle of the content area
    // and consider the vertical scale translation.
    auto topBorders = margin().top + border().top + padding().top;
    float transY = -(posAbs().y + topBorders - float(ri.viewHeight) / 2) / (float(ri.viewHeight) / 2);
    transY -= (contentHeight() / ri.viewHeight) * (1 + (mVScaleStart + mVScaleStop) / (mVScaleStop - mVScaleStart));

    // Sets transformation matrix
    float mm[]{
            hScale, 0,      0, transX,
            0,      vScale, 0, transY,
            0,      0,      1, 0,
            0,      0,      0, 1
    };
    // Transfer matrix uniform
    glUniformMatrix4fv(mLocModelMatrix, 1, true, mm);

    // Set the scissor area to the content area
    glEnable(GL_SCISSOR_TEST);
    glScissor(
       GLint(posAbs().x + leftBorders),
       GLint(ri.viewHeight - (posAbs().y + topBorders + contentHeight())),
       GLsizei(contentWidth()),
       GLsizei(contentHeight())
    );

    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    // Draw graph fill area first
    if (mFillColor.a > 0) {
        mGeomFill->transfer(ri, &mProg);
        glUniform4fv(mLocColor, 1, (GLfloat *)&mFillColor);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, mGeomFill->itemsCount());
    }

    // Draw graph lines quads
    if (mLineColor.a > 0) {
        mGeomLines->transfer(ri, &mProg);
        glUniform4fv(mLocColor, 1, (GLfloat *)&mLineColor);
        glDrawArrays(GL_TRIANGLES, 0, mGeomLines->itemsCount());
    }

    glDisable(GL_SCISSOR_TEST);
    //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

//
// Initialize static members
//
Program LineGraph::mProg;
int LineGraph::mLocModelMatrix = -1;
int LineGraph::mLocColor = -1;

//
// Vertex Shader
//
const char* LineGraph::vertexShader = R"(
#version 330 core

// Input attributes
layout(location = 0) in  vec3  VertexPosition;

// Input uniforms
uniform mat4 ModelMatrix;

void main() {

    // Set position
    gl_Position = ModelMatrix * vec4(VertexPosition.xyz, 1);
}
)";

//
// Fragment Shader
//
const char* LineGraph::fragShader = R"(
#version 330 core

// Input uniform
uniform vec4 Color;

// Output
out vec4 FragColor;

void main() {

    FragColor = Color;
}
)";

} // namespace
