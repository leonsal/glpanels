#include <algorithm>
#include "button.h"
#include "style_mgr.h"
#include "core/gui.h"

namespace glp {

/**
 * Creates and returns shared pointer to a new Button panel
 * @param text button text
 * @param icode  optional icon code
 * @return pointer to Button panel
 */
ButtonSP Button::makeSP(const std::string &text, const char *icode) {

    return std::make_shared<Button>(text, icode);
}

/**
 * Constructor
 * @param text  button text
 * @param icode  optional icon code
 */
Button::Button(const std::string &text, const char *icode) :
    mLabel{Label::makeSP(text)},
    mFocus{false},
    mPressed{false}
    {

    // Sets default name
    setName("Button:" + std::to_string(id()));

    // Get current global style
    mStyles = StyleMgr::get()->button();

    // Appends child label and optional icon label to base panel
    appendChild(mLabel);
    if (icode != nullptr) {
        mIcon = Label::makeSP(icode, true);
        appendChild(mIcon);
    }

    update();
    recalc();
}

/**
 * Sets the text of the Button
 * @param text
 */
void Button::setText(const std::string& text) {

    mLabel->setText(text);
    recalc();
}

/**
 * Sets the icon of the Button
 * @param icode
 */
void Button::setIcon(const char* icode) {

    if (mIcon == nullptr) {
        mIcon = Label::makeSP(icode, true);
        appendChild(mIcon);
        recalc();
        return;
    }
    mIcon->setText(icode);
    recalc();
}

/**
 * Process events
 * @param ev received Event
 * @return true to stop propagation
 */
bool Button::onEvent(const Event& ev) {

    // Mouse button
    if (ev.name == OnMouseButton) {
        auto mev = dynamic_cast<const MouseButtonEvent &>(ev);
        if (mev.button != MouseButtonLeft) {
            return false;
        }
        if (mev.action == KeyPress) {
            Gui::get()->setKeyFocus(this);
            mFocus = true;
            mPressed = true;
            update();
            Event click{OnClick};
            dispatch(click);
            return true;
        }
        if (mev.action == KeyRelease) {
            mPressed = false;
            update();
            return true;
        }
        return true;
    }

    // Mouse cursor entering and leaving
    if (ev.name == OnCursorEnter || ev.name == OnCursorLeave) {
        update();
        return true;
    }

    // Remove the button key focus
    if (ev.name == OnLostKeyFocus) {
        mFocus = false;
        update();
    }
}

/**
 * Applies the specified style to this Button
 * @param st referent to Style to apply
 */
void Button::applyStyle(const Style& st) {

    Panel::setStyle(st);
    mLabel->setColor(st.color);
}

/**
 * Updates the Button style
 */
void Button::update() {

    if (!enabled()) {
        applyStyle(mStyles.disabled);
        return;
    }
    if (mPressed) {
        applyStyle(mStyles.pressed);
        return;
    }
    if (cursorOver()) {
        applyStyle(mStyles.over);
        return;
    }
    if (mFocus) {
        applyStyle(mStyles.focus);
        return;
    }
    applyStyle(mStyles.normal);
}

/**
 * Recalculate the Button dimensions and children positions
 */
void Button::recalc() {

    // Calculates the button panel minimum content area width and height
    // to fit exactly the icon/text.
    float minWidth = 0;
    float minHeight = 0;
    if (mIcon != nullptr) {
        minWidth += mIcon->width();
        minHeight += mIcon->height();
    }
    minWidth += mLabel->width();
    if (mLabel->height() > minHeight) {
        minHeight = mLabel->height();
    }
    float width = std::max(contentWidth(), minWidth);
    float height = std::max(contentHeight(), minHeight);
    setContentSize(width, height);

    // Centralize the position of the child icon and text
    float posX = (width - minWidth) / 2;
    float posY = (height - minHeight) / 2;

    // Sets the optional icon position
    if (mIcon != nullptr) {
        float iconY = (height - mIcon->height()) / 2;
        mIcon->setPos(posX, iconY);
        posX += mIcon->width();
    }
    mLabel->setPos(posX, posY);
}


} // namespace
