#include "image_panel.h"
#include "core/error.h"

namespace glp {

/**
 * Creates and returns shared pointer to a new ImagePanel
 * from the specified image file path.
 * May throw ErrorFile()
 * @param filepath string with image file path
 * @return shared pointer to Image panel
 */
ImagePanelSP ImagePanel::makeSP(const std::string &filepath) {

    return std::make_shared<ImagePanel>(filepath);
}

/**
 * Creates and returns shared pointer to a new Image panel
 * using the specified texture.
 * @param tex shared pointer to Texture2D
 * @return shared pointer to Image panel
 */
ImagePanelSP ImagePanel::makeSP(Texture2DSP tex) {

    return std::make_shared<ImagePanel>(tex);
}

/**
 * Constructor from image file path
 * Throw ErrorFile() 
 * @param imageFile string with the image filepath
 */
ImagePanel::ImagePanel(const std::string& imageFile) {

    setName("ImagePanel:" + std::to_string(id()));
    setImage(imageFile);
}

/**
 * Constructor from texture
 * This object shares this texture
 * @param tex texture shared pointer
 */
ImagePanel::ImagePanel(Texture2DSP tex) {

    setName("ImagePanel:" + std::to_string(id()));
    setImage(tex);
}

/**
 * Destructor
 */
ImagePanel::~ImagePanel() {

}

/**
 * Sets the image from the specified image file
 * Throw ErrorFile() 
 * @param imageFile string with the image filepath
 */
void ImagePanel::setImage(const std::string& imageFile) {

    Texture2DSP tex = Texture2D::make();
    const char * err = tex->setImageFromFile(imageFile.c_str());
    if (err) {
        throw ErrorFile(err);
    }
    setImage(tex);
}


/**
 * Sets the image from the specified texture
 * @param tex image texture
 */
void ImagePanel::setImage(Texture2DSP tex) {

    setImageTexture(tex);
    if (tex != nullptr) {
        setContentSize(tex->width(), tex->height());
    }
}

} // namespace
