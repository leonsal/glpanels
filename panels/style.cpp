#include "style.h"


namespace glp {

/**
 * Constructor
 * @param fontText
 * @param fontIcon
 */
Style::Style(FontSP textFont, FontSP iconFont):
    mTextFont{textFont},
    mIconFont{iconFont}{
}

} // namespace


