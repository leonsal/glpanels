#include "label.h"
#include "style_mgr.h"

namespace glp {

/**
 * Creates and returns shared pointer to a new Label
 * @param text string with text
 */
LabelSP Label::makeSP(const std::string &text, bool icon) {

    return std::make_shared<Label>(text, icon);
}

/**
 * Constructor
 * @param text string with text
 * @param icon if true use the icon font
 */
Label::Label(const std::string& text, bool icon):
    mTexture{Texture2D::make()},
    mMetrics{},
    mCoords{nullptr}
    {

    // Sets default name
    setName("Label:" + std::to_string(id()));

    // Get current global style
    mStyle = StyleMgr::get()->label();
    setStyle(mStyle);
    if (icon) {
        mStyle.font = StyleMgr::get()->iconFont();
    }

    // Sets initial text
    setTextTexture(mTexture);
    setText(text);
}

/**
 * Returns the current font metrics of this label
 * @return
 */
FT_Size_Metrics Label::fontMetrics() const {

    return mMetrics;
}

/**
 * Returns the current text of this label
 * @return
 */
std::string Label::text() const {

    return mText;
}

/**
 * Returns the current text size in pixels
 * @param width
 * @param height
 */
void Label::textSize(int& width, int& height) const {

    width = mTexture->width();
    height = mTexture->height();
}

/**
 * Measure the specified text using this label current font
 * @param text text to measure
 * @param width reference to store the measured width in pixels
 * @param height reference to store the measured height in pixels
 */
void Label::measure(const std::string& text, int& width, int& height) const {

    mStyle.font->measure(text, width, height);
}

/**
 * Sets the text of this label
 */
void Label::setText(const std::string& text, int imgx) {

    //if (text == mText) {
    //    return;
    //}
    mText = text;

    mStyle.font->setSize(mStyle.fontSize);
    mStyle.font->setLineSpacing(mStyle.lineSpacing);
    mTexture->renderText(mText, mStyle.font, imgx, mCoords);
    setContentSize(mTexture->width(), mTexture->height());
    mMetrics = mStyle.font->metrics();
}

/**
 * Sets the font of this label
 * @param font
 */
void Label::setFont(FontSP font) {

    mStyle.font = font;
    setText(mText);
}

/**
 * Sets the label text color
 * @param color
 * @param alpha
 */
void Label::setColor(const glm::vec3& color, float alpha) {

    mStyle.color = glm::vec4(color, alpha);
    setTextColor(mStyle.color);
}

/**
 * Sets the label text color
 * @param color
 */
void Label::setColor(const glm::vec4& color) {

    mStyle.color = color;
    setTextColor(mStyle.color);
}

/**
 * Sets the font size of this label
 * @param size font size in points
 * @param coords optional pointer to vector to store glyph coordinates
 */
void Label::setFontSize(float size) {

    if (mStyle.fontSize == size) {
        return;
    }
    mStyle.fontSize = size;
    setText(mText, mStyle.imgX);
}

/**
 * Sets the pointer to vector to save the glyph coordinates
 * Used mainly by the Edit panel
 * @param pointer to vector of glypy coords
 */
void Label::setCoords(std::vector<Font::Coord>* coords) {

    mCoords = coords;
}

/**
 * Render this label
 * Overrides parent class method
 */
void Label::render(const RenderInfo& ri) {

    Panel::render(ri);
}


} // namespace
