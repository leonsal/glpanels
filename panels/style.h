#pragma once
#include "core/panel.h"
#include "button.h"
#include "edit.h"
#include "label.h"

namespace glp {

class Style;
using StyleUP = std::unique_ptr<Style>;

class Style {

    public:
        Style() = delete;
        Style(FontSP textFont, FontSP iconFont);

        FontSP textFont() { return mTextFont; }
        FontSP iconFont() { return mIconFont; }
        Button::Styles button() { return mButton; };
        Edit::Styles edit() { return mEdit; };
        Label::Style label() { return mLabel; };

    protected:
        FontSP mTextFont;
        FontSP mIconFont;
        Button::Styles mButton;
        Edit::Styles mEdit;
        Label::Style mLabel;
};


} // namespace
