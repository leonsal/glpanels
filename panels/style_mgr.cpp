#include "style_mgr.h"
#include "style_light.h"
#include "assets/assets.h"
#include "assets/icodes.h"

namespace glp {

/**
 * Get the current style
 * @return pointer to current style
 */
Style* StyleMgr::get() {

    if (mCurrent == nullptr) {
        // Creates default text font
        auto font = glp::Font::make(&bin2c_def_font_ttf[0], sizeof(bin2c_def_font_ttf));
        // Creates default icon font
        auto icons = glp::Font::make(&bin2c_def_icons_ttf[0], sizeof(bin2c_def_icons_ttf));
        mCurrent = std::make_unique<StyleLight>(font, icons);
    }
    return mCurrent.get();
}

/**
 * Sets the new current style
 * The previous style is deleted
 * @param style unique pointer to style to set
 */
void StyleMgr::setStyle(StyleUP style) {

    mCurrent = std::move(style);
}

// Initialize static members
StyleUP StyleMgr::mCurrent = nullptr;

}
