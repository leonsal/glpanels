#pragma once
#include "style.h"

namespace glp {

class StyleLight : public Style {

    public:
        StyleLight() = delete;
        StyleLight(FontSP fontText, FontSP fontIcon);
};

} // namespace
