#pragma once
#include "style.h"

namespace glp {

class StyleMgr {

    public:
        static Style* get();
        static void setStyle(StyleUP style);

    protected:
        static StyleUP mCurrent;
};


} // namespace
