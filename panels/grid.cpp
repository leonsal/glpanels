#include <iostream>
#include "grid.h"
#include "core/error.h"

namespace glp {

/**
 * Creates and returns shared pointer to a new Grid panel
 * @param width initial width in pixels
 * @param height  initial height in pixels
 * @return shared pointer to new Grid panel
 */
GridSP Grid::makeSP(int width, int height) {

    return std::make_shared<Grid>(width, height);
}

/**
 * Constructor
 * @param width initial width in pixels
 * @param height  initial height in pixels
 */
Grid::Grid(int width, int height) : Panel(width, height),
    mUgrid {
       2, 2, 1, 2,      // horiz lines, vert lines, grid width, line width,
       {0, 0, 0, 1},    // grid color,
       {0, 0, 0, 0},    // horiz cursor color,
       {0, 0, 0, 0},    // vert cursor color,
       0, 0, 0, 0,      // horiz cursor pos, vert cursor pos, horiz cursor width, vert cursor width
    } {

    setName("Grid:" + std::to_string(id()));

    // Sets base panel type of operation
    setOperation(Panel::Op::Grid);

    // Get uniform locations from base panel program
    mLocGrid = prog()->getUniformLocation("Grid");
    if (mLocGrid < 0) {
        throw std::runtime_error("Error getting 'Grid' uniform location");
    }
}

/**
 * Sets the grid
 * @param horiz  Number of horizontal grid lines
 * @param vert  Number of vertical grid lines
 * @param width Grid lines width in pixels
 */
void Grid::setGrid(int horiz, int vert, int width) {

    mUgrid.mHorizGrid = horiz;
    mUgrid.mVertGrid = vert;
    mUgrid.mGridWidth = width;
}

/**
 * Sets the grid lines color from a Vec3 color and optional alpha channel
 * @param color
 * @param alpha
 */
void Grid::setGridColor(const glm::vec3& color, float alpha) {

    mUgrid.mGridColor = glm::vec4(color, alpha);
}

/**
 * Sets the grid lines color from a Vec4 color
 * @param color
 */
void Grid::setGridColor(const glm::vec4& color) {

   mUgrid.mGridColor = color;
}


void Grid::setHCursor(int pos, int width) {

    mUgrid.mHCursorPos = pos;
    mUgrid.mHCursorWidth = width;
}

void Grid::setHCursorColor(const glm::vec3& color, float alpha) {

    mUgrid.mHCursorColor = glm::vec4(color, alpha);
}

void Grid::setHCursorColor(const glm::vec4& color) {

    mUgrid.mHCursorColor = color;
}

void Grid::setVCursor(int pos, int width) {

    mUgrid.mVCursorPos = pos;
    mUgrid.mVCursorWidth = width;

}
void Grid::setVCursorColor(const glm::vec3& color, float alpha) {

    mUgrid.mVCursorColor = glm::vec4(color, alpha);
}

void Grid::setVCursorColor(const glm::vec4& color) {

    mUgrid.mVCursorColor = color;
}

void Grid::render(const glp::RenderInfo &ri) {

    // Render setup of base panel
    auto render = Panel::renderSetup(ri);
    if (!render) {
        return;
    }

    // Transfer grid uniforms
    int nvecs = (sizeof(mUgrid) / sizeof(GLfloat)) / 4;
    glUniform4fv(mLocGrid, nvecs, (GLfloat *) &mUgrid);

    // Base panel draw call
    Panel::renderDraw(ri);
}

int Grid::mLocGrid = -1;

} // namespace
