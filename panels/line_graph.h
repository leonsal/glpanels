#pragma once
#include "core/geometry.h"
#include "core/program.h"
#include "core/array_float.h"
#include "panels/grid.h"
#include "glm/ext/vector_float4.hpp"

namespace glp {

class LineGraph;
using LineGraphSP = std::shared_ptr<LineGraph>;

class LineGraph : public Grid {

    public:
        static LineGraphSP makeSP(int width, int height);
        LineGraph(int width, int height);
        ~LineGraph() = default;
        void setLineColor(const glm::vec3& color, float alpha=1.0f);
        void setLineColor(const glm::vec4& color);
        void setFillColor(const glm::vec3& color, float alpha=1.0f);
        void setFillColor(const glm::vec4& color);
        void setData(const std::vector<float>& data);
        //bool onEvent(const Event& ev) override;
        void setVScale(float start, float stop);
        void render(const RenderInfo& ri) override;

    private:
        static Program mProg;
        static int mLocModelMatrix;
        static int mLocColor;
        static const char* vertexShader;
        static const char* fragShader;
        GeometryUP mGeomLines;
        GeometryUP mGeomFill;
        ArrayFloat mBufferLines;
        ArrayFloat mBufferFill;
        glm::vec4  mLineColor;         // line graph color
        glm::vec4  mFillColor;         // line graph fill area color
        float   mVScaleStart;
        float   mVScaleStop;

};

}

