#pragma once
#include "glad/glad.h"
#include "core/geometry.h"
#include "core/program.h"
#include "core/panel.h"
#include "glm/ext/vector_float4.hpp"

namespace glp {

class Grid;
using GridSP = std::shared_ptr<Grid>;

class Grid : public Panel {

    public:
        static GridSP makeSP(int width, int height);
        Grid(int width, int height);
        ~Grid() = default;
        void setGrid(int horiz, int vert, int width=1);
        void setGridColor(const glm::vec3& color, float alpha=1.0f);
        void setGridColor(const glm::vec4& color);
        void setHCursor(int pos, int width);
        void setHCursorColor(const glm::vec3& color, float alpha=1.0f);
        void setHCursorColor(const glm::vec4& color);
        void setVCursor(int pos, int width);
        void setVCursorColor(const glm::vec3& color, float alpha=1.0f);
        void setVCursorColor(const glm::vec4& color);
        void render(const RenderInfo& ri) override;

    private:
        static int mLocGrid;
        struct {
            float       mHorizGrid;         // number of horizontal grid lines
            float       mVertGrid;          // number of vertical grid lines
            float       mGridWidth;         // grid width in pixels
            float       empty1;             // complete Vec4
            glm::vec4   mGridColor;         // grid lines color
            glm::vec4   mHCursorColor;      // horizontal cursor position in tex coordinates
            glm::vec4   mVCursorColor;      // vertical cursor color
            float       mHCursorPos;        // horizontal cursor position in tex coordinates
            float       mVCursorPos;        // vertical cursor position in tex coordinates
            float       mHCursorWidth;      // horizontal width in pixels
            float       mVCursorWidth;      // vertical width in pixels
        } mUgrid;
};

}

