#pragma once
#include <string>
#include "core/texture2d.h"
#include "core/panel.h"

namespace glp {

class ImagePanel;
using ImagePanelSP = std::shared_ptr<ImagePanel>;

class ImagePanel : public Panel {

    public:
        static ImagePanelSP makeSP(const std::string &filepath);
        static ImagePanelSP makeSP(Texture2DSP tex);
        explicit ImagePanel(const std::string& filepath);
        explicit ImagePanel(Texture2DSP tex);
        ~ImagePanel();
        void setImage(const std::string& imageFile);
        void setImage(Texture2DSP tex);
};


} // namespace gui




