#include "style_light.h"
#include "core/colors.h"

namespace glp {

/**
 * Constructor
 * @param fontText
 * @param fontIcon
 */
StyleLight::StyleLight(FontSP fontText, FontSP fontIcon):
    Style(fontText, fontIcon) {

    // Default normal colors
    glm::vec4 textColor = glm::vec4(ColorBlack, 1.0);
    glm::vec4 bgColor = glm::vec4(ColorLightgray, 1.0);
    glm::vec4 borderColor = glm::vec4(ColorBlack, 1.0);

    // Default over colors
    glm::vec4 textColorOver = glm::vec4(ColorBlack, 1.0);
    glm::vec4 bgColorOver = glm::vec4(ColorWhite, 1.0);
    glm::vec4 borderColorOver = glm::vec4(ColorBlack, 1.0);

    // Default pressed colors
    glm::vec4 textColorPressed = glm::vec4(ColorBlue, 1.0);
    glm::vec4 bgColorPressed = glm::vec4(ColorWhite, 1.0);
    glm::vec4 borderColorPressed = glm::vec4(ColorBlack, 1.0);

    // Default focus colors
    glm::vec4 textColorFocus = glm::vec4(ColorBlack, 1.0);
    glm::vec4 bgColorFocus = glm::vec4(ColorWhite, 1.0);
    glm::vec4 borderColorFocus = glm::vec4(ColorBlack, 1.0);

    // Default disabled colors
    glm::vec4 textColorDisabled = glm::vec4(ColorGray, 1.0);
    glm::vec4 bgColorDisabled = glm::vec4(ColorLightgray, 1.0);
    glm::vec4 borderColorDisabled = glm::vec4(ColorGray, 1.0);

    // Default sizes
    Border marginSizes{0, 0, 0, 0};
    Border borderSizes{1, 1, 1, 1};
    Border paddingSizes{0, 0, 0, 0};

    // Label
    mLabel.margin = marginSizes;
    mLabel.border = {0,0,0,0};
    mLabel.padding = {0,0,0,0};
    mLabel.bgColor = {0,0,0,0};
    mLabel.font = mTextFont;
    mLabel.fontSize = 14;
    mLabel.color = textColor;
    mLabel.lineSpacing = 1.0;
    mLabel.imgX = 0;

    // Button normal
    mButton.normal.margin = marginSizes;
    mButton.normal.border = borderSizes;
    mButton.normal.padding = {0, 2, 0, 0};
    mButton.normal.borderColor = borderColor;
    mButton.normal.bgColor = bgColor;
    mButton.normal.color = textColor;
    mButton.normal.iconPos = Button::Icon::Left;
    // Button over
    mButton.over = mButton.normal;
    mButton.over.borderColor = borderColorOver;
    mButton.over.bgColor = bgColorOver;
    mButton.over.color = textColorOver;
    // Button pressed
    mButton.pressed = mButton.normal;
    mButton.pressed.borderColor = borderColorPressed;
    mButton.pressed.bgColor = bgColorPressed;
    mButton.pressed.color = textColorPressed;
    // Button focus
    mButton.focus = mButton.normal;
    mButton.focus.borderColor = borderColorFocus;
    mButton.focus.bgColor = bgColorFocus;
    mButton.focus.color = textColorFocus;
    // Button disabled
    mButton.disabled = mButton.normal;
    mButton.disabled.borderColor = borderColorDisabled;
    mButton.disabled.bgColor = bgColorDisabled;
    mButton.disabled.color = textColorDisabled;

    // Edit normal
    mEdit.imgX = 2;
    mEdit.normal.margin = marginSizes;
    mEdit.normal.border = borderSizes;
    mEdit.normal.padding = paddingSizes;
    mEdit.normal.borderColor = borderColor;
    mEdit.normal.bgColor = bgColor;
    mEdit.normal.color = textColor;
    mEdit.normal.holderColor = glm::vec4(ColorDimgray, 1.0);
    // Edit focus
    mEdit.focus = mEdit.normal;
    mEdit.focus.borderColor = borderColorFocus;
    mEdit.focus.bgColor = bgColorFocus;
    mEdit.focus.color = textColorFocus;
    mEdit.focus.holderColor = glm::vec4(ColorDimgray, 1.0);
    // Edit disabled
    mEdit.disabled = mEdit.normal;
    mEdit.disabled.borderColor = borderColorDisabled;
    mEdit.disabled.bgColor = bgColorDisabled;
    mEdit.disabled.color = textColorDisabled;
    mEdit.disabled.holderColor = glm::vec4(ColorDimgray, 1.0);
}


} // namespace


