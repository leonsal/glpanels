#pragma once
#include <memory>
#include "core/panel.h"
#include "core/font.h"
#include "core/texture2d.h"
#include "core/events.h"
#include "panels/label.h"

namespace glp {

class Edit;
using EditSP = std::shared_ptr<Edit>;

class Edit : public Panel {

    public:
        struct Style : Panel::Style {
            glm::vec4 color;
            glm::vec4 holderColor;
        };
        struct Styles {
            int imgX;           // left space in pixels for caret in texture image
            Style normal;
            Style focus;
            Style disabled;
        };

    public:
        static EditSP makeSP(int width, const std::string &placeHolder);
        Edit(int width, const std::string& text);
        ~Edit() = default;

        bool onEvent(const Event& ev) override;
        void cursorBack();
        void cursorDelete();
        void cursorEnd();
        void cursorHome();
        void cursorInput(std::string s);
        void cursorLeft();
        void cursorRight();
        void setText(const std::string& text);
        void setFontSize(float size);
        void render(const RenderInfo& ri) override;

    private:
        void applyStyle(const Style& st);
        void update();
        void updateCaretCoords();
        void updateLabelPos();
        Styles mStyles;
        std::string mPlaceHolder;
        LabelSP mLabel;
        std::vector<Font::Coord> mCoords;
        size_t mCol;
        size_t mMaxLength;
        bool mFocus;


};

} // namespace

