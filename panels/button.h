#pragma once
#include <string>
#include "core/panel.h"
#include "label.h"

namespace glp {

class Button;
using ButtonSP = std::shared_ptr<Button>;

class Button : public Panel {

    public:
        enum class Icon { Left, Right, Top, Bottom };
        struct Style: Panel::Style {
            glm::vec4 color;
            Icon iconPos;
        };
        struct Styles {
            Style normal;
            Style over;
            Style focus;
            Style pressed;
            Style disabled;
        };

    public:
        static ButtonSP makeSP(const std::string& text, const char* icode=nullptr);
        explicit Button(const std::string& text, const char* icode=nullptr);
        ~Button() = default;

        void setText(const std::string& text);
        void setIcon(const char* icode);
        bool onEvent(const Event& ev) override;

    private:
        void applyStyle(const Style& st);
        void update();
        void recalc();
        Styles mStyles;
        LabelSP mLabel;
        LabelSP mIcon;
        bool mFocus;
        bool mPressed;
};


} // namespace

