#include "core/gui.h"
#include "core/log.h"
#include "core/utf8.h"
#include "utf8proc/utf8proc.h"
#include "edit.h"
#include "label.h"
#include "style_mgr.h"

namespace glp {

/**
 * Creates and returns shared pointer to a new Edit panel
 * @param width initial width in pixels
 * @param placeHolder optional place holder text
 * @return pointer to Edit panel
 */
EditSP Edit::makeSP(int width, const std::string &placeHolder) {

    return std::make_shared<Edit>(width, placeHolder);
}

/**
 * Constructor
 * @param width initial width in pixels
 * @param placeHolder optional place holder text
 * @param font font face to use
 */
Edit::Edit(int width, const std::string &placeHolder):
    mPlaceHolder{placeHolder},
    mCol{0},
    mMaxLength{80},
    mFocus{false} {

    setWidth(width);
    setName("Edit:" + std::to_string(id()));

    // Get current global style
    mStyles = StyleMgr::get()->edit();

    // Creates internal label child
    mLabel = Label::makeSP("");
    mLabel->setCoords(&mCoords);
    appendChild(mLabel);
    update();
}

/**
 * Sets the text of this edit panel
 * @param text
 */
void Edit::setText(const std::string& text) {

    mCoords.clear();
    mLabel->setText(text, mStyles.imgX);
    setContentHeight(mLabel->height());
    updateCaretCoords();
}

/**
 * Sets the font size of this edit panel
 * @param size
 */
void Edit::setFontSize(float size) {

    mCoords.clear();
    mLabel->setFontSize(size);
    setContentHeight(mLabel->height());
    updateCaretCoords();
}

/**
 * Render this edit
 * Overrides parent class method
 */
void Edit::render(const RenderInfo& ri) {

    Panel::render(ri);
}

/**
 * Applies the specified style
 * @param st
 */
void Edit::applyStyle(const Style& st) {

    Panel::setStyle(st);
    mLabel->setColor(st.color);
}

void Edit::update() {

    if (!enabled()) {
        applyStyle(mStyles.disabled);
        return;
    }
    if (mFocus) {
        applyStyle(mStyles.focus);
        return;
    }
    applyStyle(mStyles.normal);
}

/**
 * Calculates the caret coordinates and dimensions in pixels
 * and sets this information inside the label panel to be passed to the shader.
 */
void Edit::updateCaretCoords() {

    auto coords = mCoords[mCol];
    auto metrics = mLabel->fontMetrics();
    auto cy = coords.y - (metrics.descender>>6);
    auto height = (metrics.ascender >> 6) - (metrics.descender>>6);
    mLabel->setCaret(coords.x - 1, cy, 1, height);
}

void Edit::updateLabelPos() {

    float caretX = 0;
    if (mCoords.size() > 0) {
        caretX = mCoords[mCoords.size()-1].x;
    }
    auto pos = mLabel->pos();
    if (caretX > pos.x + contentWidth()) {
        float deltaX = caretX - contentWidth();
        mLabel->setPos(pos.x - deltaX, pos.y);
    }

}

/**
 * Process events
 * @param ev received Event
 * @return true to stop propagation
 */
bool Edit::onEvent(const Event& ev) {

    // Mouse button
    if (ev.name == OnMouseButton) {
        auto mev = dynamic_cast<const MouseButtonEvent&>(ev);
        if (mev.action != KeyPress || mev.button != MouseButtonLeft) {
            return false;
        }
        logDebug("Edit mouse button left");
        Gui::get()->setKeyFocus(this);
        mFocus = true;

        // Find clicked column
        //auto pos = posAbs();

        update();
        return true;
    }

    // Keys
    if (ev.name == OnKey) {
        auto kev = dynamic_cast<const KeyEvent&>(ev);
        if (kev.action != KeyPress && kev.action != KeyRepeat) {
            return false;
        }
        switch (kev.keycode) {
            case KeyEnd:
                cursorEnd();
                break;
            case KeyHome:
                cursorHome();
                break;
            case KeyLeft:
                cursorLeft();
                break;
            case KeyRight:
                cursorRight();
                break;
            case KeyBackspace:
                cursorBack();
                break;
            case KeyDelete:
                cursorDelete();
                break;
        }
        return true;
    }

    // Char
    if (ev.name == OnChar) {
        auto cev = dynamic_cast<const CharEvent&>(ev);
        cursorInput(utf8String(cev.codepoint));
        return true;
    }

    // Lost key focus
    if  (ev.name == OnLostKeyFocus) {
        mFocus = false;
        update();
        return true;
    }

    return false;
}
/**
 * Deletes the character at left of the cursor if possible
 */
void Edit::cursorBack(void) {

    if (mCol > 0) {
        auto text = mLabel->text();
        mCol--;
        utf8Remove(text, mCol);
        setText(text);
    }
}

/**
 * Deletes the character at the right of the caret if possible
 */
void Edit::cursorDelete() {

    auto text = mLabel->text();
    if (mCol < utf8Len(mLabel->text())) {
        utf8Remove(text, mCol);
        setText(text);
    }
}

/**
 * Sets the caret position at the end of the text
 */
void Edit::cursorEnd() {

    mCol = mCoords.size() - 1;
    updateCaretCoords();
}

/**
 * Sets the caret position at the start of the text
 */
void Edit::cursorHome() {

    mCol = 0;
    updateCaretCoords();
}


void Edit::cursorInput(std::string s) {

    // Checks if string can be inserted
    auto text = mLabel->text();
    if (utf8Len(text) + utf8Len(s) >= mMaxLength) {
       return;
    }
    auto lastX = mCoords[mCoords.size()-1].x;

    // Inserts input text into current text
    utf8Insert(text, mCol, s);
    mCol++;
    setText(text);

    updateLabelPos();
    //if (mLabel->contentWidth() > contentWidth()) {
    //    auto deltaX = mCoords[mCoords.size()-1].x - lastX;
    //    auto pos = mLabel->pos();
    //    mLabel->setPos(pos.x - deltaX, pos.y);
    //}
}

/**
 * Moves the caret position to the left if possible
 */
void Edit::cursorLeft() {

    if (mCol > 0) {
        mCol--;
        updateCaretCoords();
    }
}

/**
 * Moves the caret position to the right if possible
 */
void Edit::cursorRight() {

    if (mCol < mCoords.size()-1) {
        mCol++;
        updateCaretCoords();
    }
}


} // namespace

