// File generated by tools/genicodes. Do not edit.
// This file is based on the original 'codepoints' file
// from the material design icon fonts:
// https://github.com/google/material-design-icons
#pragma once
namespace glp {

const unsigned int MdMin = 0xe000;
const unsigned int MdMax = 0xeb4c;

extern const char *const Md3dRotation;
extern const char *const MdAcUnit;
extern const char *const MdAccessAlarm;
extern const char *const MdAccessAlarms;
extern const char *const MdAccessTime;
extern const char *const MdAccessibility;
extern const char *const MdAccessible;
extern const char *const MdAccountBalance;
extern const char *const MdAccountBalanceWallet;
extern const char *const MdAccountBox;
extern const char *const MdAccountCircle;
extern const char *const MdAdb;
extern const char *const MdAdd;
extern const char *const MdAddAPhoto;
extern const char *const MdAddAlarm;
extern const char *const MdAddAlert;
extern const char *const MdAddBox;
extern const char *const MdAddCircle;
extern const char *const MdAddCircleOutline;
extern const char *const MdAddLocation;
extern const char *const MdAddShoppingCart;
extern const char *const MdAddToPhotos;
extern const char *const MdAddToQueue;
extern const char *const MdAdjust;
extern const char *const MdAirlineSeatFlat;
extern const char *const MdAirlineSeatFlatAngled;
extern const char *const MdAirlineSeatIndividualSuite;
extern const char *const MdAirlineSeatLegroomExtra;
extern const char *const MdAirlineSeatLegroomNormal;
extern const char *const MdAirlineSeatLegroomReduced;
extern const char *const MdAirlineSeatReclineExtra;
extern const char *const MdAirlineSeatReclineNormal;
extern const char *const MdAirplanemodeActive;
extern const char *const MdAirplanemodeInactive;
extern const char *const MdAirplay;
extern const char *const MdAirportShuttle;
extern const char *const MdAlarm;
extern const char *const MdAlarmAdd;
extern const char *const MdAlarmOff;
extern const char *const MdAlarmOn;
extern const char *const MdAlbum;
extern const char *const MdAllInclusive;
extern const char *const MdAllOut;
extern const char *const MdAndroid;
extern const char *const MdAnnouncement;
extern const char *const MdApps;
extern const char *const MdArchive;
extern const char *const MdArrowBack;
extern const char *const MdArrowDownward;
extern const char *const MdArrowDropDown;
extern const char *const MdArrowDropDownCircle;
extern const char *const MdArrowDropUp;
extern const char *const MdArrowForward;
extern const char *const MdArrowUpward;
extern const char *const MdArtTrack;
extern const char *const MdAspectRatio;
extern const char *const MdAssessment;
extern const char *const MdAssignment;
extern const char *const MdAssignmentInd;
extern const char *const MdAssignmentLate;
extern const char *const MdAssignmentReturn;
extern const char *const MdAssignmentReturned;
extern const char *const MdAssignmentTurnedIn;
extern const char *const MdAssistant;
extern const char *const MdAssistantPhoto;
extern const char *const MdAttachFile;
extern const char *const MdAttachMoney;
extern const char *const MdAttachment;
extern const char *const MdAudiotrack;
extern const char *const MdAutorenew;
extern const char *const MdAvTimer;
extern const char *const MdBackspace;
extern const char *const MdBackup;
extern const char *const MdBatteryAlert;
extern const char *const MdBatteryChargingFull;
extern const char *const MdBatteryFull;
extern const char *const MdBatteryStd;
extern const char *const MdBatteryUnknown;
extern const char *const MdBeachAccess;
extern const char *const MdBeenhere;
extern const char *const MdBlock;
extern const char *const MdBluetooth;
extern const char *const MdBluetoothAudio;
extern const char *const MdBluetoothConnected;
extern const char *const MdBluetoothDisabled;
extern const char *const MdBluetoothSearching;
extern const char *const MdBlurCircular;
extern const char *const MdBlurLinear;
extern const char *const MdBlurOff;
extern const char *const MdBlurOn;
extern const char *const MdBook;
extern const char *const MdBookmark;
extern const char *const MdBookmarkBorder;
extern const char *const MdBorderAll;
extern const char *const MdBorderBottom;
extern const char *const MdBorderClear;
extern const char *const MdBorderColor;
extern const char *const MdBorderHorizontal;
extern const char *const MdBorderInner;
extern const char *const MdBorderLeft;
extern const char *const MdBorderOuter;
extern const char *const MdBorderRight;
extern const char *const MdBorderStyle;
extern const char *const MdBorderTop;
extern const char *const MdBorderVertical;
extern const char *const MdBrandingWatermark;
extern const char *const MdBrightness1;
extern const char *const MdBrightness2;
extern const char *const MdBrightness3;
extern const char *const MdBrightness4;
extern const char *const MdBrightness5;
extern const char *const MdBrightness6;
extern const char *const MdBrightness7;
extern const char *const MdBrightnessAuto;
extern const char *const MdBrightnessHigh;
extern const char *const MdBrightnessLow;
extern const char *const MdBrightnessMedium;
extern const char *const MdBrokenImage;
extern const char *const MdBrush;
extern const char *const MdBubbleChart;
extern const char *const MdBugReport;
extern const char *const MdBuild;
extern const char *const MdBurstMode;
extern const char *const MdBusiness;
extern const char *const MdBusinessCenter;
extern const char *const MdCached;
extern const char *const MdCake;
extern const char *const MdCall;
extern const char *const MdCallEnd;
extern const char *const MdCallMade;
extern const char *const MdCallMerge;
extern const char *const MdCallMissed;
extern const char *const MdCallMissedOutgoing;
extern const char *const MdCallReceived;
extern const char *const MdCallSplit;
extern const char *const MdCallToAction;
extern const char *const MdCamera;
extern const char *const MdCameraAlt;
extern const char *const MdCameraEnhance;
extern const char *const MdCameraFront;
extern const char *const MdCameraRear;
extern const char *const MdCameraRoll;
extern const char *const MdCancel;
extern const char *const MdCardGiftcard;
extern const char *const MdCardMembership;
extern const char *const MdCardTravel;
extern const char *const MdCasino;
extern const char *const MdCast;
extern const char *const MdCastConnected;
extern const char *const MdCenterFocusStrong;
extern const char *const MdCenterFocusWeak;
extern const char *const MdChangeHistory;
extern const char *const MdChat;
extern const char *const MdChatBubble;
extern const char *const MdChatBubbleOutline;
extern const char *const MdCheck;
extern const char *const MdCheckBox;
extern const char *const MdCheckBoxOutlineBlank;
extern const char *const MdCheckCircle;
extern const char *const MdChevronLeft;
extern const char *const MdChevronRight;
extern const char *const MdChildCare;
extern const char *const MdChildFriendly;
extern const char *const MdChromeReaderMode;
extern const char *const MdClass;
extern const char *const MdClear;
extern const char *const MdClearAll;
extern const char *const MdClose;
extern const char *const MdClosedCaption;
extern const char *const MdCloud;
extern const char *const MdCloudCircle;
extern const char *const MdCloudDone;
extern const char *const MdCloudDownload;
extern const char *const MdCloudOff;
extern const char *const MdCloudQueue;
extern const char *const MdCloudUpload;
extern const char *const MdCode;
extern const char *const MdCollections;
extern const char *const MdCollectionsBookmark;
extern const char *const MdColorLens;
extern const char *const MdColorize;
extern const char *const MdComment;
extern const char *const MdCompare;
extern const char *const MdCompareArrows;
extern const char *const MdComputer;
extern const char *const MdConfirmationNumber;
extern const char *const MdContactMail;
extern const char *const MdContactPhone;
extern const char *const MdContacts;
extern const char *const MdContentCopy;
extern const char *const MdContentCut;
extern const char *const MdContentPaste;
extern const char *const MdControlPoint;
extern const char *const MdControlPointDuplicate;
extern const char *const MdCopyright;
extern const char *const MdCreate;
extern const char *const MdCreateNewFolder;
extern const char *const MdCreditCard;
extern const char *const MdCrop;
extern const char *const MdCrop169;
extern const char *const MdCrop32;
extern const char *const MdCrop54;
extern const char *const MdCrop75;
extern const char *const MdCropDin;
extern const char *const MdCropFree;
extern const char *const MdCropLandscape;
extern const char *const MdCropOriginal;
extern const char *const MdCropPortrait;
extern const char *const MdCropRotate;
extern const char *const MdCropSquare;
extern const char *const MdDashboard;
extern const char *const MdDataUsage;
extern const char *const MdDateRange;
extern const char *const MdDehaze;
extern const char *const MdDelete;
extern const char *const MdDeleteForever;
extern const char *const MdDeleteSweep;
extern const char *const MdDescription;
extern const char *const MdDesktopMac;
extern const char *const MdDesktopWindows;
extern const char *const MdDetails;
extern const char *const MdDeveloperBoard;
extern const char *const MdDeveloperMode;
extern const char *const MdDeviceHub;
extern const char *const MdDevices;
extern const char *const MdDevicesOther;
extern const char *const MdDialerSip;
extern const char *const MdDialpad;
extern const char *const MdDirections;
extern const char *const MdDirectionsBike;
extern const char *const MdDirectionsBoat;
extern const char *const MdDirectionsBus;
extern const char *const MdDirectionsCar;
extern const char *const MdDirectionsRailway;
extern const char *const MdDirectionsRun;
extern const char *const MdDirectionsSubway;
extern const char *const MdDirectionsTransit;
extern const char *const MdDirectionsWalk;
extern const char *const MdDiscFull;
extern const char *const MdDns;
extern const char *const MdDoNotDisturb;
extern const char *const MdDoNotDisturbAlt;
extern const char *const MdDoNotDisturbOff;
extern const char *const MdDoNotDisturbOn;
extern const char *const MdDock;
extern const char *const MdDomain;
extern const char *const MdDone;
extern const char *const MdDoneAll;
extern const char *const MdDonutLarge;
extern const char *const MdDonutSmall;
extern const char *const MdDrafts;
extern const char *const MdDragHandle;
extern const char *const MdDriveEta;
extern const char *const MdDvr;
extern const char *const MdEdit;
extern const char *const MdEditLocation;
extern const char *const MdEject;
extern const char *const MdEmail;
extern const char *const MdEnhancedEncryption;
extern const char *const MdEqualizer;
extern const char *const MdError;
extern const char *const MdErrorOutline;
extern const char *const MdEuroSymbol;
extern const char *const MdEvStation;
extern const char *const MdEvent;
extern const char *const MdEventAvailable;
extern const char *const MdEventBusy;
extern const char *const MdEventNote;
extern const char *const MdEventSeat;
extern const char *const MdExitToApp;
extern const char *const MdExpandLess;
extern const char *const MdExpandMore;
extern const char *const MdExplicit;
extern const char *const MdExplore;
extern const char *const MdExposure;
extern const char *const MdExposureNeg1;
extern const char *const MdExposureNeg2;
extern const char *const MdExposurePlus1;
extern const char *const MdExposurePlus2;
extern const char *const MdExposureZero;
extern const char *const MdExtension;
extern const char *const MdFace;
extern const char *const MdFastForward;
extern const char *const MdFastRewind;
extern const char *const MdFavorite;
extern const char *const MdFavoriteBorder;
extern const char *const MdFeaturedPlayList;
extern const char *const MdFeaturedVideo;
extern const char *const MdFeedback;
extern const char *const MdFiberDvr;
extern const char *const MdFiberManualRecord;
extern const char *const MdFiberNew;
extern const char *const MdFiberPin;
extern const char *const MdFiberSmartRecord;
extern const char *const MdFileDownload;
extern const char *const MdFileUpload;
extern const char *const MdFilter;
extern const char *const MdFilter1;
extern const char *const MdFilter2;
extern const char *const MdFilter3;
extern const char *const MdFilter4;
extern const char *const MdFilter5;
extern const char *const MdFilter6;
extern const char *const MdFilter7;
extern const char *const MdFilter8;
extern const char *const MdFilter9;
extern const char *const MdFilter9Plus;
extern const char *const MdFilterBAndW;
extern const char *const MdFilterCenterFocus;
extern const char *const MdFilterDrama;
extern const char *const MdFilterFrames;
extern const char *const MdFilterHdr;
extern const char *const MdFilterList;
extern const char *const MdFilterNone;
extern const char *const MdFilterTiltShift;
extern const char *const MdFilterVintage;
extern const char *const MdFindInPage;
extern const char *const MdFindReplace;
extern const char *const MdFingerprint;
extern const char *const MdFirstPage;
extern const char *const MdFitnessCenter;
extern const char *const MdFlag;
extern const char *const MdFlare;
extern const char *const MdFlashAuto;
extern const char *const MdFlashOff;
extern const char *const MdFlashOn;
extern const char *const MdFlight;
extern const char *const MdFlightLand;
extern const char *const MdFlightTakeoff;
extern const char *const MdFlip;
extern const char *const MdFlipToBack;
extern const char *const MdFlipToFront;
extern const char *const MdFolder;
extern const char *const MdFolderOpen;
extern const char *const MdFolderShared;
extern const char *const MdFolderSpecial;
extern const char *const MdFontDownload;
extern const char *const MdFormatAlignCenter;
extern const char *const MdFormatAlignJustify;
extern const char *const MdFormatAlignLeft;
extern const char *const MdFormatAlignRight;
extern const char *const MdFormatBold;
extern const char *const MdFormatClear;
extern const char *const MdFormatColorFill;
extern const char *const MdFormatColorReset;
extern const char *const MdFormatColorText;
extern const char *const MdFormatIndentDecrease;
extern const char *const MdFormatIndentIncrease;
extern const char *const MdFormatItalic;
extern const char *const MdFormatLineSpacing;
extern const char *const MdFormatListBulleted;
extern const char *const MdFormatListNumbered;
extern const char *const MdFormatPaint;
extern const char *const MdFormatQuote;
extern const char *const MdFormatShapes;
extern const char *const MdFormatSize;
extern const char *const MdFormatStrikethrough;
extern const char *const MdFormatTextdirectionLToR;
extern const char *const MdFormatTextdirectionRToL;
extern const char *const MdFormatUnderlined;
extern const char *const MdForum;
extern const char *const MdForward;
extern const char *const MdForward10;
extern const char *const MdForward30;
extern const char *const MdForward5;
extern const char *const MdFreeBreakfast;
extern const char *const MdFullscreen;
extern const char *const MdFullscreenExit;
extern const char *const MdFunctions;
extern const char *const MdGTranslate;
extern const char *const MdGamepad;
extern const char *const MdGames;
extern const char *const MdGavel;
extern const char *const MdGesture;
extern const char *const MdGetApp;
extern const char *const MdGif;
extern const char *const MdGolfCourse;
extern const char *const MdGpsFixed;
extern const char *const MdGpsNotFixed;
extern const char *const MdGpsOff;
extern const char *const MdGrade;
extern const char *const MdGradient;
extern const char *const MdGrain;
extern const char *const MdGraphicEq;
extern const char *const MdGridOff;
extern const char *const MdGridOn;
extern const char *const MdGroup;
extern const char *const MdGroupAdd;
extern const char *const MdGroupWork;
extern const char *const MdHd;
extern const char *const MdHdrOff;
extern const char *const MdHdrOn;
extern const char *const MdHdrStrong;
extern const char *const MdHdrWeak;
extern const char *const MdHeadset;
extern const char *const MdHeadsetMic;
extern const char *const MdHealing;
extern const char *const MdHearing;
extern const char *const MdHelp;
extern const char *const MdHelpOutline;
extern const char *const MdHighQuality;
extern const char *const MdHighlight;
extern const char *const MdHighlightOff;
extern const char *const MdHistory;
extern const char *const MdHome;
extern const char *const MdHotTub;
extern const char *const MdHotel;
extern const char *const MdHourglassEmpty;
extern const char *const MdHourglassFull;
extern const char *const MdHttp;
extern const char *const MdHttps;
extern const char *const MdImage;
extern const char *const MdImageAspectRatio;
extern const char *const MdImportContacts;
extern const char *const MdImportExport;
extern const char *const MdImportantDevices;
extern const char *const MdInbox;
extern const char *const MdIndeterminateCheckBox;
extern const char *const MdInfo;
extern const char *const MdInfoOutline;
extern const char *const MdInput;
extern const char *const MdInsertChart;
extern const char *const MdInsertComment;
extern const char *const MdInsertDriveFile;
extern const char *const MdInsertEmoticon;
extern const char *const MdInsertInvitation;
extern const char *const MdInsertLink;
extern const char *const MdInsertPhoto;
extern const char *const MdInvertColors;
extern const char *const MdInvertColorsOff;
extern const char *const MdIso;
extern const char *const MdKeyboard;
extern const char *const MdKeyboardArrowDown;
extern const char *const MdKeyboardArrowLeft;
extern const char *const MdKeyboardArrowRight;
extern const char *const MdKeyboardArrowUp;
extern const char *const MdKeyboardBackspace;
extern const char *const MdKeyboardCapslock;
extern const char *const MdKeyboardHide;
extern const char *const MdKeyboardReturn;
extern const char *const MdKeyboardTab;
extern const char *const MdKeyboardVoice;
extern const char *const MdKitchen;
extern const char *const MdLabel;
extern const char *const MdLabelOutline;
extern const char *const MdLandscape;
extern const char *const MdLanguage;
extern const char *const MdLaptop;
extern const char *const MdLaptopChromebook;
extern const char *const MdLaptopMac;
extern const char *const MdLaptopWindows;
extern const char *const MdLastPage;
extern const char *const MdLaunch;
extern const char *const MdLayers;
extern const char *const MdLayersClear;
extern const char *const MdLeakAdd;
extern const char *const MdLeakRemove;
extern const char *const MdLens;
extern const char *const MdLibraryAdd;
extern const char *const MdLibraryBooks;
extern const char *const MdLibraryMusic;
extern const char *const MdLightbulbOutline;
extern const char *const MdLineStyle;
extern const char *const MdLineWeight;
extern const char *const MdLinearScale;
extern const char *const MdLink;
extern const char *const MdLinkedCamera;
extern const char *const MdList;
extern const char *const MdLiveHelp;
extern const char *const MdLiveTv;
extern const char *const MdLocalActivity;
extern const char *const MdLocalAirport;
extern const char *const MdLocalAtm;
extern const char *const MdLocalBar;
extern const char *const MdLocalCafe;
extern const char *const MdLocalCarWash;
extern const char *const MdLocalConvenienceStore;
extern const char *const MdLocalDining;
extern const char *const MdLocalDrink;
extern const char *const MdLocalFlorist;
extern const char *const MdLocalGasStation;
extern const char *const MdLocalGroceryStore;
extern const char *const MdLocalHospital;
extern const char *const MdLocalHotel;
extern const char *const MdLocalLaundryService;
extern const char *const MdLocalLibrary;
extern const char *const MdLocalMall;
extern const char *const MdLocalMovies;
extern const char *const MdLocalOffer;
extern const char *const MdLocalParking;
extern const char *const MdLocalPharmacy;
extern const char *const MdLocalPhone;
extern const char *const MdLocalPizza;
extern const char *const MdLocalPlay;
extern const char *const MdLocalPostOffice;
extern const char *const MdLocalPrintshop;
extern const char *const MdLocalSee;
extern const char *const MdLocalShipping;
extern const char *const MdLocalTaxi;
extern const char *const MdLocationCity;
extern const char *const MdLocationDisabled;
extern const char *const MdLocationOff;
extern const char *const MdLocationOn;
extern const char *const MdLocationSearching;
extern const char *const MdLock;
extern const char *const MdLockOpen;
extern const char *const MdLockOutline;
extern const char *const MdLooks;
extern const char *const MdLooks3;
extern const char *const MdLooks4;
extern const char *const MdLooks5;
extern const char *const MdLooks6;
extern const char *const MdLooksOne;
extern const char *const MdLooksTwo;
extern const char *const MdLoop;
extern const char *const MdLoupe;
extern const char *const MdLowPriority;
extern const char *const MdLoyalty;
extern const char *const MdMail;
extern const char *const MdMailOutline;
extern const char *const MdMap;
extern const char *const MdMarkunread;
extern const char *const MdMarkunreadMailbox;
extern const char *const MdMemory;
extern const char *const MdMenu;
extern const char *const MdMergeType;
extern const char *const MdMessage;
extern const char *const MdMic;
extern const char *const MdMicNone;
extern const char *const MdMicOff;
extern const char *const MdMms;
extern const char *const MdModeComment;
extern const char *const MdModeEdit;
extern const char *const MdMonetizationOn;
extern const char *const MdMoneyOff;
extern const char *const MdMonochromePhotos;
extern const char *const MdMood;
extern const char *const MdMoodBad;
extern const char *const MdMore;
extern const char *const MdMoreHoriz;
extern const char *const MdMoreVert;
extern const char *const MdMotorcycle;
extern const char *const MdMouse;
extern const char *const MdMoveToInbox;
extern const char *const MdMovie;
extern const char *const MdMovieCreation;
extern const char *const MdMovieFilter;
extern const char *const MdMultilineChart;
extern const char *const MdMusicNote;
extern const char *const MdMusicVideo;
extern const char *const MdMyLocation;
extern const char *const MdNature;
extern const char *const MdNaturePeople;
extern const char *const MdNavigateBefore;
extern const char *const MdNavigateNext;
extern const char *const MdNavigation;
extern const char *const MdNearMe;
extern const char *const MdNetworkCell;
extern const char *const MdNetworkCheck;
extern const char *const MdNetworkLocked;
extern const char *const MdNetworkWifi;
extern const char *const MdNewReleases;
extern const char *const MdNextWeek;
extern const char *const MdNfc;
extern const char *const MdNoEncryption;
extern const char *const MdNoSim;
extern const char *const MdNotInterested;
extern const char *const MdNote;
extern const char *const MdNoteAdd;
extern const char *const MdNotifications;
extern const char *const MdNotificationsActive;
extern const char *const MdNotificationsNone;
extern const char *const MdNotificationsOff;
extern const char *const MdNotificationsPaused;
extern const char *const MdOfflinePin;
extern const char *const MdOndemandVideo;
extern const char *const MdOpacity;
extern const char *const MdOpenInBrowser;
extern const char *const MdOpenInNew;
extern const char *const MdOpenWith;
extern const char *const MdPages;
extern const char *const MdPageview;
extern const char *const MdPalette;
extern const char *const MdPanTool;
extern const char *const MdPanorama;
extern const char *const MdPanoramaFishEye;
extern const char *const MdPanoramaHorizontal;
extern const char *const MdPanoramaVertical;
extern const char *const MdPanoramaWideAngle;
extern const char *const MdPartyMode;
extern const char *const MdPause;
extern const char *const MdPauseCircleFilled;
extern const char *const MdPauseCircleOutline;
extern const char *const MdPayment;
extern const char *const MdPeople;
extern const char *const MdPeopleOutline;
extern const char *const MdPermCameraMic;
extern const char *const MdPermContactCalendar;
extern const char *const MdPermDataSetting;
extern const char *const MdPermDeviceInformation;
extern const char *const MdPermIdentity;
extern const char *const MdPermMedia;
extern const char *const MdPermPhoneMsg;
extern const char *const MdPermScanWifi;
extern const char *const MdPerson;
extern const char *const MdPersonAdd;
extern const char *const MdPersonOutline;
extern const char *const MdPersonPin;
extern const char *const MdPersonPinCircle;
extern const char *const MdPersonalVideo;
extern const char *const MdPets;
extern const char *const MdPhone;
extern const char *const MdPhoneAndroid;
extern const char *const MdPhoneBluetoothSpeaker;
extern const char *const MdPhoneForwarded;
extern const char *const MdPhoneInTalk;
extern const char *const MdPhoneIphone;
extern const char *const MdPhoneLocked;
extern const char *const MdPhoneMissed;
extern const char *const MdPhonePaused;
extern const char *const MdPhonelink;
extern const char *const MdPhonelinkErase;
extern const char *const MdPhonelinkLock;
extern const char *const MdPhonelinkOff;
extern const char *const MdPhonelinkRing;
extern const char *const MdPhonelinkSetup;
extern const char *const MdPhoto;
extern const char *const MdPhotoAlbum;
extern const char *const MdPhotoCamera;
extern const char *const MdPhotoFilter;
extern const char *const MdPhotoLibrary;
extern const char *const MdPhotoSizeSelectActual;
extern const char *const MdPhotoSizeSelectLarge;
extern const char *const MdPhotoSizeSelectSmall;
extern const char *const MdPictureAsPdf;
extern const char *const MdPictureInPicture;
extern const char *const MdPictureInPictureAlt;
extern const char *const MdPieChart;
extern const char *const MdPieChartOutlined;
extern const char *const MdPinDrop;
extern const char *const MdPlace;
extern const char *const MdPlayArrow;
extern const char *const MdPlayCircleFilled;
extern const char *const MdPlayCircleOutline;
extern const char *const MdPlayForWork;
extern const char *const MdPlaylistAdd;
extern const char *const MdPlaylistAddCheck;
extern const char *const MdPlaylistPlay;
extern const char *const MdPlusOne;
extern const char *const MdPoll;
extern const char *const MdPolymer;
extern const char *const MdPool;
extern const char *const MdPortableWifiOff;
extern const char *const MdPortrait;
extern const char *const MdPower;
extern const char *const MdPowerInput;
extern const char *const MdPowerSettingsNew;
extern const char *const MdPregnantWoman;
extern const char *const MdPresentToAll;
extern const char *const MdPrint;
extern const char *const MdPriorityHigh;
extern const char *const MdPublic;
extern const char *const MdPublish;
extern const char *const MdQueryBuilder;
extern const char *const MdQuestionAnswer;
extern const char *const MdQueue;
extern const char *const MdQueueMusic;
extern const char *const MdQueuePlayNext;
extern const char *const MdRadio;
extern const char *const MdRadioButtonChecked;
extern const char *const MdRadioButtonUnchecked;
extern const char *const MdRateReview;
extern const char *const MdReceipt;
extern const char *const MdRecentActors;
extern const char *const MdRecordVoiceOver;
extern const char *const MdRedeem;
extern const char *const MdRedo;
extern const char *const MdRefresh;
extern const char *const MdRemove;
extern const char *const MdRemoveCircle;
extern const char *const MdRemoveCircleOutline;
extern const char *const MdRemoveFromQueue;
extern const char *const MdRemoveRedEye;
extern const char *const MdRemoveShoppingCart;
extern const char *const MdReorder;
extern const char *const MdRepeat;
extern const char *const MdRepeatOne;
extern const char *const MdReplay;
extern const char *const MdReplay10;
extern const char *const MdReplay30;
extern const char *const MdReplay5;
extern const char *const MdReply;
extern const char *const MdReplyAll;
extern const char *const MdReport;
extern const char *const MdReportProblem;
extern const char *const MdRestaurant;
extern const char *const MdRestaurantMenu;
extern const char *const MdRestore;
extern const char *const MdRestorePage;
extern const char *const MdRingVolume;
extern const char *const MdRoom;
extern const char *const MdRoomService;
extern const char *const MdRotate90DegreesCcw;
extern const char *const MdRotateLeft;
extern const char *const MdRotateRight;
extern const char *const MdRoundedCorner;
extern const char *const MdRouter;
extern const char *const MdRowing;
extern const char *const MdRssFeed;
extern const char *const MdRvHookup;
extern const char *const MdSatellite;
extern const char *const MdSave;
extern const char *const MdScanner;
extern const char *const MdSchedule;
extern const char *const MdSchool;
extern const char *const MdScreenLockLandscape;
extern const char *const MdScreenLockPortrait;
extern const char *const MdScreenLockRotation;
extern const char *const MdScreenRotation;
extern const char *const MdScreenShare;
extern const char *const MdSdCard;
extern const char *const MdSdStorage;
extern const char *const MdSearch;
extern const char *const MdSecurity;
extern const char *const MdSelectAll;
extern const char *const MdSend;
extern const char *const MdSentimentDissatisfied;
extern const char *const MdSentimentNeutral;
extern const char *const MdSentimentSatisfied;
extern const char *const MdSentimentVeryDissatisfied;
extern const char *const MdSentimentVerySatisfied;
extern const char *const MdSettings;
extern const char *const MdSettingsApplications;
extern const char *const MdSettingsBackupRestore;
extern const char *const MdSettingsBluetooth;
extern const char *const MdSettingsBrightness;
extern const char *const MdSettingsCell;
extern const char *const MdSettingsEthernet;
extern const char *const MdSettingsInputAntenna;
extern const char *const MdSettingsInputComponent;
extern const char *const MdSettingsInputComposite;
extern const char *const MdSettingsInputHdmi;
extern const char *const MdSettingsInputSvideo;
extern const char *const MdSettingsOverscan;
extern const char *const MdSettingsPhone;
extern const char *const MdSettingsPower;
extern const char *const MdSettingsRemote;
extern const char *const MdSettingsSystemDaydream;
extern const char *const MdSettingsVoice;
extern const char *const MdShare;
extern const char *const MdShop;
extern const char *const MdShopTwo;
extern const char *const MdShoppingBasket;
extern const char *const MdShoppingCart;
extern const char *const MdShortText;
extern const char *const MdShowChart;
extern const char *const MdShuffle;
extern const char *const MdSignalCellular4Bar;
extern const char *const MdSignalCellularConnectedNoInternet4Bar;
extern const char *const MdSignalCellularNoSim;
extern const char *const MdSignalCellularNull;
extern const char *const MdSignalCellularOff;
extern const char *const MdSignalWifi4Bar;
extern const char *const MdSignalWifi4BarLock;
extern const char *const MdSignalWifiOff;
extern const char *const MdSimCard;
extern const char *const MdSimCardAlert;
extern const char *const MdSkipNext;
extern const char *const MdSkipPrevious;
extern const char *const MdSlideshow;
extern const char *const MdSlowMotionVideo;
extern const char *const MdSmartphone;
extern const char *const MdSmokeFree;
extern const char *const MdSmokingRooms;
extern const char *const MdSms;
extern const char *const MdSmsFailed;
extern const char *const MdSnooze;
extern const char *const MdSort;
extern const char *const MdSortByAlpha;
extern const char *const MdSpa;
extern const char *const MdSpaceBar;
extern const char *const MdSpeaker;
extern const char *const MdSpeakerGroup;
extern const char *const MdSpeakerNotes;
extern const char *const MdSpeakerNotesOff;
extern const char *const MdSpeakerPhone;
extern const char *const MdSpellcheck;
extern const char *const MdStar;
extern const char *const MdStarBorder;
extern const char *const MdStarHalf;
extern const char *const MdStars;
extern const char *const MdStayCurrentLandscape;
extern const char *const MdStayCurrentPortrait;
extern const char *const MdStayPrimaryLandscape;
extern const char *const MdStayPrimaryPortrait;
extern const char *const MdStop;
extern const char *const MdStopScreenShare;
extern const char *const MdStorage;
extern const char *const MdStore;
extern const char *const MdStoreMallDirectory;
extern const char *const MdStraighten;
extern const char *const MdStreetview;
extern const char *const MdStrikethroughS;
extern const char *const MdStyle;
extern const char *const MdSubdirectoryArrowLeft;
extern const char *const MdSubdirectoryArrowRight;
extern const char *const MdSubject;
extern const char *const MdSubscriptions;
extern const char *const MdSubtitles;
extern const char *const MdSubway;
extern const char *const MdSupervisorAccount;
extern const char *const MdSurroundSound;
extern const char *const MdSwapCalls;
extern const char *const MdSwapHoriz;
extern const char *const MdSwapVert;
extern const char *const MdSwapVerticalCircle;
extern const char *const MdSwitchCamera;
extern const char *const MdSwitchVideo;
extern const char *const MdSync;
extern const char *const MdSyncDisabled;
extern const char *const MdSyncProblem;
extern const char *const MdSystemUpdate;
extern const char *const MdSystemUpdateAlt;
extern const char *const MdTab;
extern const char *const MdTabUnselected;
extern const char *const MdTablet;
extern const char *const MdTabletAndroid;
extern const char *const MdTabletMac;
extern const char *const MdTagFaces;
extern const char *const MdTapAndPlay;
extern const char *const MdTerrain;
extern const char *const MdTextFields;
extern const char *const MdTextFormat;
extern const char *const MdTextsms;
extern const char *const MdTexture;
extern const char *const MdTheaters;
extern const char *const MdThumbDown;
extern const char *const MdThumbUp;
extern const char *const MdThumbsUpDown;
extern const char *const MdTimeToLeave;
extern const char *const MdTimelapse;
extern const char *const MdTimeline;
extern const char *const MdTimer;
extern const char *const MdTimer10;
extern const char *const MdTimer3;
extern const char *const MdTimerOff;
extern const char *const MdTitle;
extern const char *const MdToc;
extern const char *const MdToday;
extern const char *const MdToll;
extern const char *const MdTonality;
extern const char *const MdTouchApp;
extern const char *const MdToys;
extern const char *const MdTrackChanges;
extern const char *const MdTraffic;
extern const char *const MdTrain;
extern const char *const MdTram;
extern const char *const MdTransferWithinAStation;
extern const char *const MdTransform;
extern const char *const MdTranslate;
extern const char *const MdTrendingDown;
extern const char *const MdTrendingFlat;
extern const char *const MdTrendingUp;
extern const char *const MdTune;
extern const char *const MdTurnedIn;
extern const char *const MdTurnedInNot;
extern const char *const MdTv;
extern const char *const MdUnarchive;
extern const char *const MdUndo;
extern const char *const MdUnfoldLess;
extern const char *const MdUnfoldMore;
extern const char *const MdUpdate;
extern const char *const MdUsb;
extern const char *const MdVerifiedUser;
extern const char *const MdVerticalAlignBottom;
extern const char *const MdVerticalAlignCenter;
extern const char *const MdVerticalAlignTop;
extern const char *const MdVibration;
extern const char *const MdVideoCall;
extern const char *const MdVideoLabel;
extern const char *const MdVideoLibrary;
extern const char *const MdVideocam;
extern const char *const MdVideocamOff;
extern const char *const MdVideogameAsset;
extern const char *const MdViewAgenda;
extern const char *const MdViewArray;
extern const char *const MdViewCarousel;
extern const char *const MdViewColumn;
extern const char *const MdViewComfy;
extern const char *const MdViewCompact;
extern const char *const MdViewDay;
extern const char *const MdViewHeadline;
extern const char *const MdViewList;
extern const char *const MdViewModule;
extern const char *const MdViewQuilt;
extern const char *const MdViewStream;
extern const char *const MdViewWeek;
extern const char *const MdVignette;
extern const char *const MdVisibility;
extern const char *const MdVisibilityOff;
extern const char *const MdVoiceChat;
extern const char *const MdVoicemail;
extern const char *const MdVolumeDown;
extern const char *const MdVolumeMute;
extern const char *const MdVolumeOff;
extern const char *const MdVolumeUp;
extern const char *const MdVpnKey;
extern const char *const MdVpnLock;
extern const char *const MdWallpaper;
extern const char *const MdWarning;
extern const char *const MdWatch;
extern const char *const MdWatchLater;
extern const char *const MdWbAuto;
extern const char *const MdWbCloudy;
extern const char *const MdWbIncandescent;
extern const char *const MdWbIridescent;
extern const char *const MdWbSunny;
extern const char *const MdWc;
extern const char *const MdWeb;
extern const char *const MdWebAsset;
extern const char *const MdWeekend;
extern const char *const MdWhatshot;
extern const char *const MdWidgets;
extern const char *const MdWifi;
extern const char *const MdWifiLock;
extern const char *const MdWifiTethering;
extern const char *const MdWork;
extern const char *const MdWrapText;
extern const char *const MdYoutubeSearchedFor;
extern const char *const MdZoomIn;
extern const char *const MdZoomOut;
extern const char *const MdZoomOutMap;


} // namespace
