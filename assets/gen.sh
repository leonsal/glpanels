#!/bin/sh
# In Ubuntu bin2c can be installed by:
# sudo apt-get install htxtools

bin2c -C assets.c def_font.ttf def_icons.ttf
../tools/genicodes/genicodes codepoints

