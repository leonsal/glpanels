#pragma once
#include <cstddef>
#include <vector>
#include "glm/ext/vector_float2.hpp"
#include "glm/ext/vector_float3.hpp"
#include "glm/ext/vector_float4.hpp"

namespace glp {

class ArrayFloat : public std::vector<float> {

    public:
        ArrayFloat();
        ArrayFloat(size_t n);
        void append(float v);
        void append(float v0, float v1);
        void append(float v0, float v1, float v3);
        void append(float v0, float v1, float v3, float v4);
        void append(float v0, float v1, float v3, float v4, float v5);
        void append(float v0, float v1, float v3, float v4, float v5, float v6);
        void append(const glm::vec2& v);
        void append(const glm::vec3& v);
        void append(const glm::vec4& v);
        void get(size_t pos, glm::vec2& v);
        void get(size_t pos, glm::vec3& v);
        void get(size_t pos, glm::vec4& v);
        void set(size_t pos, float v0, float v1);
        void set(size_t pos, float v0, float v1, float v2);
        void set(size_t pos, float v0, float v1, float v2, float v3);
        void set(size_t pos, float v0, float v1, float v2, float v3, float v4);
        void set(size_t pos, const glm::vec2& v);
        void set(size_t pos, const glm::vec3& v);
        void set(size_t pos, const glm::vec4& v);
};

} // namespace

