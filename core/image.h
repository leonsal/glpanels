#pragma once
#include <memory>
#include "glm/ext/vector_float4.hpp"

namespace glp {

class Image;
using ImageSP = std::shared_ptr<Image>;

class Image {

    public:
        static ImageSP make(int width, int height, int channels, unsigned char* data=nullptr);
        static ImageSP fromFile(const char* filename, const char** error);
        static ImageSP fromData(const unsigned char* data, int len, const char** errmsg);

    public:
        Image() = default;
        Image(int width, int height, int channels, unsigned char* data=nullptr);
        ~Image();
        void setColor(const glm::vec4& color);
        unsigned char* data(void) const;
        unsigned char* dataMove(void);
        int width(void) const;
        int height(void) const;
        int channels(void) const;
        void drawPix(int x, int y, glm::vec4 color);

    private:
        int mWidth;
        int mHeight;
        int mChannels;
        unsigned char* mData;
};


} // namespace
