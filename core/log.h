#pragma once
#include "fmt/format.h"

namespace glp {

void log_emit(const char* format, const char* prefix, fmt::format_args args);

/**
 * Prints debug message
 */
template <typename... Args>
void logDebug(const char *format, const Args & ... args) {

#ifndef NDEBUG
    log_emit(format, "D", fmt::make_format_args(args...));
#endif
}

/**
 * Prints info message
 */
template <typename... Args>
void logInfo(const char *format, const Args & ... args) {

    log_emit(format, "I", fmt::make_format_args(args...));
}


} // namespace

