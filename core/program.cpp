#include <string>
#include <sstream>
#include "log.h"
#include "program.h"

namespace glp {

/**
 * Creates and returns unique pointer to a new Program object.
 */
ProgramUP Program::makeUP() {

    return std::make_unique<Program>();
}

/**
 * Default constructor
 */
Program::Program() noexcept:
    mHandle{0},
    mVertexHandle{0},
    mFragHandle{0},
    mGeomHandle{0} {
}

/**
 * Destructor
 */
Program::~Program() {

    logDebug("~Program({})", mHandle);
    if (mHandle != 0) {
        glDeleteProgram(mHandle);
        mHandle = 0;
    }
}

/**
 * Adds or replaces a shader in a program under construction
 * @param stype shader type
 * @param source pointer to shader source
 * @return pointer to error message or NULL
 */
const char* Program::addShader(GLenum stype, const char* source) {

    if (mHandle != 0) {
        return "program already build";
    }
    switch (stype) {
    case GL_VERTEX_SHADER:
        mVertex = source;
        break;
    case GL_FRAGMENT_SHADER:
        mFrag = source;
        break;
    case GL_GEOMETRY_SHADER:
        mGeom = source;
        break;
    default:
        return "shader type not supported";
    }
    return NULL;
}

/**
 * Builds program, compiling previously specified shaders and linking them
 * @return pointer to error message or NULL
 */
const char* Program::build(void) {

    // Checks if program is already built
    if (mHandle != 0) {
        return "Program already built";
    }

    // Checks if shaders were provided
    if (mVertex.empty()) {
        return "Vertex shader not supplied";
    }
    if (mFrag.empty()) {
        return "Fragment shader not supplied";
    }

    // Create program
    mHandle = glCreateProgram();
    if (mHandle == 0) {
        return "Error creating program";
    }

    // Compiles and attach vertex shader
    const char* err;
    err = compileShader(GL_VERTEX_SHADER, mVertex, &mVertexHandle);
    if (err != nullptr) {
        goto error_exit;
    }
    glAttachShader(mHandle, mVertexHandle);

    // Compiles and attach fragment shader
    err = compileShader(GL_FRAGMENT_SHADER, mFrag, &mFragHandle);
    if (err != nullptr) {
        goto error_exit;
    }
    glAttachShader(mHandle, mFragHandle);

    // Checks for optional geometry shader
    if (!mGeom.empty()) {
        err = compileShader(GL_GEOMETRY_SHADER, mGeom, &mGeomHandle);
        if (err != nullptr) {
            goto error_exit;
        }
    }

    // Link program and checks for errors
    glLinkProgram(mHandle);
    GLint status;
    glGetProgramiv(mHandle, GL_LINK_STATUS, &status);
    if (status == GL_FALSE) {
        GLint logLength;
        glGetProgramiv(mHandle, GL_INFO_LOG_LENGTH, &logLength);
        std::string log(logLength + 1, '\0');
        glGetProgramInfoLog(mHandle, logLength, nullptr, (GLchar*)(log.data()));
        mLog += log;
        err = "Error linking program";
        goto error_exit;
    }
    return nullptr;

error_exit:
    glDeleteProgram(mHandle);
    mHandle = 0;
    return err;
}

/**
 * Returns the location of the specified uniform or -1 if not found
 * The location is cached internally in this object
 *
 * @param name string with name of the uniform
 * @return uniform location in this program
 */
GLint Program::getUniformLocation(const std::string& name) {

    // Looks in cache
    if (mUniforms.count(name) > 0) {
        return mUniforms[name];
    }

    // Get location from OpenGL
    GLint loc = glGetUniformLocation(mHandle, name.c_str());
    if (loc < 0) {
        return loc;
    }

    // Saves location in cache
    mUniforms[name] = loc;
    return loc;
}

/**
 * Returns the program handle
 * if the returned handle is 0 the program is not valid
 *
 * @return program handle
 */
GLuint Program::handle() {

    return mHandle;
}

/**
 * Returns a copy of the current program log
 *
 * @return current program log
 */
std::string Program::log() {

    return mLog;
}

// maps OpenGL type to a string with its name
static std::map<GLenum, const char*> glTypeName = {
    {GL_FLOAT,                          "float"},
    {GL_FLOAT_VEC2,                     "vec2"},
    {GL_FLOAT_VEC3,                     "vec3"},
    {GL_FLOAT_VEC4,                     "vec4"},
    {GL_INT,                            "int"},
    {GL_INT_VEC2,                       "ivec2"},
    {GL_INT_VEC3,                       "ivec3"},
    {GL_INT_VEC4,                       "ivec4"},
    {GL_UNSIGNED_INT,                   "unsigned int"},
    {GL_UNSIGNED_INT_VEC2,              "uvec2"},
    {GL_UNSIGNED_INT_VEC3,              "uvec3"},
    {GL_UNSIGNED_INT_VEC4,              "uvec4"},
    {GL_BOOL,                           "bool"},
    {GL_BOOL_VEC2,                      "bvec2"},
    {GL_BOOL_VEC3,                      "bvec3"},
    {GL_BOOL_VEC4,                      "bvec4"},
    {GL_FLOAT_MAT2,                     "mat2"},
    {GL_FLOAT_MAT3,                     "mat3"},
    {GL_FLOAT_MAT4,                     "mat4"},
    {GL_FLOAT_MAT2x3,                   "mat2x3"},
    {GL_FLOAT_MAT2x4,                   "mat2x4"},
    {GL_FLOAT_MAT3x2,                   "mat3x2"},
    {GL_FLOAT_MAT3x4,                   "mat3x4"},
    {GL_FLOAT_MAT4x2,                   "mat4x2"},
    {GL_FLOAT_MAT4x3,                   "mat4x3"},
    {GL_SAMPLER_1D,                     "sampler1D"},
    {GL_SAMPLER_2D,                     "sampler2D"},
    {GL_SAMPLER_3D,                     "sampler3D"},
    {GL_SAMPLER_CUBE,                   "samplerCube"},
    {GL_SAMPLER_1D_SHADOW,              "sampler1DShadow"},
    {GL_SAMPLER_2D_SHADOW,              "sampler2DShadow"},
    {GL_SAMPLER_1D_ARRAY,               "sampler1DArray"},
    {GL_SAMPLER_2D_ARRAY,               "sampler2DArray"},
    {GL_SAMPLER_1D_ARRAY_SHADOW,        "sampler1DArrayShadow"},
    {GL_SAMPLER_2D_ARRAY_SHADOW,        "sampler2DArrayShadow"},
    {GL_SAMPLER_2D_MULTISAMPLE,         "sampler2DMS"},
    {GL_SAMPLER_2D_MULTISAMPLE_ARRAY,   "sampler2DMSArray"},
    {GL_SAMPLER_CUBE_SHADOW,            "samplerCubeShadow"},
    {GL_SAMPLER_BUFFER,                 "samplerBuffer"},
    {GL_SAMPLER_2D_RECT,                "sampler2DRect"},
    {GL_SAMPLER_2D_RECT_SHADOW,         "sampler2DRectShadow"},
    {GL_INT_SAMPLER_1D,                 "isampler1D"},
    {GL_INT_SAMPLER_2D,                 "isampler2D"},
    {GL_INT_SAMPLER_3D,                 "isampler3D"},
    {GL_INT_SAMPLER_CUBE,               "isamplerCube"},
    {GL_INT_SAMPLER_1D_ARRAY,           "isampler1DArray"},
    {GL_INT_SAMPLER_2D_ARRAY,           "isampler2DArray"},
    {GL_INT_SAMPLER_2D_MULTISAMPLE,                 "isampler2DMS"},
    {GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY,           "isampler2DMSArray"},
    {GL_INT_SAMPLER_BUFFER,                         "isamplerBuffer"},
    {GL_INT_SAMPLER_2D_RECT,                        "isampler2DRect"},
    {GL_UNSIGNED_INT_SAMPLER_1D,                    "usampler1D"},
    {GL_UNSIGNED_INT_SAMPLER_2D,                    "usampler2D"},
    {GL_UNSIGNED_INT_SAMPLER_3D,                    "usampler3D"},
    {GL_UNSIGNED_INT_SAMPLER_CUBE,                  "usamplerCube"},
    {GL_UNSIGNED_INT_SAMPLER_1D_ARRAY,              "usampler2DArray"},
    {GL_UNSIGNED_INT_SAMPLER_2D_ARRAY,              "usampler2DArray"},
    {GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE,        "usampler2DMS"},
    {GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY,  "usampler2DMSArray"},
    {GL_UNSIGNED_INT_SAMPLER_BUFFER,                "usamplerBuffer"},
    {GL_UNSIGNED_INT_SAMPLER_2D_RECT,               "usampler2DRect"},
};

const char* Program::compileShader(GLenum shaderType, const std::string& text, GLuint *handle) {

    // Creates shader
    *handle = glCreateShader(shaderType);
    if (*handle == 0) {
        return "Error creating shader";
    }

    // Set shader source text and compile it
    const char* source = text.data();
    glShaderSource(*handle, 1, &source, nullptr);
    glCompileShader(*handle);

    // Get the shader compiler log and if there is data
    // appends to instance log.
    GLint logLength;
    glGetShaderiv(*handle, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        std::string log(logLength + 1, '\0');
        glGetShaderInfoLog(*handle, logLength, nullptr, (GLchar*)(log.data()));
        mLog += log;
        mLog += std::string(80, '-');
        mLog += "\n";
        mLog += numberLines(text);
    }

    // Get the shader compile status
    GLint status;
    glGetShaderiv(*handle, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE) {
        switch (shaderType) {
        case GL_VERTEX_SHADER:
            return "Error compiling VERTEX SHADER";
            break;
        case GL_FRAGMENT_SHADER:
            return "Error compiling FRAGMENT SHADER";
            break;
        case GL_GEOMETRY_SHADER:
            return "Error compiling GEOMETRY SHADER";
            break;
        default:
            return "Error compiling shader";
        }
    }
    return NULL;
}

// Returns string with the original string with lines numbered
std::string Program::numberLines(const std::string& text)  {

    std::stringstream iss(text);
    std::string out;
    std::ostringstream sout(out);
    int nline = 1;
    while (iss.good()) {
        std::string line;
        getline(iss, line, '\n');
        sout << fmt::format("{}:", nline);
        sout << line << std::endl;
        nline++;
    }
    return sout.str();
}

} // namespace

