#include <cassert>
#include "log.h"
#include "utf8proc/utf8proc.h"
#include "error.h"
#include "image.h"
#include "font.h"

// Macros to generate Freetype table for error string names
#undef FTERRORS_H_
#define FT_ERRORDEF( e, v, s )  { e, s },
#define FT_ERROR_START_LIST     {
#define FT_ERROR_END_LIST       { 0, NULL } };
static const struct ft_error {
    int          err_code;
    const char*  err_msg;
} ft_errors[] =
#include FT_ERRORS_H

// Static freetype library instance
static FT_Library ftlib = NULL;

namespace glp {

/**
 * Creates and returns shared pointer to new font object from the specified font file path.
 * May throw ErrorFont() exception on error.
 * @param filename string with font file path
 * @return shared pointer to font object
 */
FontSP Font::make(const std::string& filename) {

    // Creates font face from file
    FontSP f = std::make_shared<Font>();
    int errcode = FT_New_Face(ftlib, filename.c_str(), 0, &f->mFace);
    if (errcode) {
        throw ErrorFont(errorStr(errcode));
    }
    f->setSize(f->mSize);
    return f;
}

/**
 * Creates and returns shared pointer to new font object from font data
 * Throw ErrorFont() exception on error
 * @param data pointer to font data
 * @param len number of bytes of font data
 * @return shared pointer to font object
 */
FontSP Font::make(const unsigned char* data, int len) {

    // Creates font from memory data
    FontSP f = std::make_shared<Font>();
    int errcode = FT_New_Memory_Face(ftlib, (const FT_Byte*)(data), len, 0, &f->mFace);
    if (errcode) {
        throw ErrorFont(errorStr(errcode));
    }
    f->setSize(f->mSize);
    return f;
}

/**
 * Constructor used internally
 */
Font::Font(): mFace{nullptr}, mSize{12.0}, mDPI{72.0}, mColor{0,0,0,1}, mLineSpacing{1.0} {

    // Initialize Freetype library if necessary
    if (ftlib == nullptr) {
        int errcode = FT_Init_FreeType(&ftlib);
        if (errcode) {
            throw ErrorFont(errorStr(errcode));
        }
    }
}

/**
 * Destructor
 */
Font::~Font() {

    logDebug("~Font()");
    FT_Done_Face(mFace);
    mFace = nullptr;
}


/**
 * Sets the font color
 * @param color color to set
 * @return reference to this font
 */
Font& Font::setColor(const glm::vec4& color) {

    mColor = color;
    return *this;
}

/**
 * Sets the font size in points
 * May throw ErrorFont exception
 * @param size font size in points (1/72 inch)
 */
void Font::setSize(float size) {

    mSize = size;
    auto error = FT_Set_Char_Size(mFace, 0, mSize * 64, mDPI, mDPI);
    if (error) {
        throw ErrorFont(errorStr(error));
    }
}

/**
 * Sets the font DPI
 * May throw ErrorFont exception
 * @param dpi font dots per inch
 */
void Font::setDPI(float dpi) {

    mDPI = dpi;
    setSize(mSize);
}

/**
 * Sets the font line spacing factor used when rendering multiline text to an image.
 * The default value is 1.0 which uses the font line height.
 * @param spacing line spacing
 * @return reference to this font
 */
Font& Font::setLineSpacing(float spacing) {

    mLineSpacing = spacing;
    return *this;
}

/**
 * Returns this font size in points
 * @return font size
 */
float Font::size() const {
    
    return mSize;
}

/**
 * Returns the font height in pixels
 * @return font height in pixels
 */
int Font::height() const {

    return mFace->size->metrics.height >> 6;
}

/**
 * Return this font metrics
 * @return  copy of this font metrics
 */
FT_Size_Metrics Font::metrics() const {

    return mFace->size->metrics;
}

/**
 * Returns the current line spacing factor
 * @return current line spacing factor
 */
float Font::lineSpacing() const {

    return mLineSpacing;
}

/**
 * Loads glyph specified by the codepoint
 * @param codepoint
 * @param flags
 * @return pointer to error messsage or nullptr if no error
 */
const char* Font::loadGlyph(int codepoint, int flags) {

    int error = FT_Load_Char(mFace, codepoint, FT_LOAD_RENDER);
    if (error) {
        return errorStr(error);
    } else {
        return nullptr;
    }
}

/**
 * Returns pointer to previously loaded glyph
 * @return
 */
FT_GlyphSlot Font::getGlyph() {

    return mFace->glyph;
}

/**
 * Measure the bounding box of the text in pixels
 * Throw ErrorFont() exception on error
 * @param text text to measure
 * @param width reference to store measured width
 * @param height reference to store measured height
 * @return 0 if OK or error code
 */
void Font::measure(const std::string& text, int& width, int& height) const {

    int lineHeight = (float)(mFace->size->metrics.height >> 6) * mLineSpacing;
    int maxWidth = 0;
    int x = 0;
    int y = lineHeight;
    auto pc = (const unsigned char*)text.data();
    int textlen = text.size();
    int codepoint;
    while (*pc) {
        // Get next codepoint
        size_t len = utf8proc_iterate(pc, textlen, &codepoint);
        if (codepoint == -1) {
            throw(ErrorFont("FT Invalid character code"));
        }
        pc += len;
        textlen -= len;
        // Checks for line terminator
        if (codepoint == '\n') {
            if (x > maxWidth) {
                maxWidth = x;
            }
            x = 0;
            y = y + lineHeight;
            continue;
        }
        // load glyph image into the slot
        int error = FT_Load_Char(mFace, codepoint, FT_LOAD_RENDER);
        if (error) {
            throw ErrorFont(errorStr(error));
        }
        FT_GlyphSlot slot = mFace->glyph;
        // Advance pen coordinates
        x += slot->advance.x >> 6;
        y += slot->advance.y >> 6;
    }
    if (x > maxWidth) {
        maxWidth = x;
    }
    width = maxWidth;
    if (codepoint == '\n') {
        height = y;
    } else {
        height = y - (mFace->size->metrics.descender >> 6) + 2;
    }
}

/**
 * Render text encoded in UTF8 in the specified image at the specified coordinates.
 * Coordinates start from the upper left corner.
 * The initial x, y coordinates are the origin point for the font baseline.
 * Throw ErrorFont() exception on error
 * @param img reference to Image object
 * @param x   initial image x coordinate in pixels (starts from top/left)
 * @param y   initial image y coordinate in pixels (starts from top/left)
 * @param text text to render
 * @param coords pointer to optional vector to which glyph coordinates will be stored.
 */
void Font::render(ImageSP img, int x, int y, const std::string& text, std::vector<Coord>* coords) {

    assert(img->channels() == 1 || img->channels() == 4);
    assert(x >= 0 && x < img->width());
    assert(y >= 0 && x < img->height());

    int cx = x;
    int cy = y;
    int lineHeight = (float)(mFace->size->metrics.height >> 6) * mLineSpacing;
    int codepoint;
    auto pc = (const unsigned char*)text.data();
    int textlen = text.size();
    int nline = 0;
    int ncol = 0;
    while (true) {
        if (coords) {
            coords->emplace_back(float(cx), float(cy));
        }
        if (!*pc) {
            break;
        }
        // Get next codepoint
        size_t len = utf8proc_iterate(pc, textlen, &codepoint);
        if (codepoint < 0) {
            throw(ErrorFont("FT Invalid character code"));
        }
        pc += len;
        textlen -= len;
        // Checks for line terminator
        if (codepoint == '\n') {
            cx = x;
            cy = cy + lineHeight;
            nline++;
            continue;
        }
        // load glyph image into the slot (erase previous one)
        int error = FT_Load_Char(mFace, codepoint, FT_LOAD_RENDER);
        if (error) {
            throw ErrorFont(errorStr(error));
        }
        FT_GlyphSlot slot = mFace->glyph;
        // Single channel image
        if (img->channels() == 1) {
            drawBitmap1(img, &slot->bitmap, cx + slot->bitmap_left, cy - slot->bitmap_top);
        } else {
            drawBitmap4(img, &slot->bitmap, cx + slot->bitmap_left, cy - slot->bitmap_top);
        }
        // Advance image coordinates
        cx += slot->advance.x >> 6;
        cy += slot->advance.y >> 6;
        ncol++;
    }
}

/**
 * Returns pointer to error string message from error code
 * @param errorcode error code
 * @return pointer to C string with error message
 */
const char* Font::errorStr(int errcode) {

    auto perr = (struct ft_error*)&ft_errors[0];
    while (perr->err_msg != nullptr) {
        if (perr->err_code == errcode) {
            return perr->err_msg;
        }
        perr++;
    }
    return "unknown error";
}

/**
 * Draw glyph bitmap to a one channel image
 * @param img reference to image
 * @param bm  pointer to glyph bitmap
 * @param x   x coordinate
 * @param y   y coordinate
 */
void Font::drawBitmap1(const ImageSP& img, FT_Bitmap* bm, int x, int y) {

    int xmax = x + bm->width;
    int ymax = y + bm->rows;
    int ix, iy, bx, by;
    for (ix = x, bx = 0; ix < xmax; ix++, bx++) {
        for (iy = y, by = 0; iy < ymax; iy++, by++) {
            if (ix >= img->width() || iy >= img->height()) {
                continue;
            }
            img->data()[iy * img->width() + ix] = bm->buffer[by * bm->width + bx];
        }
    }
}

/**
 * Draw bitmap to a four channel image
 * @param img reference to image
 * @param bm  pointer to font bitmap
 * @param x   x coordinate
 * @param y   y coordinate
 */
void Font::drawBitmap4(const ImageSP& img, FT_Bitmap* bm, int x, int y) {

    int xmax = x + bm->width;
    int ymax = y + bm->rows;
    int ix, iy, bx, by;
    for (ix = x, bx = 0; ix < xmax; ix++, bx++) {
        for (iy = y, by = 0; iy < ymax; iy++, by++) {
            if (ix >= img->width() || iy < 0 || iy >= img->height()) {
                continue;
            }
            unsigned char  bmb = bm->buffer[by * bm->width + bx];
            unsigned char* pim = &img->data()[iy * img->width() * 4 + ix * 4];
            pim[0] = 255 * mix(pim[0] / 255.0, mColor.r, bmb / 255.0);
            pim[1] = 255 * mix(pim[1] / 255.0, mColor.g, bmb / 255.0);
            pim[2] = 255 * mix(pim[2] / 255.0, mColor.b, bmb / 255.0);
            pim[3] = 255 * mix(pim[3] / 255.0, mColor.a, bmb / 255.0);
        }
    }
}

// Performs a linear interpolation between 'x'  and 'y' using 'a' to weight between them.
float Font::mix(float x, float y, float a) {

    return x * (1 - a) + y * a;
}

/**
 * Draw caret at the specified image and coordinates
 *
 * @param img reference to image
 * @param x x coordinate of the current glyph baseline
 * @param y y coordinate of the current glyph baseline (from top to bottom)
 */
void Font::drawCaret(Image& img, int x, int y) {

    int ascender = mFace->size->metrics.ascender >> 6;
    for (int py = y-ascender; py < y; py++) {
        img.drawPix(x, py, mColor);
    }
}

} // namespace


