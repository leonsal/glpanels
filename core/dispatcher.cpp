#include <algorithm>
#include "dispatcher.h"

namespace glp {

// Initialize static next subscription id
int Dispatcher::mNextSubid = 1;

/**
 * Constructor
 */
Dispatcher::Dispatcher(): mCancelNotify{false} {

}

/**
 * Cancel next dispatch of current event
 */
void Dispatcher::cancelDispatch() {

    mCancelNotify = true;
}

/**
 * Register a listener to the events with the specified name
 * @param evname string with event name
 * @param cb callback (function, functor, lambda or object method (using std::bind)
 * @return id id to unlisten() to this event.
 */
int Dispatcher::listen(const char* evname, CB cb) {

    if (mSubs.count(evname) == 0) {
        mSubs[evname] = std::vector<Subs>();
    }
    Subs s {cb, mNextSubid++};
    mSubs[evname].push_back(s);
    return s.id;
}

/**
 * Remove listener for the specified event name and id
 * @param evname string with event name
 * @param id id used when this event was listend
 * @return true if event listener with specified id was found or false otherwise.
 */
bool Dispatcher::unlisten(int subid) {

    for (auto& entry: mSubs)  {
        auto& subs = entry.second;
        auto it = std::find_if(subs.begin(), subs.end(), [subid](auto& sub){
            return sub.id == subid;
        });
        if (it != subs.end()) {
            return true;
        }
    }
    return false;
}

/**
 * Notify all listeners of the specified event
 * @param evname pointer to null terminated string with event name
 * @param ev pointer to event object
 */
void Dispatcher::dispatch(const Event& ev) {

    if (mSubs.count(ev.name) == 0) {
        return;
    }
    auto callbacks = mSubs[ev.name];
    mCancelNotify = false;
    for (auto s : callbacks) {
        s.cb(ev);
        if (mCancelNotify) {
            break;
        }
    }
}

} // namespace


