#include "border.h"

namespace glp {

/**
 * Sets the widths in pixels of each side
 * @param top top border width in pixels
 * @param right right border width in pixels
 * @param bottom bottom border width in pixels
 * @param left left border width in pixels
 */
void Border::set(float top, float right, float bottom, float left) {

    this->top = top;
    this->right = right;
    this->bottom = bottom;
    this->left = left;
}

/**
 * Returns if all sides of this Border have 0 width
 * @return true if all sides of this Border have 0 width
 */
bool Border::empty() const {

    return top == 0 && right == 0 && bottom == 0 && left == 0;
}

} // namespace

