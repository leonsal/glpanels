#include <chrono>
#include <ctime>
#include "log.h"
#include "fmt/printf.h"
#include "fmt/chrono.h"

namespace glp {

void log_emit(const char* format, const char* prefix, fmt::format_args args) {

    std::time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    fmt::print("{:%Y-%m-%d %H:%M:%S}:{}:{}\n", *std::localtime(&now), prefix, fmt::vformat(format, args));
}

}


