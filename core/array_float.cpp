#include <cassert>
#include "array_float.h"

namespace glp {

/**
 * Default constructor
 */
ArrayFloat::ArrayFloat(): std::vector<float>() {
}

/**
 * Constructor from specified number of elements
 *
 * @param n number of elements to set array size
 */
ArrayFloat::ArrayFloat(size_t n): std::vector<float>(n) {
}

/**
 * Appends float to array
 *
 * @param v float to append
 */
void ArrayFloat::append(float v) {

    push_back(v);
}

/**
 * Appends two floats to array
 *
 * @param v0 float to append
 * @param v1 float to append
 */
void ArrayFloat::append(float v0, float v1) {

    push_back(v0);
    push_back(v1);
}

/**
 * Appends three floats to array
 *
 * @param v0 float to append
 * @param v1 float to append
 * @param v2 float to append
 */
void ArrayFloat::append(float v0, float v1, float v2) {

    push_back(v0);
    push_back(v1);
    push_back(v2);
}

/**
 * Appends four floats to array
 *
 * @param v0 float to append
 * @param v1 float to append
 * @param v2 float to append
 */
void ArrayFloat::append(float v0, float v1, float v2, float v3) {

    push_back(v0);
    push_back(v1);
    push_back(v2);
    push_back(v3);
}

/**
 * Appends five floats to array
 *
 * @param v0 float to append
 * @param v1 float to append
 * @param v2 float to append
 */
void ArrayFloat::append(float v0, float v1, float v2, float v3, float v4) {

    push_back(v0);
    push_back(v1);
    push_back(v2);
    push_back(v3);
    push_back(v4);
}

void ArrayFloat::append(float v0, float v1, float v2, float v3, float v4, float v5) {

    push_back(v0);
    push_back(v1);
    push_back(v2);
    push_back(v3);
    push_back(v4);
    push_back(v5);
}

/**
 * Appends a Vec2 to the array
 *
 * @param v reference to vector to append
 */
void ArrayFloat::append(const glm::vec2& v) {

    push_back(v.x);
    push_back(v.y);
}

/**
 * Appends a Vec3 to the array
 *
 * @param v reference to vector to append
 */
void ArrayFloat::append(const glm::vec3& v) {

    push_back(v.x);
    push_back(v.y);
    push_back(v.z);
}

/**
 * Appends a Vec4 to the array
 *
 * @param v referencer to vector to append
 */
void ArrayFloat::append(const glm::vec4& v) {

    push_back(v.x);
    push_back(v.y);
    push_back(v.z);
    push_back(v.w);
}

/**
 * Get Vec2 from the specified position in the array
 *
 * @param pos position in the array
 * @param v reference to Vec2
 */
void ArrayFloat::get(size_t pos, glm::vec2& v) {

    assert(pos + 2 <= size());
    v.x = std::vector<float>::operator[](pos++);
    v.y = std::vector<float>::operator[](pos);
}

/**
 * Get Vec3 from the specified position in the array
 *
 * @param pos position in the array
 * @param v reference to Vec3
 */
void ArrayFloat::get(size_t pos, glm::vec3& v) {

    assert(pos + 3 <= size());
    v.x = std::vector<float>::operator[](pos++);
    v.y = std::vector<float>::operator[](pos++);
    v.z = std::vector<float>::operator[](pos);
}

/**
 * Get Vec4 from the specified position in the array
 *
 * @param pos position in the array
 * @param v reference to Vec4
 */
void ArrayFloat::get(size_t pos, glm::vec4& v) {

    assert(pos + 4 <= size());
    v.x = std::vector<float>::operator[](pos++);
    v.y = std::vector<float>::operator[](pos++);
    v.z = std::vector<float>::operator[](pos++);
    v.w = std::vector<float>::operator[](pos);
}

/**
 * Sets two elements from the specified start position
 *
 * @param pos start position
 * @param v0 value of first element
 * @param v1 value of second element
 */
void ArrayFloat::set(size_t pos, float v0, float v1) {

    assert(pos + 2 <= size());
    auto it = begin() + pos;
    *it++ = v0;
    *it = v1;
}

/**
 * Sets three elements from the specified start position
 *
 * @param pos start position
 * @param v0 value of first element
 * @param v1 value of second element
 * @param v2 value of third element
 */
void ArrayFloat::set(size_t pos, float v0, float v1, float v2) {

    assert(pos + 3 <= size());
    auto it = begin() + pos;
    *it++ = v0;
    *it++ = v1;
    *it   = v2;
}

/**
 * Sets four elements from the specified start position
 *
 * @param pos start position
 * @param v0 value of first element
 * @param v1 value of second element
 * @param v2 value of third element
 * @param v4 value of third element
 */
void ArrayFloat::set(size_t pos, float v0, float v1, float v2, float v3) {

    assert(pos + 4 <= size());
    auto it = begin() + pos;
    *it++ = v0;
    *it++ = v1;
    *it++ = v2;
    *it   = v3;
}

/**
 * Sets five elements from the specified start position
 *
 * @param pos start position
 * @param v0 value of first element
 * @param v1 value of second element
 * @param v2 value of third element
 * @param v4 value of third element
 */
void ArrayFloat::set(size_t pos, float v0, float v1, float v2, float v3, float v4) {

    assert(pos + 5 <= size());
    auto it = begin() + pos;
    *it++ = v0;
    *it++ = v1;
    *it++ = v2;
    *it++ = v3;
    *it   = v4;
}

/**
 * Sets Vec2 at the specified position in the array
 *
 * @param pos position in the array
 * @param v reference to Vec2
 */
void ArrayFloat::set(size_t pos, const glm::vec2& v) {

    assert(pos + 2 <= size());
    std::vector<float>::operator[](pos++) = v.x;
    std::vector<float>::operator[](pos) = v.y;
}

/**
 * Sets Vec3 at the specified position in the array
 *
 * @param pos position in the array
 * @param v reference to Vec3
 */
void ArrayFloat::set(size_t pos, const glm::vec3& v) {

    assert(pos + 3 <= size());
    std::vector<float>::operator[](pos++) = v.x;
    std::vector<float>::operator[](pos++) = v.y;
    std::vector<float>::operator[](pos)   = v.z;
}

/**
 * Sets Vec4 at the specified position in the array
 *
 * @param pos position in the array
 * @param v reference to Vec4
 */
void ArrayFloat::set(size_t pos, const glm::vec4& v) {

    assert(pos + 4 <= size());
    std::vector<float>::operator[](pos++) = v.x;
    std::vector<float>::operator[](pos++) = v.y;
    std::vector<float>::operator[](pos++) = v.z;
    std::vector<float>::operator[](pos)   = v.w;
}

} // namespace

