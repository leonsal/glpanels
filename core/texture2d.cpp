#include <cassert>
#include "log.h"
#include "texture2d.h"
#include "utf8proc/utf8proc.h"
#include "error.h"

namespace glp {

/**
 * Creates and returns shared pointer to a new Texture2D.
 * The created texture is empty.
 * @return shared pointer to Texture2D
 */
Texture2DSP Texture2D::make(void) {

    return std::make_shared<Texture2D>();
}

/**
 * Default constructor
 * Creates an empty texture
 */
Texture2D::Texture2D():
    mTexname(0),
    mUnit(GL_TEXTURE0),
    mMagFilter(GL_LINEAR),
    mMinFilter(GL_LINEAR),
    mWrapS(GL_CLAMP_TO_EDGE),
    mWrapT(GL_CLAMP_TO_EDGE),
    mUdata{0, 0, 1, 1},
    mIformat(0),
    mWidth(0),
    mHeight(0),
    mFormat(0),
    mFormatType(0),
    mSendParams(true),
    mGenMipmaps(false) {

    glGenTextures(1, &mTexname);
}

/**
 * Destructor
 */
Texture2D::~Texture2D() {

    logDebug("~Texture2D({})", mTexname);
    if (mTexname > 0) {
        glDeleteTextures(1, &mTexname);
        mTexname = 0;
    }
}

/**
 * Sets this texture from the specified image
 * The data from the image provided is transferred to OpenGL,
 * so after this call the provided image can be destroyed.
 * @param img pointer to Image object
 */
void Texture2D::setImage(ImageSP img) {

    // Sets the texture unit for this texture
    glActiveTexture(mUnit);

    // Sets the correct pixel alignment depending on the number of channels
    glBindTexture(GL_TEXTURE_2D, mTexname);
    switch (img->channels()) {
        case 1:
            mFormat = GL_RED;
            glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
            break;
        case 2:
            mFormat = GL_RG;
            glPixelStorei(GL_UNPACK_ALIGNMENT, 2);
            break;
        case 3:
            mFormat = GL_RGB;
            glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
            break;
        case 4:
            mFormat = GL_RGBA;
            glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
            break;
        default:
            mFormat = 0;
            throw Error("Non supported number of channels");
    }
    mWidth      = img->width();
    mHeight     = img->height();
    mIformat    = mFormat;
    mFormatType = GL_UNSIGNED_BYTE;

    // Sets texture data for mutable texture
    glTexImage2D(
        GL_TEXTURE_2D,  // texture type
        0,              // level of detail
        mIformat,       // internal format
        mWidth,         // width in texels
        mHeight,        // height in texels
        0,              // border must be 0
        mFormat,        // format of supplied texture data
        mFormatType,    // type of external format color component
        img->data()     // pointer to image data
    );
    // Generates mipmaps if requested
    if (mGenMipmaps) {
        glGenerateMipmap(GL_TEXTURE_2D);
    }
}

/**
 * Sets this texture data from image file
 * @param filepath pointer to file path name
 * @return pointer to error string or NULL if OK
 */
const char* Texture2D::setImageFromFile(const char* filepath) {

    const char* errmsg;
    auto img = Image::fromFile(filepath, &errmsg);
    if (img == nullptr) {
        return errmsg;
    }
    setImage(img);
    return nullptr;
}

/**
 * Sets this texture with the image of the specified text rendered with the specified font
 * @param text text to render
 * @param font font object to used
 * @param imgx optional start x coordinate in the rendered image
 * @param coord optional pointer to vector to store the glyph coordinates in the texture
 */
void Texture2D::renderText(const std::string& text, FontSP font, int imgx, std::vector<Font::Coord>* coords) {

    // If text is empty, generates empty texture.
    if (text.empty()) {
        auto img = Image::make(0, 0, 1);
        mGenMipmaps = false;
        setImage(img);
        return;
    }

    // Measure dimensions of the text to render in pixels
    int width;
    int height;
    font->measure(text, width, height);

    // Creates image and renders font to image
    auto img = Image::make(imgx + width, height, 1);
    img->setColor(glm::vec4{0,0,0,0});
    font->render(img, imgx, font->height(), text, coords);

    // Sets this texture from the rendered image
    mGenMipmaps = false;
    setImage(img);
}

/**
 * Sets the texture magnification filter
 * @param magFilter magnification filter type
 */
void Texture2D::setMagFilter(int magFilter) {

    mMagFilter = magFilter;
    mSendParams = true;
}

/**
 * Sets the texture minification filter
 * @param minFilter minification filter type
 */
void Texture2D::setMinFilter(int minFilter) {

    mMinFilter = minFilter;
    mSendParams = true;
}

/**
 * Sets wrap mode for texture s (x) coordinate
 * @param wrapS wrap mode
 */
void Texture2D::setWrapS(int wrapS) {

    mWrapS = wrapS;
    mSendParams = true;
}

/**
 * Sets wrap mode for texture t (y) coordinate
 * @param wrapT wrap mode
 */
void Texture2D::setWrapT(int wrapT) {

    mWrapT = wrapT;
    mSendParams = true;
}

/**
 * Sets the repeat factor for x and y coordinates
 * @param rx x repeat factor
 * @param ry y repeat factor
 */
void Texture2D::setRepeat(float rx, float ry) {

    mUdata.repeatX = rx;
    mUdata.repeatY = rx;
}

/**
 * Sets the offset factor for x and y coordinates
 * @param ox offset factor for x coordinate
 * @param oy offset factor for y coordinate
 */
void Texture2D::setOffset(float ox, float oy) {

    mUdata.offsetX = ox;
    mUdata.offsetY = oy;
}

/**
 * Sets the texture unit to used.
 * The default value is GL_TEXTURE0
 * @param unit
 */
void Texture2D::setUnit(GLenum unit) {

    mUnit = unit;
}

/**
 * Returns the current magnification filter type
 * @return magnification filter type
 */
int Texture2D::magFilter() const {

    return mMagFilter;
}

/**
 * Returns the current minification filter type
 * @return minification filter type
 */
int Texture2D::minFilter() const {

    return mMinFilter;
}

/**
 * Returns the wrap mode for s coordinate
 * @return wrap mode for s coordinate
 */
int Texture2D::wrapS() const {

    return mWrapS;
}

/**
 * Returns the wrap mode for t coordinate
 * @return wrap mode for t coordinate
 */
int Texture2D::wrapT() const {

    return mWrapT;
}

/**
 * Returns the current repeat factor for x and y coordinates
 * @param px pointer to store x repeat factor
 * @param py pointer to store y repeat factor
 */
void Texture2D::repeat(float* px, float *py) const {

    *px = mUdata.repeatX;
    *py = mUdata.repeatY;
}

/**
 * Returns the current offset factor for x and y coordinates
 * @param ox pointer to store x offset factor
 * @param oy pointer to store y offset factor
 */
void Texture2D::offset(float* ox, float *oy) const {

    *ox = mUdata.offsetX;
    *oy = mUdata.offsetY;
}

/**
 * Returns this texture width in pixels
 * @return texture width in pixesl
 */
int Texture2D::width(void) const {

    return mWidth;
}

/**
 * Returns this texture height in pixels
 * @return texture height in pixesl
 */
int Texture2D::height(void) const {

    return mHeight;
}

/**
 * Transfer texture information to OpenGL
 * Should be called before drawing the geometry which uses this texture.
 * @param locSample location of the shader uniform for the sampler
 * @param locInfo location of the shader uniform for the texture info
 */
void Texture2D::transfer(GLint locSampler, GLint locInfo) {

    // Sets the texture unit for this texture
    glActiveTexture(mUnit);
    glBindTexture(GL_TEXTURE_2D, mTexname);

    // Sets texture parameters if needed
    if (mSendParams) {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mMagFilter);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mMinFilter);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, mWrapS);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, mWrapT);
        mSendParams = false;
    }

    // Transfer texture unit uniform
    glUniform1i(locSampler, GL_TEXTURE0 - mUnit);

    // Transfer texture info 
    const int count = sizeof(mUdata)/sizeof(float); // number of floats
    glUniform2fv(locInfo, count, (GLfloat*)(&mUdata));
}

} // namespace

