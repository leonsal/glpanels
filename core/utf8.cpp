#include "utf8proc/utf8proc.h"
#include "error.h"
#include "utf8.h"

namespace glp {

/**
 * Converts an Unicode codepoint to a string encoded using UTF8
 *
 * @param codepoint character codepoint
 * @param trow throw exception flag
 * @return encoded string (empty if error)
 */
std::string utf8String(int codepoint, bool trow) {

    if (!utf8proc_codepoint_valid(codepoint)) {
        if (trow) {
            throw ErrorUnicode("Invalid codepoint");
        }
        return "";
    }
    unsigned char dst[4];
    std::size_t clen = utf8proc_encode_char(codepoint, dst);
    return std::string((char*)(&dst[0]), clen);
}

/**
 * Returns the number of Unicode characters in the specified string encoded using UTF8
 *
 * @param s string encoded using UTF8
 * @param trow throw exception flag
 * @return number of Unicode characters or 0 if error found
 */
std::size_t utf8Len(const std::string& s, bool trow) {

    if (s.empty()) {
        return 0;
    }

    int nbytes = s.size();
    std::size_t count = 0;
    const unsigned char* pc = (const unsigned char*)(s.c_str());
    while (nbytes > 0) {
        int codepoint;
        int clen = utf8proc_iterate(pc, nbytes, &codepoint);
        if (clen < 0) {
            if (trow) {
                throw ErrorUnicode("Invalid UTF8 string");
            }
            return 0;
        }
        pc += clen;
        nbytes -= clen;
        count++;
    }
    return count;
}

/**
 * Returns the start byte position and length of the Unicode character at the
 * specified character position in the string
 *
 * @param s string with UTF8 encoded characters
 * @param pos character position in the string
 * @param len reference to store the number of bytes of the character
 * @param trow throw exception flag
 * @return position in bytes from the start of the string to the specified
 * character position or std::string::npos if error
 */
std::size_t utf8Char(const std::string& s, std::size_t pos, std::size_t& len, bool trow) {

    if (pos >= s.size()) {
        if (trow) {
            throw ErrorUnicode("Invalid character position in UTF8 string");
        }
        return std::string::npos;
    }
    const unsigned char* pc = (const unsigned char*)(s.c_str());
    std::size_t start = 0;
    std::size_t nchars = 0;
    std::size_t nbytes = s.size();
    int clen;
    while (true) {
        int codepoint;
        clen = utf8proc_iterate(pc, nbytes, &codepoint);
        if (clen < 0) {
            if (trow) {
                throw ErrorUnicode("Invalid UTF8 string");
            }
            return std::string::npos;
        }
        if (nchars == pos) {
            len = clen;
            return start;
        }
        pc += clen;
        nbytes -= clen;
        start += clen;
        nchars++;
    }
}

/**
 * Removes character at specified position in the specified
 * string encoded using UTF8
 *
 * @param s reference to string which to remove character
 * @param pos position of the character in the string
 * @param trow throw exception flag
 * @return 0 if OK or -1 if error
 */
int utf8Remove(std::string& s, std::size_t pos, bool trow) {

    std::size_t len = 0;
    std::size_t start = utf8Char(s, pos, len, trow);
    if (start == std::string::npos) {
        return -1;
    }
    s.erase(start, len);
    return 0;
}

/**
 * Inserts string at the character position of another string encoded using UTF8
 *
 * @param s reference to string which will be modified
 * @param pos position of the character in the string
 * @param data reference to string to be inserted
 * @param trow throw exception flag
 *
 */
int utf8Insert(std::string& s, std::size_t pos, const std::string& data, bool trow) {

    if (pos > s.size()) {
        throw ErrorUnicode("Invalid character position in UTF8 string");
    }
    if (pos == s.size()) {
        s.append(data);
        return 0;
    }
    std::size_t len = 0;
    int offset = utf8Char(s, pos, len, trow);
    if (offset < 0) {
        return offset;
    }
    s.insert(offset, data);
    return 0;
}

/**
 * Returns string which is the prefix of the specified number of characters
 * of another string.
 *
 * @param s reference to string to extract prefix
 * @param pos number of characters of the prefix
 * @return string with prefix or empty if error.
 */
std::string utf8Prefix(const std::string& s, std::size_t pos, bool trow) {

    if (pos == s.size()) {
        return s;
    }
    std::size_t len = 0;
    std::size_t start = utf8Char(s, pos, len, trow);
    if (start == std::string::npos) {
        return "";
    }
    return s.substr(0, start);
}

} // namespace

