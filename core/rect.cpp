#include "rect.h"

namespace glp {

/**
 * Constructor
 * @param x
 * @param y
 * @param width
 * @param height
 */
Rect::Rect(float x, float y, float width, float height):
    x{x}, y{y}, width{width}, height{height} {
}

/**
 * Returns if this Rect is empty (area == 0)
 * @return
 */
bool Rect::empty() const {

    return width == 0 || height == 0;
}

/**
 * Returns another Rect with the intersection of this Rect with other Rect.
 * If other does not intersect this Rect, the returned Rect has area zero.
 * @param other
 * @return Rect with intersect of this with other.
 */
Rect Rect::intersect(const Rect &other) const {

    Rect res{};

    // No intersection cases
    if ((x + width <= other.x) || (x >= other.x + other.width)) {
        return res;
    }
    if ((y + height <= other.y) || y >= other.y > other.height) {
        return res;
    }

    // Result x and width
    if (x < other.x) {
        res.x = other.x;
        res.width = x + width - other.x;
        if (res.width > other.width) {
            res.width = other.width;
        }
    } else {
        res.x = x;
        res.width = width;
        auto iw = other.x + other.width - x;
        if (res.width > iw) {
            res.width = iw;
        }
    }

    // Result y and height
    if (y < other.y) {
        res.y = other.y;
        res.height = y + height - other.y;
        if (res.height > other.height) {
            res.height = other.height;
        }
    } else {
        res.y = y;
        res.height = height;
        auto ih = other.y + other.height - y;
        if (res.height > ih) {
            res.height = ih;
        }
    }
    return res;
}

}

