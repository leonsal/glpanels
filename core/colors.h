#pragma once
#include "glm/ext/vector_float3.hpp"

namespace glp {

extern const glm::vec3 ColorAliceblue;
extern const glm::vec3 ColorAntiquewhite;
extern const glm::vec3 ColorAqua;
extern const glm::vec3 ColorAquamarine;
extern const glm::vec3 ColorAzure;
extern const glm::vec3 ColorBeige;
extern const glm::vec3 ColorBisque;
extern const glm::vec3 ColorBlack;
extern const glm::vec3 ColorBlanchedalmond;
extern const glm::vec3 ColorBlue;
extern const glm::vec3 ColorBlueviolet;
extern const glm::vec3 ColorBrown;
extern const glm::vec3 ColorBurlywood;
extern const glm::vec3 ColorCadetblue;
extern const glm::vec3 ColorChartreuse;
extern const glm::vec3 ColorChocolate;
extern const glm::vec3 ColorCoral;
extern const glm::vec3 ColorCornflowerblue;
extern const glm::vec3 ColorCornsilk;
extern const glm::vec3 ColorCrimson;
extern const glm::vec3 ColorCyan;
extern const glm::vec3 ColorDarkblue;
extern const glm::vec3 ColorDarkcyan;
extern const glm::vec3 ColorDarkgoldenrod;
extern const glm::vec3 ColorDarkgray;
extern const glm::vec3 ColorDarkgreen;
extern const glm::vec3 ColorDarkgrey;
extern const glm::vec3 ColorDarkkhaki;
extern const glm::vec3 ColorDarkmagenta;
extern const glm::vec3 ColorDarkolivegreen;
extern const glm::vec3 ColorDarkorange;
extern const glm::vec3 ColorDarkorchid;
extern const glm::vec3 ColorDarkred;
extern const glm::vec3 ColorDarksalmon;
extern const glm::vec3 ColorDarkseagreen;
extern const glm::vec3 ColorDarkslateblue;
extern const glm::vec3 ColorDarkslategray;
extern const glm::vec3 ColorDarkslategrey;
extern const glm::vec3 ColorDarkturquoise;
extern const glm::vec3 ColorDarkviolet;
extern const glm::vec3 ColorDeeppink;
extern const glm::vec3 ColorDeepskyblue;
extern const glm::vec3 ColorDimgray;
extern const glm::vec3 ColorDimgrey;
extern const glm::vec3 ColorDodgerblue;
extern const glm::vec3 ColorFirebrick;
extern const glm::vec3 ColorFloralwhite;
extern const glm::vec3 ColorForestgreen;
extern const glm::vec3 ColorFuchsia;
extern const glm::vec3 ColorGainsboro;
extern const glm::vec3 ColorGhostwhite;
extern const glm::vec3 ColorGold;
extern const glm::vec3 ColorGoldenrod;
extern const glm::vec3 ColorGray;
extern const glm::vec3 ColorGreen;
extern const glm::vec3 ColorGreenyellow;
extern const glm::vec3 ColorGrey;
extern const glm::vec3 ColorHoneydew;
extern const glm::vec3 ColorHotpink;
extern const glm::vec3 ColorIndianred;
extern const glm::vec3 ColorIndigo;
extern const glm::vec3 ColorIvory;
extern const glm::vec3 ColorKhaki;
extern const glm::vec3 ColorLavender;
extern const glm::vec3 ColorLavenderblush;
extern const glm::vec3 ColorLawngreen;
extern const glm::vec3 ColorLemonchiffon;
extern const glm::vec3 ColorLightblue;
extern const glm::vec3 ColorLightcoral;
extern const glm::vec3 ColorLightcyan;
extern const glm::vec3 ColorLightgoldenrodyellow;
extern const glm::vec3 ColorLightgray;
extern const glm::vec3 ColorLightgreen;
extern const glm::vec3 ColorLightgrey;
extern const glm::vec3 ColorLightpink;
extern const glm::vec3 ColorLightsalmon;
extern const glm::vec3 ColorLightseagreen;
extern const glm::vec3 ColorLightskyblue;
extern const glm::vec3 ColorLightslategray;
extern const glm::vec3 ColorLightslategrey;
extern const glm::vec3 ColorLightsteelblue;
extern const glm::vec3 ColorLightyellow;
extern const glm::vec3 ColorLime;
extern const glm::vec3 ColorLimegreen;
extern const glm::vec3 ColorLinen;
extern const glm::vec3 ColorMagenta;
extern const glm::vec3 ColorMaroon;
extern const glm::vec3 ColorMediumaquamarine;
extern const glm::vec3 ColorMediumblue;
extern const glm::vec3 ColorMediumorchid;
extern const glm::vec3 ColorMediumpurple;
extern const glm::vec3 ColorMediumseagreen;
extern const glm::vec3 ColorMediumslateblue;
extern const glm::vec3 ColorMediumspringgreen;
extern const glm::vec3 ColorMediumturquoise;
extern const glm::vec3 ColorMediumvioletred;
extern const glm::vec3 ColorMidnightblue;
extern const glm::vec3 ColorMintcream;
extern const glm::vec3 ColorMistyrose;
extern const glm::vec3 ColorMoccasin;
extern const glm::vec3 ColorNavajowhite;
extern const glm::vec3 ColorNavy;
extern const glm::vec3 ColorOldlace;
extern const glm::vec3 ColorOlive;
extern const glm::vec3 ColorOlivedrab;
extern const glm::vec3 ColorOrange;
extern const glm::vec3 ColorOrangered;
extern const glm::vec3 ColorOrchid;
extern const glm::vec3 ColorPalegoldenrod;
extern const glm::vec3 ColorPalegreen;
extern const glm::vec3 ColorPaleturquoise;
extern const glm::vec3 ColorPalevioletred;
extern const glm::vec3 ColorPapayawhip;
extern const glm::vec3 ColorPeachpuff;
extern const glm::vec3 ColorPeru;
extern const glm::vec3 ColorPink;
extern const glm::vec3 ColorPlum;
extern const glm::vec3 ColorPowderblue;
extern const glm::vec3 ColorPurple;
extern const glm::vec3 ColorRed;
extern const glm::vec3 ColorRosybrown;
extern const glm::vec3 ColorRoyalblue;
extern const glm::vec3 ColorSaddlebrown;
extern const glm::vec3 ColorSalmon;
extern const glm::vec3 ColorSandybrown;
extern const glm::vec3 ColorSeagreen;
extern const glm::vec3 ColorSeashell;
extern const glm::vec3 ColorSienna;
extern const glm::vec3 ColorSilver;
extern const glm::vec3 ColorSkyblue;
extern const glm::vec3 ColorSlateblue;
extern const glm::vec3 ColorSlategray;
extern const glm::vec3 ColorSlategrey;
extern const glm::vec3 ColorSnow;
extern const glm::vec3 ColorSpringgreen;
extern const glm::vec3 ColorSteelblue;
extern const glm::vec3 ColorTan;
extern const glm::vec3 ColorTeal;
extern const glm::vec3 ColorThistle;
extern const glm::vec3 ColorTomato;
extern const glm::vec3 ColorTurquoise;
extern const glm::vec3 ColorViolet;
extern const glm::vec3 ColorWheat;
extern const glm::vec3 ColorWhite;
extern const glm::vec3 ColorWhitesmoke;
extern const glm::vec3 ColorYellow;
extern const glm::vec3 ColorYellowgreen;

} // namespace



