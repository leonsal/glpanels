#pragma once

namespace glp {

struct Rect {
    Rect() = default;
    ~Rect() = default;
    Rect(float x, float y, float width, float height);
    bool empty() const;
    Rect intersect(const Rect& other) const;

    float x;
    float y;
    float width;
    float height;
};

} // namespace

