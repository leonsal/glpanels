#pragma once
#include <memory>
#include "geometry.h"
#include "program.h"
#include "border.h"
#include "rect.h"
#include "renderinfo.h"
#include "texture2d.h"
#include "dispatcher.h"
#include "glm/ext/vector_float3.hpp"
#include "glm/ext/vector_float4.hpp"
#include "glm/ext/matrix_float4x4.hpp"

namespace glp {

class Panel;
using PanelSP = std::shared_ptr<Panel>;

class Panel : public Dispatcher {

    public:
        struct Style {
            Border      margin;
            Border      border;
            Border      padding;
            glm::vec4   roundness;
            glm::vec4   borderColor;
            glm::vec4   bgColor;
        };

    public:
        static PanelSP makeSP(float with, float height);
        Panel();
        Panel(float width, float height);
        ~Panel();
        Panel(const Panel&) = delete;
        Panel& operator=(const Panel&) = delete;

        void appendChild(PanelSP child);
        Border border() const;
        bool bounded() const;
        const std::vector<PanelSP> &children() const;
        float contentHeight() const;
        float contentWidth() const;
        glm::vec4 contentCoords() const;
        bool cursorOver() const;
        bool enabled() const;
        PanelSP fistChild() const;
        float height() const;
        int id() const;
        bool isChild(PanelSP child) const;
        void insertChild(PanelSP child, int pos);
        bool insideBorders(float x, float y) const;
        bool isTransparent() const;
        PanelSP lastChild() const;
        Border margin() const;
        glm::mat4 modelMatrix() const;
        std::string name() const;
        virtual bool onEvent(const Event& ev);
        PanelSP nextSibling() const;
        Border padding() const;
        glm::vec3 pos() const;
        glm::vec3 posAbs() const;
        PanelSP prevSibling() const;
        PanelSP removeChild(PanelSP child);
        void renderAll(const RenderInfo& ri);
        virtual void render(const RenderInfo& ri);
        bool renderSetup(const RenderInfo& ri);
        void renderDraw(const RenderInfo& ri);
        void setBgColor(const glm::vec3& color, float alpha=1.0f);
        void setBgColor(const glm::vec4& color);
        void setBorder(float width);
        void setBorder(float top, float right, float bottom, float left);
        void setBorder(const Border& border);
        void setBorderColor(const glm::vec3& color, float alpha=1.0f);
        void setBorderColor(const glm::vec4& color);
        void setBounded(bool bounded);
        void setCaret(float x, float y, float width, float height);
        void setCaretColor(const glm::vec4& color);
        void setContentAspectWidth(float width);
        void setContentAspectHeight(float height);
        void setContentHeight(float height);
        void setContentSize(float width, float height);
        void setContentWidth(float width);
        void setCursorOver(bool state);
        void setEnabled(bool enabled);
        void setHeight(float height);
        void setMargin(float top, float right, float bottom, float left);
        void setMargin(const Border& margin);
        void setName(const std::string& name);
        void setPadding(float width);
        void setPadding(float top, float right, float bottom, float left);
        void setPaddingColor(const glm::vec3& color, float alpha=1.0f);
        void setPaddingColor(const glm::vec4& color);
        void setPos(float x, float y);
        void setPos(float x, float y, float z);
        void setRoundness(float tl, float tr, float br, float bl);
        void setRoundness(float r);
        void setSize(float width, float height);
        void setStyle(const Panel::Style& st);
        void setTextColor(const glm::vec3& color, float alpha=1.0f);
        void setTextColor(const glm::vec4& color);
        void setImageTexture(Texture2DSP tex);
        void setTextTexture(Texture2DSP tex);
        void setWidth(float width);
        Texture2DSP texture() const;
        bool visible() const;
        float width() const;
        float x() const;
        float y() const;
        float z() const;

    public:
        enum class Op {
            Color       = 0,    // apply only color to content area
            Image       = 1,    // apply image texture to content area
            Text        = 2,    // apply text texture to content area
            Gradient    = 3,    // apply gradient to content area
            Grid        = 4,    // apply grid to content area
        };

    protected:
        float calcWidth() const;
        float calcHeight() const;
        Program* prog() const;
        void resize(float width, float height);
        void updateBounds();
        void setOperation(Op op);
        static void setup();

    private:
        static int mCount;
        static std::vector<float> mBuffer;
        static GeometryUP mGeom;
        static Program mProg;
        static const char* vertexShader;
        static const char* fragShader;
        static int mLocModelMatrix;
        static int mLocPanel;
        static int mLocTexSampler;
        static int mLocTexInfo;

    private:
        int mId;
        Panel* mParent;
        std::vector<PanelSP> mChildren;
        std::string mName;
        glm::vec3  mPos;         // position of panel left/top coordinate in pixels relative to its parent
        glm::vec3  mPosAbs;      // absolute position of panel left/top coordinate in pixels.
        float mWidth;       // panel width in pixels
        float mHeight;      // panel height in pixels
        Border mMargin;
        Border mBorder;
        Border mPadding;
        Rect   mContent;
        bool   mBounded;
        bool   mEnabled;
        bool   mVisible;
        bool   mCursorOver;
        glm::mat4  mModelMatrix;
        Texture2DSP mTex;   // optional texture
        float mXmin;        // minimum absolute x this panel can use
        float mXmax;        // maximum absolute x this panel can use
        float mYmin;        // minimum absolute y this panel can use
        float mYmax;        // maximum absolute y this panel can use
        struct {
            float       operation;       // operation type
            float       aspect;          // panel aspect ratio (width/height)
            float       width;           // panel width in pixels
            float       height;          // panel height in pixels
            glm::vec4   bounds;          // panel bounds in texture coordinates
            glm::vec4   border;          // panel borders in texture coordinates
            glm::vec4   padding;         // panel paddings in texture coordinates
            glm::vec4   content;         // panel content area in texture coordinates
            glm::vec4   borderColor;     // panel border color
            glm::vec4   paddingColor;    // panel padding color
            glm::vec4   contentColor;    // panel content color
            glm::vec4   roundness;       // panel corner roundness
            glm::vec4   textColor;       // panel text color
            float       caretWidth;      // caret width in texture coords (0: no caret)
            float       caretHeight;     // caret height in texture coords
            float       caretX;          // caret x in texture coords
            float       caretY;          // caret y in texture coords
            glm::vec4   caretColor;      // caret color
        } mUdata;                   // uniform data
};

} // namespace
