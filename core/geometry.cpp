#include <cassert>
#include "log.h"
#include "geometry.h"

namespace glp {

/**
 * Creates and returns unique pointer to a new empty geometry
 */
GeometryUP Geometry::makeUP() {

    return std::make_unique<Geometry>();
}

/**
 * Creates and returns shared pointer to a new empty geometry
 */
GeometrySP Geometry::makeSP() {

    return std::make_shared<Geometry>();
}

/**
 * Default constructor
 */
Geometry::Geometry():
    mHandleIndices{0},
    mUpdateIndices{true} {

    // Creates indices buffer
    glGenBuffers(1, &mHandleIndices);
}

/**
 * Destructor
 */
Geometry::~Geometry() {

    logDebug("~Geometry()");
    // Delete VBOs
    for (Vbo* vbo: mVbos) {
        delete vbo;
    }

    // Delete VAOs from all contexts
    for (auto& ctx: mContexts) {
        if (ctx.mHandleVao > 0) {
            glDeleteVertexArrays(1, &ctx.mHandleVao);
        }
    }
    mContexts.clear();

    // Delete indices buffer
    if (mHandleIndices > 0) {
        glDeleteBuffers(1, &mHandleIndices);
        mHandleIndices = 0;
    }
}

/**
 * Adds a VBO to this geometry
 * Assumes ownership of the VBO
 * @param vbo unique pointer to VBO to add
 */
void Geometry::addVbo(Vbo* vbo) {

    // If VBO is already in this geometry, ignore
    for (size_t idx = 0; idx < mVbos.size(); idx++) {
        if (mVbos[idx] == vbo) {
            return;
        }
    }
    mVbos.push_back(vbo);
}

/**
 * Builds and adds a VBO to this geometry
 * @param name pointer to VBO attribute name
 * @param size VBO item size
 * @param buf pointer to buffer with VBO data
 */
void Geometry::addVbo(const char* name, int itemSize, std::vector<float>& buf) {

    auto vbo = new Vbo();
    vbo->addAttrib(name, itemSize);
    vbo->setBuffer(&buf);
    addVbo(vbo);
}

/**
 * Sets or replace this geometry indices with the specified array.
 * The passed array is moved to the geometry and cleared.
 * @param indices array with indices to set
 */
void Geometry::setIndices(std::vector<unsigned int>& indices) {

    mIndices = std::move(indices);
    mUpdateIndices = true;
}

/**
 * Sets or replace this geometry indices with a copy of the specified array.
 * The passed array is not changed.
 * @param array array with indices buffer to move to this geometry
 */
void Geometry::setIndices(const GLuint* buf, size_t len) {

    mIndices.assign(buf, buf + len);
    mUpdateIndices = true;
}

/**
 * Returns reference to VBO with specified attribute name
 * @param name name of the VBO attribute
 * @return pointer to VBO or NULL if not found
 */
Vbo* Geometry::vbo(const char* name) {

    for (auto* vbo: mVbos) {
        int size = vbo->attribSize(name);
        if (size >= 0) {
            return vbo;
        }
    }
    return nullptr;
}

/**
 * Returns pointer to VBO at the specified index
 * Returns nullptr if supplied index is invalid
 */
Vbo* Geometry::vboAt(size_t index) {

    if (index >= mVbos.size()) {
        return nullptr;
    }
    return mVbos[index];
}

/**
 * Returns the number indices elements in the geometry
 * @return number of indices
 */
size_t Geometry::indicesCount() {

    return mIndices.size();
}

/**
 * Returns the number of items in the first VBO 
 * An item is a complete group of attributes in the VBO buffer
 * The number of items should be the same for all VBOs
 * @return number of items in the VBOs
 */
size_t Geometry::itemsCount() {

    if (mVbos.empty()) {
        return 0;
    }
    return mVbos[0]->itemsCount();
}

/**
 * Transfer geometry data if necessary
 * @param ri pointer to current RenderInfo
 * @param prog pointer to current program object
 */
void Geometry::transfer(const RenderInfo& ri, Program *prog) {

    // Look for current context to get the VAO to use
    GLuint vao = 0;
    for (auto& ci: mContexts) {
        if (ci.mContext == ri.ctx) {
            vao = ci.mHandleVao;
            break;
        }
    }

    // If current context not found, creates a new VAO for this context
    bool initVBO = false;
    if (vao == 0) {
        ContextInfo ci{ri.ctx};
        glGenVertexArrays(1, &ci.mHandleVao);
        mContexts.push_back(ci);
        vao = ci.mHandleVao;
        initVBO = true;
    }
    glBindVertexArray(vao);

    // Transfer VBOs if necessary
    for (auto& vbo: mVbos) {
        if (initVBO) {
            if (!mIndices.empty()) {
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mHandleIndices);
            }
            vbo->init(prog);
        }
        vbo->transfer(ri, prog);
    }

    // Update indices buffer if necessary
    if (!mIndices.empty() > 0 && mUpdateIndices) {
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mHandleIndices);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                mIndices.size() * sizeof(GL_UNSIGNED_INT),
                mIndices.data(), GL_STATIC_DRAW);
		mUpdateIndices = false;
    }
}

} // namespace

