#pragma once
#include <memory>
#include "glad/glad.h"
#include "renderinfo.h"
#include "image.h"
#include "font.h"

namespace glp {

class Texture2D;
using Texture2DSP = std::shared_ptr<Texture2D>;

class Texture2D {

    public:
        static Texture2DSP make();
        Texture2D();
        ~Texture2D();
        Texture2D(const Texture2D&) = delete;
        Texture2D& operator=(const Texture2D&) = delete;

        void setImage(ImageSP img);
        const char* setImageFromFile(const char* filepath);
        void renderText(const std::string& text, FontSP font, int imgx = 0, std::vector<Font::Coord> *coords = nullptr);
        void setMagFilter(int magFilter);
        void setMinFilter(int minFilter);
        void setWrapS(int wrapS);
        void setWrapT(int wrapT);
        void setRepeat(float rx, float ry);
        void setOffset(float ox, float oy);
        void setUnit(GLenum unit);
        void transfer(GLint locSampler, GLint locInfo);
        int  width() const;
        int  height() const;
        int  magFilter() const;
        int  minFilter() const;
        int  wrapS() const;
        int  wrapT() const;
        void repeat(float* rx, float* ry) const;
        void offset(float* ox, float* oy) const;

    private:
        GLuint          mTexname;
        GLenum          mUnit;
        GLenum          mMagFilter;
        GLenum          mMinFilter;
        GLenum          mWrapS;
        GLenum          mWrapT;
        struct udata {
            float       offsetX;
            float       offsetY;
            float       repeatX;
            float       repeatY;
        }               mUdata;
        GLint           mIformat;                // internal format
        GLsizei         mWidth;                  // texture width in pixels
        GLsizei         mHeight;                 // texture height in pixels
        GLenum          mFormat;                 // format of the pixel data
        GLenum          mFormatType;             // type of the pixel data
        bool            mSendParams;             // texture parameters needs to be sent
        bool            mGenMipmaps;             // generate mipmaps flag
};


} // namespace

