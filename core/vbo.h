#pragma once
#include <vector>
#include <string>
#include "program.h"
#include "renderinfo.h"

namespace glp {

class Vbo {

    public:
        Vbo();
        ~Vbo();
        void addAttrib(const char* name, int itemSize);
        size_t attribCount() const;
        int attribSize(const char* name) const; 
        size_t stride() const;
        size_t itemsCount() const;
        std::vector<float>* buffer();
        void setBuffer(std::vector<float>* buf);
        void update();
        void init(Program* prog);
        void transfer(const RenderInfo& ri, Program *prog);

    private:
        GLuint  mHandle;
        bool    mUpdate;
        GLenum  mUsage;
        std::vector<float>* mBuf;
        struct Attrib {
            std::string name;
            int  itemSize;
        };
        std::vector<Attrib> mAttribs;
};

} // namespace

