#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stdexcept>
#include <iostream>
#include "log.h"
#include "events.h"
#include "display.h"

namespace glp {

/**
 * Creates and returns shared pointer to a new Display
 * @param width display width in pixels
 * @param height display height in pixels
 * @return shared pointer to Display
 */
DisplaySP Display::make(int width, int height, const char* title, bool fullScreen, DisplaySP share) {

    return std::make_shared<Display>(width, height, title, fullScreen, share);
}

/**
 * Constructs Display with the specified size
 * Throws exception if error
 * @param width display width in pixels
 * @param height display height in pixels
 * @param title initial title
 */
Display::Display(int width, int height, const char *title, bool fullScreen, DisplaySP share) {

    // If full screen required get primary monitor
    GLFWmonitor* monitor = nullptr;
    if (fullScreen) {
        monitor = glfwGetPrimaryMonitor();
    }

    // Sets pointer to optional window to share context
    GLFWwindow* wshare = nullptr;
    if (share) {
        wshare = share->mWindow;
    }

    // Creates window and context
    mWindow = glfwCreateWindow(width, height, title, monitor, wshare);
    if (!mWindow) {
        glfwTerminate();
        throw std::runtime_error("Error creating GLFW window");
    }

    // Make the window's context current
    glfwMakeContextCurrent(mWindow);

    // Load OpenGL functions using glad
    if (!gladLoadGL()) {
        throw std::runtime_error("Error loading OpenGL functions");
    }
    #ifdef GLAD_DEBUG
        logInfo("Using GLAD Debug Version"); 
    #endif

    // Sets defaults
    glEnable(GL_POLYGON_OFFSET_FILL);
    glEnable(GL_POLYGON_OFFSET_LINE);
    glEnable(GL_POLYGON_OFFSET_POINT);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);     // default is GL_LESS
    glDepthMask(true);
    glFrontFace(GL_CCW);
    //glCullFace(GL_BACK);
    //glEnable(GL_CULL_FACE);

    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnable(GL_PROGRAM_POINT_SIZE);
    glEnable(GL_MULTISAMPLE);

    // Sets the initial background color
    glClearColor(0.5, 0.5, 0.5, 1);

    // Sets pointer to display
    glfwSetWindowUserPointer(mWindow, this);
    // Set callbacks
    setCallbacks();

    glfwSwapInterval(1);
}

/**
 * Destructor
 */
Display::~Display() {

    logDebug("~Display()");
    if (mWindow) {
        glfwDestroyWindow(mWindow);
        mWindow = nullptr;
    }
}

/**
 * Sets the title of this display
 * @param title
 */
void Display::setTitle(const std::string &title) {

    glfwSetWindowTitle(mWindow, title.c_str());
}

/**
 * Closes this display
 * After this call the display cannot be used anymore
 */
void Display::close() {

    glfwSetWindowShouldClose(mWindow, GLFW_TRUE);
}

void Display::makeContextCurrent() {

    glfwMakeContextCurrent(mWindow);
}

/**
 * Returns if this display has the current context
 * @return
 */
bool Display::isCurrentContext() {

    return glfwGetCurrentContext() == mWindow;
}

/**
 * Swaps the front and back buffers of the specified window
 * If the specified interval is greater than 0, the GPU drivers
 * waits the specified number of screen updates before swapping the buffers.
 */
void Display::swapBuffers(void) {

    glfwSwapBuffers(mWindow);
}

/**
 * Returns the size of this window in pixels
 *
 * @param width reference to store width
 * @param height reference to store height
 */
void Display::size(int& width, int& height) const {

    glfwGetWindowSize(mWindow, &width, &height);
}

/**
 * Sets the size of this window in screen coordinated
 *
 * @param width window width
 * @param height window height
 */
void Display::setSize(int width, int height) {

    glfwSetWindowSize(mWindow, width, height);
}

void Display::frameBufferSize(int& width, int& height) {

    glfwGetFramebufferSize(mWindow, &width, &height);
}

/**
 * Returns this window position in screen coordinates
 *
 * @param xpos reference to store the x coordinate
 * @param ypos reference to store the y coordinate
 */
void Display::pos(int &xpos, int &ypos) const {

    glfwGetWindowPos(mWindow, &xpos, &ypos);
}

/**
 * Set this display position in screen coordinates
 * @param xpos position x coordinate
 * @param ypos position y coordinate
 */
void Display::setPos(int xpos, int ypos) {

    glfwSetWindowPos(mWindow, xpos, ypos);
}

/**
 * Sets this display title
 * @param title string with title to set
 */
void Display::setTitle(std::string title) {

    glfwSetWindowTitle(mWindow, title.c_str());
}

/**
 * Returns if this display should close
 * @return flag indicating if this window should close
 */
bool Display::shouldClose() {

    return glfwWindowShouldClose(mWindow);
}


/**
 * Called by GLFW with key event
 */
void Display::keyCB(GLFWwindow *win, int key, int scancode, int action, int mods) {

    auto* self = static_cast<Display *>(glfwGetWindowUserPointer(win));
    self->dispatch(KeyEvent(key, scancode, action, mods));
}

/**
 * Called by GLFW with char event
 */
void Display::charCB(GLFWwindow *win, unsigned int codepoint) {

    auto self = static_cast<Display *>(glfwGetWindowUserPointer(win));
    CharEvent ev{codepoint};
    self->dispatch(ev);
}

/**
 * Called by GLFW with cursor position change event
 * @param win
 * @param xpos
 * @param ypos
 */
void Display::cursorCB(GLFWwindow *win, double xpos, double ypos) {

    auto self = static_cast<Display*>(glfwGetWindowUserPointer(win));
    CursorPosEvent ev{xpos, ypos};
    self->dispatch(ev);
}

/**
 * Called by GLFW with mouse button event
 * @param win
 * @param button
 * @param action
 * @param mods
 */
void Display::mouseButtonCB(GLFWwindow *win, int button, int action, int mods) {

    auto self = static_cast<Display*>(glfwGetWindowUserPointer(win));
    // Get current cursor pos to add to event data
    double xpos, ypos;
    glfwGetCursorPos(self->mWindow, &xpos, &ypos);
    self->dispatch(MouseButtonEvent{button, action, mods, xpos, ypos});
}

/**
 * Called by GLFW with scroll event
 * @param win
 * @param xoffset
 * @param yoffset
 */
void Display::scrollCB(GLFWwindow* win, double xoffset, double yoffset) {

    auto self = static_cast<Display*>(glfwGetWindowUserPointer(win));
    ScrollEvent ev{xoffset, yoffset};
    self->dispatch(ev);
}

/**
 * Called by GLFW with window size changed event
 * @param win
 * @param width
 * @param height
 */
void Display::sizeCB(GLFWwindow *win, int width, int height) {

    auto self = static_cast<Display*>(glfwGetWindowUserPointer(win));
    SizeEvent ev{width, height};
    self->dispatch(ev);
}

void Display::setCallbacks() {

    glfwSetKeyCallback(mWindow, keyCB);
    glfwSetCharCallback(mWindow, charCB);
    glfwSetCursorPosCallback(mWindow, cursorCB);
    glfwSetMouseButtonCallback(mWindow, mouseButtonCB);
    glfwSetScrollCallback(mWindow, scrollCB);
    glfwSetWindowSizeCallback(mWindow, sizeCB);
}

} // namespace
