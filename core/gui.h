#pragma once
#include <vector>
#include <memory>
#include <glad/glad.h>
#include "display.h"
#include "panel.h"

namespace glp {

class Gui {

public:
    static std::shared_ptr<Gui> get();
    ~Gui();
    DisplaySP newDisplay(int width, int height, const char* title, bool fullScreen=false);
    void displayPanel(const DisplaySP &display, PanelSP panel, bool resize = true);
    DisplaySP currentContext();
    void run();
    void renderFrame();
    int  displays();
    void close();
    void setKeyFocus(Panel* p);
    void releaseKeyFocus(Panel* p);
    //void setTheme(themeSP theme) { mTheme = theme; }
    //themeSP theme() {return mTheme;};

private:
    static std::shared_ptr<Gui> mInstance;
    //themeSP mTheme;
    double mUpdateTime;
    struct DisplayInfo {
        DisplaySP   mDisplay;
        PanelSP     mPanel;
        bool        mResize;
        std::vector<int> mSubs;
        explicit DisplayInfo(DisplaySP d): mDisplay{d}, mPanel{nullptr}, mResize{false} {}
    };
    std::vector<DisplayInfo> mDisplays;
    std::vector<Panel*> mTargets;
    Panel* mKeyFocus;
    Panel* mMouseFocus;

private:
    Gui();
    const DisplayInfo* displayInfo(Display* d);
    bool renderDisplay(const DisplayInfo &di);
    void deleteDisplay(DisplayInfo &di);
    void registerListeners(DisplayInfo &di);
    void onKey(Display* d, const Event& ev);
    void onChar(Display* d, const Event& ev);
    void onMouseButton(Display* d, const Event& ev);
    void onCursorPos(Display* d, const Event& ev);
    void onScroll(Display* d, const Event& ev);
    void onSize(Display* d, const Event& ev);
    void checkPanel(float x, float y, const Event& ev, Panel* panel);
    void sendPanels(float x, float y, const Event& ev, Panel* panel);
};

} // namespace
