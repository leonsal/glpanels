#pragma once
#include <map>
#include <string>
#include <vector>
#include <functional>
#include "events.h"

namespace glp {

class Dispatcher {

    public:
        using CB = std::function<void(const Event& ev)>;

        Dispatcher();
        ~Dispatcher() = default;
        Dispatcher(const Dispatcher& other) = delete;
        Dispatcher& operator=(const Dispatcher& other) = delete;

        void cancelDispatch();
        int  listen(const char* evname, CB cb);
        void dispatch(const Event& ev);
        bool unlisten(int subid);

    private:
        static int mNextSubid;
        struct Subs {
            CB cb;
            int id;
        };
        std::map<const char*, std::vector<Subs>> mSubs;
        bool mCancelNotify;
};

} // namespace
