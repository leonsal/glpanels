#pragma once
#include <string>
#include <map>
#include <glad/glad.h>

namespace glp {

class Program;
using ProgramUP = std::unique_ptr<Program>;

class Program {

    public:
        static ProgramUP makeUP();
        Program() noexcept;
        ~Program();
        Program(const Program&) = delete;
        Program& operator=(const Program&) = delete;

        const char* addShader(GLenum stype, const char* source);
        const char* build();
        GLint getUniformLocation(const std::string& name);
        std::string log();
        GLuint handle();

    private:
        const char* compileShader(GLenum shaderType, const std::string& text, GLuint *handle);
        std::string numberLines(const std::string& text);

    private:
        GLuint      mHandle;         // program handle
        GLuint      mVertexHandle;   // vertex shader handle
        GLuint      mFragHandle;     // fragment shader handle
        GLuint      mGeomHandle;     // geometry shader handle
        std::string mVertex;         // vertex shader source
        std::string mFrag;           // fragment shader source
        std::string mGeom;           // geometry shader source
        std::string mLog;            // compilation log
        std::map<std::string,GLint>  mAttribs;    // attributes location cache
        std::map<std::string,GLint>  mUniforms;   // uniform location cache
};

} // namespace
