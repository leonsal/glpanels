#pragma once
#include <string>

namespace glp {

    std::string utf8String(int codepoint, bool trow=true);
    std::size_t utf8Len(const std::string& s, bool trow=true);
    std::size_t utf8Char(const std::string& s, std::size_t pos, std::size_t& len, bool trow=true);
    int utf8Remove(std::string& s, std::size_t pos, bool trow=true);
    int utf8Insert(std::string& s, std::size_t pos, const std::string& data, bool trow=true);
    std::string utf8Prefix(const std::string& s, std::size_t pos, bool trow=true);

}

