#pragma once
#include <string>
#include <memory>
#include "ft2build.h"
#include FT_FREETYPE_H
#include "image.h"
#include "vec4.h"


namespace gui {

class FontAtlas;
using FontAtlasSP = std::shared_ptr<FontAtlas>;

class FontAtlas  {

    public:
        Font();
        ~Font();

    private:
        FT_Face mFace;              // freetype face handle
};


} // namespace

