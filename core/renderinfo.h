#pragma once

namespace glp {

typedef void* Context;  // opaque pointer to identify context

struct RenderInfo {
    Context ctx;        // pointer to GLFWwindow with current context
    int viewWidth;      // current view port width in pixels
    int viewHeight;     // current view port height in pixels
    int fbWidth;        // current framebuffer width in pixels
    int fbHeight;       // current framebuffer height in pixels
};

} // namespace

