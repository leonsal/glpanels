#pragma once
#include <string>
#include <memory>
#include <vector>
#include "ft2build.h"
#include FT_FREETYPE_H
#include "image.h"
#include "glm/ext/vector_float4.hpp"

namespace glp {

class Font;
using FontSP = std::shared_ptr<Font>;

class Font  {

    public:
        struct Coord {
            float x;    // glyph start coordinate in pixels from left
            float y;    // glyph baseline coordinate in pixels from top
            Coord(float vx, float vy): x{vx}, y{vy} {}
        };

    public:
        static FontSP make(const std::string& filename);
        static FontSP make(const unsigned char* data, int len);
        Font();
        ~Font();
        Font(const Font&) = delete;
        Font& operator=(const Font&) = delete;

        Font& setColor(const glm::vec4& color);
        void  setSize(float size);
        void  setDPI(float dpi);
        Font& setLineSpacing(float spacing);
        float size() const;
        int   height() const;
        FT_Size_Metrics metrics() const;
        float lineSpacing() const;
        const char* loadGlyph(int codepoint, int flags);
        FT_GlyphSlot getGlyph();
        void  measure(const std::string& text, int& width, int& height) const;
        void  render(ImageSP img, int x, int y, const std::string& text, std::vector<Coord>* coords=nullptr);

    protected:
        static const char* errorStr(int errcode);
        void drawBitmap1(const ImageSP& img, FT_Bitmap* bm, int x, int y);
        void drawBitmap4(const ImageSP& img, FT_Bitmap* bm, int x, int y);
        void drawCaret(Image& img, int x, int y);
        static float mix(float x, float y, float a);
        FT_Face     mFace;              // freetype face handle
        float       mSize;              // font size in points
        float       mDPI;               // font Dots Per Inch
        float       mLineSpacing;       // line spacing factor
        glm::vec4   mColor;             // font color in RGBA
};

} // namespace

