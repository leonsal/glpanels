#include <stdexcept>
#include <iostream>
#include "log.h"
#include "gui.h"
#include "renderinfo.h"
#include "events.h"

namespace glp {

/**
 * Returns single Gui instance
 * @return  single Gui instance
 */
std::shared_ptr<Gui> Gui::get() {

    if (mInstance == nullptr) {
        mInstance.reset(new Gui());
    }
    return mInstance;
}

/**
 * Called by GLFW when an error occurs
 * @param error
 * @param description
 */
static void glfw_error_callback(int error, const char *description) {

    throw std::runtime_error(std::string("GLFW Error: ") + description);
}

/**
 * Private constructor
 */
Gui::Gui(): mUpdateTime{}, mMouseFocus{}, mKeyFocus{} {

    // Sets callback for GLFW errors
    glfwSetErrorCallback(glfw_error_callback);

    // Initialize GLFW
    if (glfwInit() == GLFW_FALSE) {
        throw std::runtime_error("Error initializing GLFW");
    }

    // Requests specific OpenGL version
    const int vmajor = 3;
    const int vminor = 3;
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, vmajor);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, vminor);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_SAMPLES, 4);


    //setTheme(ThemeLight::create());
}

/**
 * Destructor
 */
Gui::~Gui() {

    logDebug("~Gui()");
    close();
    glfwTerminate();
}

/**
 * Creates and returns a new Display
 * The created display will be rendered by the Gui render loop
 * All displays created after the first one share the same OpenGL
 * context created by the first display.
 * @param width display width in pixels
 * @param height display height in pixels
 * @param title initial display title
 * @param fullScreen true to set to full screen
 * @return pointer to created Display
 */
DisplaySP Gui::newDisplay(int width, int height, const char *title, bool fullScreen) {

    // If the new display is not the first, it should share the context with the first
    DisplaySP dshare = nullptr;
    if (!mDisplays.empty()) {
        dshare = mDisplays.front().mDisplay;
    }

    // Creates new display
    auto d = Display::make(width, height, title, fullScreen, dshare);
    mDisplays.emplace_back(d);
    registerListeners(mDisplays.back());
    return d;
}

/**
 * Sets the panel to be rendered by the specified display
 * @param display pointer to display
 * @param panel pointer to panel to render
 * @param resize if true resize the panel with the display size
 */
void Gui::displayPanel(const DisplaySP &display, PanelSP panel, bool resize) {

    for (auto& di: mDisplays) {
        if (di.mDisplay == display) {
            di.mPanel = panel;
            di.mResize = resize;
            // If panel is valid and to be resized
            if (di.mPanel && di.mResize) {
                int width, height;
                di.mDisplay->size(width, height);
                di.mPanel->setSize(width, height);
            }
            return;
        }
    }
    throw std::runtime_error("Invalid display for setPanel");
}

/**
 * Returns the display with the current OpenGL context or nullptr if no current context
 * (No display created)
 * @return  shared pointer to Display or nullptr
 */
DisplaySP Gui::currentContext() {

    for (const auto& d: mDisplays) {
        if (d.mDisplay->isCurrentContext()) {
            return d.mDisplay;
        }
    }
    return nullptr;
}

/**
 * Runs the application main loop rendering all displays
 * Terminates when all displays are closed
 */
void Gui::run() {

    mUpdateTime = glfwGetTime();
    while (!mDisplays.empty()) {
        renderFrame();
    }
}

void Gui::renderFrame() {

    auto it = mDisplays.begin();
    while (it != mDisplays.end()) {
        auto closed = renderDisplay(*it);
        if (closed) {
            deleteDisplay(*it);
            it = mDisplays.erase(it);
            continue;
        }
        it++;
        glfwPollEvents();
    }
}

/**
 * Returns the current number of displays
 * @return
 */
int Gui::displays() {

    return mDisplays.size();
}

/**
 * Render one frame for the specified display
 * @param rt
 * @return true if display should be closed, false otherwise
 */
bool Gui::renderDisplay(const DisplayInfo &di) {

    // Checks if display should be closed
    if (di.mDisplay->shouldClose()) {
        return true;
    }

    // Make the window's context current
    di.mDisplay->makeContextCurrent();

    // Set fields of RenderInfo to be passed to all panels
    RenderInfo ri{};
    ri.ctx = di.mDisplay.get();
    di.mDisplay->size(ri.viewWidth, ri.viewHeight);
    di.mDisplay->frameBufferSize(ri.fbWidth, ri.fbHeight);
    glViewport(0, 0, ri.viewWidth, ri.viewHeight);

    // Clear the current frame
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // Render root panel for this display and all of its children
    if (di.mPanel) {
        auto start = glfwGetTime();
        di.mPanel->renderAll(ri);
        auto stop = glfwGetTime();
        auto elapsed = stop - start;
        if (stop - mUpdateTime >= 1.0) {
            logInfo("cpu rendering time (milliseconds):{:.4f}", elapsed * 1000.0);
            mUpdateTime = stop;
        }
    }

    // Swap front and back buffers
    di.mDisplay->swapBuffers();
    return false;
}

void Gui::deleteDisplay(glp::Gui::DisplayInfo &di) {

    // Remove all gui listeners from this display
    for (auto& subid: di.mSubs) {
        di.mDisplay->unlisten(subid);
    }
}

/**
 * Close all displays
 */
void Gui::close() {

    mDisplays.clear();
}

/**
 * Sets the panel with the key focus.
 * Panel should use releaseKeyFocus() to remove the key focus.
 * @param p
 */
void Gui::setKeyFocus(Panel* p) {

    if (mKeyFocus != nullptr) {
        if (mKeyFocus == p) {
            return;
        }
        mKeyFocus->onEvent(Event{OnLostKeyFocus});
    }
    mKeyFocus = p;
}

/**
 * Release the key focus for the specified panel
 * @param p
 */
void Gui::releaseKeyFocus(Panel* p) {

    if (mKeyFocus == p) {
        mKeyFocus = nullptr;
    }
}

/**
 * Returns DisplayInfo associated with the specified display or nullptr if not found
 * @param d
 * @return
 */
const Gui::DisplayInfo* Gui::displayInfo(Display* d) {

    for (auto& di: mDisplays) {
        if (di.mDisplay.get() == d) {
            return &di;
        }
    }
    return nullptr;
}

// Initialize static pointer to singleton instance
std::shared_ptr<Gui> Gui::mInstance = nullptr;

// Register event listeners for the specified display
void Gui::registerListeners(DisplayInfo& di) {

    int id;
    auto display = di.mDisplay.get();

    // Listen to OnKeyDown events
    id = display->listen(OnKey, [this, display](const Event& ev){
        onKey(display, ev);
    });
    di.mSubs.push_back(id);

    // Listen to OnChar events
    id = display->listen(OnChar, [this, display](const Event& ev) {
        onChar(display, ev);
    });
    di.mSubs.push_back(id);

    // Listen to OnMouseDown events
    id = display->listen(OnMouseButton, [this, display](const Event& ev) {
        onMouseButton(display, ev);
    });
    di.mSubs.push_back(id);

    // Listen to OnCursorPos events
    id = display->listen(OnCursorPos, [this, display](const Event& ev) {
        onCursorPos(display, ev);
    });
    di.mSubs.push_back(id);

    // Listen to OnScroll events
    id = display->listen(OnScroll, [this, display](const Event& ev) {
        onScroll(display, ev);
    });
    di.mSubs.push_back(id);

    // Listen to window size events
    id = display->listen(OnSize, [this, display](const Event& ev) {
        onSize(display, ev);
    });
    di.mSubs.push_back(id);
}

/**
 * Process key events received from the specified display
 * @param d
 * @param ev
 */
void Gui::onKey(Display* d, const Event& ev) {

    // If no panel has the key focus, nothing to do
    if (mKeyFocus == nullptr) {
        return;
    }

    // Sends event to focused panel only
    mKeyFocus->onEvent(ev);
}

/**
 * Process char events received from the specified display
 * @param d
 * @param ev
 */
void Gui::onChar(Display* d, const Event& ev) {

    // If no panel has the key focus, nothing to do
    if (mKeyFocus == nullptr) {
        return;
    }

    // Sends event to focused panel only
    mKeyFocus->onEvent(ev);
}

/**
 * Process mouse button event from the specified display
 * @param d naked pointer to Display
 * @param ev mouse button event
 */
void Gui::onMouseButton(Display* d, const Event& ev) {

    auto di = displayInfo(d);
    if (di == nullptr) {
        return;
    }
    auto mev = static_cast<const MouseButtonEvent&>(ev);
    sendPanels(mev.xpos, mev.ypos, ev, di->mPanel.get());
}

/**
 * Process cursor position events from the specified display
 * @param d
 * @param ev
 */
void Gui::onCursorPos(Display* d, const Event& ev) {

    auto di = displayInfo(d);
    if (di == nullptr) {
        return;
    }
    auto cev = static_cast<const CursorPosEvent&>(ev);
    sendPanels(cev.xpos, cev.ypos, ev, di->mPanel.get());

}

/**
 * Process scroll wheel events from the specified display
 * @param d
 * @param ev
 */
void Gui::onScroll(Display* d, const Event& ev) {

}

/**
 * Process display resize events
 * @param d
 * @param evname
 * @param ev
 */
void Gui::onSize(Display* d, const Event& ev) {

    auto di = displayInfo(d);
    if (di == nullptr) {
        return;
    }
    auto sev = static_cast<const SizeEvent&>(ev);
    std::cout << "width:" << sev.width << " height:" << sev.height << "\n";
    if (di->mPanel && di->mResize) {
        di->mPanel->setSize(sev.width, sev.height);
    }
}

/**
 * Checks recursively if the specified panel or any of its children
 * contains the specified event coordinate.
 * If it contains, inserts the panel in the mTargets vector
 * @param x event x coordinate in pixels
 * @param y event y coordinate in pixels
 * @param panel pointer to panel to check
 */

void Gui::checkPanel(float x, float y, const Event& ev, Panel* panel) {

    // If panel not visible or enabled, ignore
    if (!panel->visible() || !panel->enabled()) {
        return;
    }

    // If this panel contains the event position appends it to targets list
    bool found = panel->insideBorders(x, y);
    if (found) {
        mTargets.push_back(panel);
    } else {
        // If OnCursorEnter previously sent, sends OnCursorLeave with a nil event
        if (panel->cursorOver()) {
            panel->setCursorOver(false);
            panel->onEvent(Event{OnCursorLeave});
        }
        // If mouse button was pressed, sends event informing mouse down outside of the panel
        if (ev.name == OnMouseButton) {
            panel->onEvent(Event{OnMouseOut});
        }
    }

    // Checks if any of its children also contains the position
    for (const auto& child: panel->children()) {
        checkPanel(x, y, ev, child.get());
    }
}

/**
 * Sends mouse button and cursor position events to panels
 * which contains the specified screen position
 * @param x event x coordinate in pixels
 * @param y event y coordinate in pixels
 * @param evname pointer to event name
 * @param ev reference to event to send
 */
void Gui::sendPanels(float x, float y, const Event& ev, Panel* panel) {

    // If there is a panel with the mouse focus sends only to this panel
    if (mMouseFocus != nullptr) {
        mMouseFocus->onEvent(ev);
        return;
    }

    // Checks all children of this root node
    mTargets.clear();
    for (const auto& child: panel->children()) {
        checkPanel(x, y, ev, child.get());
    }

    // No panels found
    if (mTargets.empty()) {
        // If event is mouse click, removes the keyboard focus
        if (ev.name == OnMouseButton) {
            setKeyFocus(nullptr);
        }
        return;
    }

    // Sorts target panels by absolute Z with the most foreground panels (lower Z) first
    // and sends event to all panels or until a stop is requested
    std::sort(std::begin(mTargets), std::end(mTargets), [](const Panel* a, const Panel* b) -> bool {
        return (a->z() < b->z());
    });

    // Send events to target panels from front to back
    for (auto& p : mTargets) {
        bool stop;
        // Cursor position event
        if (ev.name == OnCursorPos) {
            stop = p->onEvent(ev);
            if (!p->cursorOver()) {
                p->setCursorOver(true);
                p->onEvent(Event{OnCursorEnter});
            }
        // Mouse button event
        } else {
            stop = p->onEvent(ev);
        }
        // Checks if target panel requested stop propagation
        if (stop) {
            break;
        }
    }
}

} // namespaces
