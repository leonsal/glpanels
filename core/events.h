#pragma once
#include <GLFW/glfw3.h>

namespace glp {

// Display event names
extern const char *const OnKey;
extern const char *const OnChar;
extern const char *const OnCursorPos;
extern const char *const OnMouseButton;
extern const char *const OnScroll;
extern const char *const OnSize;

// Other event names
extern const char *const OnClick;
extern const char *const OnCursorEnter;
extern const char *const OnCursorLeave;
extern const char *const OnEnable;
extern const char *const OnMouseOut;
extern const char *const OnResize;
extern const char *const OnChange;
extern const char *const OnChild;          // child added to or removed from panel
extern const char *const OnRadioGroup;     // radio button from a group changed state
extern const char *const OnLostKeyFocus;

enum Keys {
    // Printable keys
    KeySpace          = GLFW_KEY_SPACE,
    KeyApostrophe     = GLFW_KEY_APOSTROPHE,
    KeyComma          = GLFW_KEY_COMMA,
    KeyMinus          = GLFW_KEY_MINUS,
    KeyPeriod         = GLFW_KEY_PERIOD,
    KeySlash          = GLFW_KEY_SLASH,
    Key0              = GLFW_KEY_0,
    Key1              = GLFW_KEY_1,
    Key2              = GLFW_KEY_2,
    Key3              = GLFW_KEY_3,
    Key4              = GLFW_KEY_4,
    Key5              = GLFW_KEY_5,
    Key6              = GLFW_KEY_6,
    Key7              = GLFW_KEY_7,
    Key8              = GLFW_KEY_8,
    Key9              = GLFW_KEY_9,
    KeySemicolon      = GLFW_KEY_SEMICOLON,
    KeyEqual          = GLFW_KEY_EQUAL,
    KeyA              = GLFW_KEY_A,
    KeyB              = GLFW_KEY_B,
    KeyC              = GLFW_KEY_C,
    KeyD              = GLFW_KEY_D,
    KeyE              = GLFW_KEY_E,
    KeyF              = GLFW_KEY_F,
    KeyG              = GLFW_KEY_G,
    KeyH              = GLFW_KEY_H,
    KeyI              = GLFW_KEY_I,
    KeyJ              = GLFW_KEY_J,
    KeyK              = GLFW_KEY_K,
    KeyL              = GLFW_KEY_L,
    KeyM              = GLFW_KEY_M,
    KeyN              = GLFW_KEY_N,
    KeyO              = GLFW_KEY_O,
    KeyP              = GLFW_KEY_P,
    KeyQ              = GLFW_KEY_Q,
    KeyR              = GLFW_KEY_R,
    KeyS              = GLFW_KEY_S,
    KeyT              = GLFW_KEY_T,
    KeyU              = GLFW_KEY_U,
    KeyV              = GLFW_KEY_V,
    KeyW              = GLFW_KEY_W,
    KeyX              = GLFW_KEY_X,
    KeyY              = GLFW_KEY_Y,
    KeyZ              = GLFW_KEY_Z,
    KeyLeftBracket    = GLFW_KEY_LEFT_BRACKET,
    KeyBackslash      = GLFW_KEY_BACKSLASH,
    KeyRightBracket   = GLFW_KEY_RIGHT_BRACKET,
    KeyGraveAccent    = GLFW_KEY_GRAVE_ACCENT,
    KeyWorld1         = GLFW_KEY_WORLD_1,
    KeyWorld2         = GLFW_KEY_WORLD_2,
    // Function keys
    KeyEscape         = GLFW_KEY_ESCAPE,
    KeyEnter          = GLFW_KEY_ENTER,
    KeyTab            = GLFW_KEY_TAB,
    KeyBackspace      = GLFW_KEY_BACKSPACE,
    KeyInsert         = GLFW_KEY_INSERT,
    KeyDelete         = GLFW_KEY_DELETE,
    KeyRight          = GLFW_KEY_RIGHT,
    KeyLeft           = GLFW_KEY_LEFT,
    KeyDown           = GLFW_KEY_DOWN,
    KeyUp             = GLFW_KEY_UP,
    KeyPage_up        = GLFW_KEY_PAGE_UP,
    KeyPage_down      = GLFW_KEY_PAGE_DOWN,
    KeyHome           = GLFW_KEY_HOME,
    KeyEnd            = GLFW_KEY_END,
    KeyCapsLock       = GLFW_KEY_CAPS_LOCK,
    KeyScrollLock     = GLFW_KEY_SCROLL_LOCK,
    KeyNumLock        = GLFW_KEY_NUM_LOCK,
    KeyPrintScreen    = GLFW_KEY_PRINT_SCREEN,
    KeyPause          = GLFW_KEY_PAUSE,
    KeyF1             = GLFW_KEY_F1,
    KeyF2             = GLFW_KEY_F2,
    KeyF3             = GLFW_KEY_F3,
    KeyF4             = GLFW_KEY_F4,
    KeyF5             = GLFW_KEY_F5,
    KeyF6             = GLFW_KEY_F6,
    KeyF7             = GLFW_KEY_F7,
    KeyF8             = GLFW_KEY_F8,
    KeyF9             = GLFW_KEY_F9,
    KeyF10            = GLFW_KEY_F10,
    KeyF11            = GLFW_KEY_F11,
    KeyF12            = GLFW_KEY_F12,
    KeyF13            = GLFW_KEY_F13,
    KeyF14            = GLFW_KEY_F14,
    KeyF15            = GLFW_KEY_F15,
    KeyF16            = GLFW_KEY_F16,
    KeyF17            = GLFW_KEY_F17,
    KeyF18            = GLFW_KEY_F18,
    KeyF19            = GLFW_KEY_F19,
    KeyF20            = GLFW_KEY_F20,
    KeyF21            = GLFW_KEY_F21,
    KeyF22            = GLFW_KEY_F22,
    KeyF23            = GLFW_KEY_F23,
    KeyF24            = GLFW_KEY_F24,
    KeyF25            = GLFW_KEY_F25,
    KeyKP0            = GLFW_KEY_KP_0,
    KeyKP1            = GLFW_KEY_KP_1,
    KeyKP2            = GLFW_KEY_KP_2,
    KeyKP3            = GLFW_KEY_KP_3,
    KeyKP4            = GLFW_KEY_KP_4,
    KeyKP5            = GLFW_KEY_KP_5,
    KeyKP6            = GLFW_KEY_KP_6,
    KeyKP7            = GLFW_KEY_KP_7,
    KeyKP8            = GLFW_KEY_KP_8,
    KeyKP9            = GLFW_KEY_KP_9,
    KeyKPDecimal      = GLFW_KEY_KP_DECIMAL,
    KeyKPDivide       = GLFW_KEY_KP_DIVIDE,
    KeyKPMultiply     = GLFW_KEY_KP_MULTIPLY,
    KeyKPSubtract     = GLFW_KEY_KP_SUBTRACT,
    KeyKPAdd          = GLFW_KEY_KP_ADD,
    KeyKPEnter        = GLFW_KEY_KP_ENTER,
    KeyKPEqual        = GLFW_KEY_KP_EQUAL,
    KeyLeftShift      = GLFW_KEY_LEFT_SHIFT,
    KeyLeftControl    = GLFW_KEY_LEFT_CONTROL,
    KeyLeftAlt        = GLFW_KEY_LEFT_ALT,
    KeyLeftSuper      = GLFW_KEY_LEFT_SUPER,
    KeyRightShift     = GLFW_KEY_RIGHT_SHIFT,
    KeyRightControl   = GLFW_KEY_RIGHT_CONTROL,
    KeyRightAlt       = GLFW_KEY_RIGHT_ALT,
    KeyRightSuper     = GLFW_KEY_RIGHT_SUPER,
    KeyMenu           = GLFW_KEY_MENU,
    KeyLast           = GLFW_KEY_LAST,
    // Key actions
    KeyRelease        = GLFW_RELEASE,
    KeyPress          = GLFW_PRESS,
    KeyRepeat         = GLFW_REPEAT,
    // Key modifiers
    ModShift          = GLFW_MOD_SHIFT,
    ModControl        = GLFW_MOD_CONTROL,
    ModAlt            = GLFW_MOD_ALT,
    ModSuper          = GLFW_MOD_SUPER,
    // Mouse buttons
    MouseButtonLeft    = GLFW_MOUSE_BUTTON_LEFT,
    MouseButtonRight   = GLFW_MOUSE_BUTTON_RIGHT,
    MouseButtonMiddle  = GLFW_MOUSE_BUTTON_MIDDLE
};

// Base Event contains only the event name.
// It has a virtual destructor so derived classes can use dynamic_cast<>
// to cast a base event pointer to a derived event.
struct Event {
    const char* name;           // pointer to event name
    Event() = delete;           // event must have a name
    virtual ~Event() = default;
    explicit Event(const char* name): name{name} {}
};

// Key press/repeat/release event
struct KeyEvent : public Event {
    int keycode;    // keyboard key
    int scancode;   // scancode
    int action;     // KeyPress | KeyRelease | KeyRepeat
    int mods;       // ModShift, ModControl, ModAlt, ModSuper, ...
    KeyEvent(int keycode, int scancode, int action, int mods):
        Event{OnKey}, keycode{keycode}, scancode{scancode}, action{action}, mods{mods} {}
};

// Char pressed event
struct CharEvent : public Event {
    unsigned int codepoint;  // keyboard key
    explicit CharEvent(unsigned int codepoint): Event{OnChar}, codepoint{codepoint} {}
};

// Cursor position event
struct CursorPosEvent : public Event {
    double xpos;    // x position in screen coordinates relative to the window
    double ypos;    // y position in screen coordinates relative to the window
    CursorPosEvent(double xpos, double ypos): Event{OnCursorPos}, xpos{xpos}, ypos{ypos} {}
};

// Mouse button event
struct MouseButtonEvent : public Event {
    int button;     // MouseButtonLeft|MouseButtonMiddle|MouseButtonRight
    int action;     // KeyRelease|KeyPress|KeyRepeat
    int mods;       // ModShift, ModControl, ModAlt, ModSuper, ...
    double xpos;    // x position in screen coordinates relative to the window
    double ypos;    // y position in screen coordinates relative to the window
    MouseButtonEvent(int button, int action, int mods, double xpos, double ypos):
        Event(OnMouseButton), button{button}, action{action}, mods{mods}, xpos{xpos}, ypos{ypos} {}
};

// Scroll event
struct ScrollEvent : public Event {
    double xoffset; // scroll offset along the x axis
    double yoffset; // scroll offset along the y axis
    ScrollEvent(double xoffset, double yoffset) : Event{OnScroll}, xoffset{xoffset}, yoffset{yoffset} {}
};

// Display size event
struct SizeEvent : public Event {
    int width;      // the new width in screen coordinates.
    int height;     // the new height in screen coordinates.
    SizeEvent(int width, int height) : Event{OnSize}, width{width}, height{height} {}
};


} // namespace



