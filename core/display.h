#pragma once
#include <string>
#include <GLFW/glfw3.h>
#include "dispatcher.h"

namespace glp {

class Display;
using DisplaySP = std::shared_ptr<Display>;

class Display: public Dispatcher {

public:
    static DisplaySP make(int width, int height, const char* title, bool fullScreen=false, DisplaySP share=nullptr);
    Display(int width, int height, const char *title, bool fullScreen=false, DisplaySP share=nullptr);
    ~Display();
    void setTitle(const std::string &title);
    void close();
    void makeContextCurrent();
    bool isCurrentContext();
    void swapBuffers();
    void size(int& width, int& height) const;
    void setSize(int width, int height);
    void frameBufferSize(int& width, int& height);
    void pos(int &xpos, int &ypos) const;
    void setPos(int xpos, int ypos);
    void setTitle(std::string title);
    bool shouldClose();


private:
    static int convertButton(int button);
    static int convertMods(int mods);
    static int convertAction(int action);
    static void keyCB(GLFWwindow *win, int key, int scancode, int action, int mods);
    static void charCB(GLFWwindow *win, unsigned int codepoint);
    static void cursorCB(GLFWwindow *win, double xpos, double ypos);
    static void mouseButtonCB(GLFWwindow *win, int button, int action, int mods);
    static void scrollCB(GLFWwindow* win, double xoffset, double yoffset);
    static void sizeCB(GLFWwindow *win, int width, int height);
    void setCallbacks();

private:
    GLFWwindow *mWindow;
};


} // namespace
