#include "image.h"

// Sets stb image configuration
#define STB_IMAGE_IMPLEMENTATION
#define STBI_MALLOC(sz)           malloc(sz)
#define STBI_REALLOC(p,newsz)     realloc(p,newsz)
#define STBI_FREE(p)              free(p)
#define STBI_NO_PSD
#define STBI_NO_TGA
#define STBI_NO_HDR
#define STBI_NO_PIC
#define STBI_NO_PNM
#include "deps/stb_image.h"
#include "log.h"

namespace glp {

/**
 * Creates and returns a shared pointer to a new image.
 * If pointer to data was supplied, assumes ownership of this data,
 * otherwise allocates memory for the data.
 * @param width image width in pixels
 * @param height image height in pixels
 * @param channels number of channels (1 to 4)
 * @param data pointer to image data (to take ownership) or nullptr
 * @return shared pointer to image.
 */
ImageSP Image::make(int width, int height, int channels, unsigned char* data) {

    return std::make_shared<Image>(width, height, channels, data);
}

/**
 * Construct image with the specified dimensions and number of channels
 * If pointer to data was supplied, assumes ownership of this data,
 * otherwise allocates memory for the data.
 * @param width image width in pixels
 * @param height image height in pixels
 * @param channels number of channels (1 to 4)
 * @param data pointer to image data (to take ownership) or nullptr
 */
Image::Image(int width, int height, int channels, unsigned char* data):
    mWidth(width),
    mHeight(height),
    mChannels(channels) {

    if (data != nullptr) {
        mData = data;
        return;
    }
    mData = new unsigned char[width * height * channels];
}

/**
 * Destructor
 */
Image::~Image() {

    logDebug("~Image()");
    if (mData) {
        delete[] mData;
    }
    mData = nullptr;
}

/**
 * Returns pointer to image data
 * @return pointer to image data
 */
unsigned char* Image::data(void) const {

    return mData;
}

/**
 * Returns image width in pixels
 * @return image width
 */
int Image::width(void) const {

    return mWidth;
}

/**
 * Returns image height in pixels
 * @return image height
 */
int Image::height(void) const {

    return mHeight;
}

/**
 * Returns number of channels
 * @return number of channels
 */
int Image::channels(void) const {

    return mChannels;
}

/**
 * Sets the color of the image
 * @param color pointer to color to set
 */
void Image::setColor(const glm::vec4& color) {
  
    unsigned char r = color.x * 0xFF;
    unsigned char g = color.y * 0xFF;
    unsigned char b = color.z * 0xFF;
    unsigned char a = color.w * 0xFF;
    for (int ix = 0; ix < mWidth; ix++) {
        for (int iy = 0; iy < mHeight; iy++) {
            unsigned char* ppix = &mData[iy*mWidth * mChannels + ix * mChannels];
            switch (mChannels) {
            case 4:
                ppix[3] = a;
            case 3:
                ppix[2] = b;
            case 2:
                ppix[1] = g;
            case 1:
                ppix[0] = r;
            }
        }
    }
}

/**
 * Creates image from file
 * @param filename pointer to image file name
 * @param errmsg pointer to store error message
 * @return image object or NULL if error
 */
ImageSP Image::fromFile(const char* filename, const char** errmsg) {

    // Load and decodes image using STB
    int width;
    int height;
    int channels;
    auto imgData = stbi_load(filename, &width, &height, &channels, 0);
    if (imgData == nullptr) {
        *errmsg = stbi_failure_reason();
        return nullptr;
    }

    // Creates and returns image with decoded image data
    return make(width, height, channels, imgData);
}

/**
 * Creates image from specified encoded image data
 * @param data pointer to encoded image data
 * @param len size of image data
 * @param errmsg pointer to store error message
 * @return image object or NULL if error
 */
ImageSP Image::fromData(const unsigned char* data, int len, const char** errmsg) {

    // Decodes image using STB
    int width;
    int height;
    int channels;
    auto imgData = stbi_load_from_memory(data, len, &width, &height, &channels, 0);
    if (imgData == nullptr) {
        *errmsg = stbi_failure_reason();
        return nullptr;
    }

    // Creates and returns image with decoded image data
    return make(width, height, channels, imgData);
}

/**
 * Draw pixel at the specified image coordinates
 * The final color is the current image color at the position
 * multiplied by the specified color
 * @param x x coordinate from left to right
 * @param y y coordinate from top to bottom
 * @param color pixel color
 */
void Image::drawPix(int x, int y, glm::vec4 color) {

    unsigned char* ppix = &mData[y * mWidth * mChannels + x * mChannels];
    #define CLAMP(v) (v) > 255 ? 255 : (v)
    int8_t r = CLAMP(color.r * 255.0);
    int8_t g = CLAMP(color.g * 255.0);
    int8_t b = CLAMP(color.b * 255.0);
    int8_t a = CLAMP(color.a * 255.0);
    ppix[0] = r;
    ppix[1] = g;
    ppix[2] = b;
    ppix[3] = a;
}


} // namespace

