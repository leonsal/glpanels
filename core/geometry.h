#pragma once
#include <memory>
#include <vector>
#include "glad/glad.h"
#include "vbo.h"
#include "program.h"
#include "renderinfo.h"

namespace glp {

class Geometry;
using GeometryUP = std::unique_ptr<Geometry>;
using GeometrySP = std::shared_ptr<Geometry>;

class Geometry {

    public:
        static GeometryUP makeUP();
        static GeometrySP makeSP();
        Geometry();
        ~Geometry();
        void addVbo(Vbo* vbo);
        void addVbo(const char* name, int itemSize, std::vector<float>& buf);
        void setIndices(std::vector<unsigned int>& indices);
        void setIndices(const GLuint* buf, size_t len);
        Vbo* vbo(const char* attrib);
        Vbo* vboAt(size_t index);
        size_t indicesCount();
        size_t itemsCount();
        void transfer(const RenderInfo& ri, Program *prog);

    private:
        struct ContextInfo {
            Context mContext;
            GLuint  mHandleVao;
        };
        GLuint  mHandleIndices;
        bool    mUpdateIndices;
        std::vector<ContextInfo> mContexts;
        std::vector<GLuint>      mIndices;
        std::vector<Vbo*>        mVbos; 
};

} // namespace


