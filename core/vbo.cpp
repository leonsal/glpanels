#include <cassert>
#include "log.h"
#include "glad/glad.h"
#include "program.h"
#include "vbo.h"

namespace glp {

/**
 * Constructor
 */
Vbo::Vbo():
    mHandle(0), mUpdate(true), mUsage(GL_STATIC_DRAW), mBuf{} {

    // Create buffer for this VBO
    glGenBuffers(1, &mHandle);
}

/**
 * Destructor
 */
Vbo::~Vbo() {

    logDebug("~Vbo({})", mHandle);
    if (mHandle != 0) {
        glDeleteBuffers(1, &mHandle);
        mHandle = 0;
    }
}

/**
 * Adds attribute to this VBO
 * @param name attribute name as specified in the shader
 * @param itemSize number of 'floats' for the attribute
 */
void Vbo::addAttrib(const char* name, int itemSize) {

    // Checks if attribute name is unique
    for (const auto& attr: mAttribs) {
        if (attr.name == name) {
            return;
        }
    }
    // Appends attribute
    mAttribs.push_back({name, itemSize});
}

/**
 * Returns the number of attributes in this VBO
 * @return number of attributes
 */
size_t Vbo::attribCount() const {

    return mAttribs.size();
}

/**
 * Returns the item size of the specified attribute
 * @param name name of the attribute
 * @return item size or -1 if not found
 */
int Vbo::attribSize(const char* name) const {

    for (const auto& attr: mAttribs) {
        if (attr.name == name) {
            return attr.itemSize;
        }
    }
    return -1;
}

/**
 * Returns the stride of this VBO which is the number of bytes
 * used by one complete group attributes side by side in the buffer
 * Ex: | x y z r g b | x y z r g b | ... | x y z r g b |
 * The stride will be: sizeof(float) * 6 = 24
 * @return stride
 */
size_t Vbo::stride() const {

	size_t stride = 0;
    for (const auto& attr: mAttribs) {
		stride += sizeof(float) * attr.itemSize;
    }
	return stride;
}

/**
 * Returns the number of items in this VBO
 * An item is a complete group of attributes
 * Ex. this buffer: [ x y z r g b | x y z r g b ] has 2 items
 */
size_t Vbo::itemsCount() const {

    if (mAttribs.empty() || !mBuf) {
        return 0;
    }
    return mBuf->size() * sizeof(float) / stride();
}

/**
 * Returns pointer to this VBO buffer
 * @return pointer to this vbo buffer
 */
std::vector<float>* Vbo::buffer() {

    return mBuf;
}

/**
 * Sets the pointer to the buffer with data to send to OpenGL
 * This buffer is not managed by the VBO
 * @param buf pointer to buffer
 */
void Vbo::setBuffer(std::vector<float>* buf) {

    mBuf = buf;
    mUpdate = true;
}

/**
 * Forces the update of this VBO at the next render
 */
void Vbo::update() {

    mUpdate = true;
}

/**
 * Called by Geometry every time a new VAO is created for a new Context.
 * Before this function is called the VAO and the indices buffer MUST be binded.
 * @param prog pointer to program object in use
 */
void Vbo::init(Program* prog) {

    glBindBuffer(GL_ARRAY_BUFFER, mHandle);

    // Calculates stride
    GLsizei stride = 0;
    if (mAttribs.size() > 1) {
        for (const auto& attr: mAttribs)  {
            stride += sizeof(GL_FLOAT) * attr.itemSize;
        }
    }

    // Enable vertex attribute arrays
    GLint items = 0;
    GLvoid* pointer = nullptr;
    for (const auto& attr: mAttribs)  {
        // Get attribute location in the current program
        GLint loc = glGetAttribLocation(prog->handle(), attr.name.c_str());
        if (loc < 0) {
            logInfo("VBO: attribute [{}] location not found", attr.name);
            continue;
        }
        // Enables attribute and sets its stride and offset in the buffer
        glEnableVertexAttribArray(loc);
        glVertexAttribPointer(loc, attr.itemSize, GL_FLOAT, false, stride, pointer);
        items += attr.itemSize;
        pointer = (void*)(sizeof(GL_FLOAT) * items);
    }
}

/**
 * Transfer this VBO to OpenGL if necessary
 * @param pointer to current program
 */
void Vbo::transfer(const RenderInfo& ri, Program *prog) {

    if (!mUpdate || !mBuf) {
        return;
    }

    // Transfer VBO buffer
    glBindBuffer(GL_ARRAY_BUFFER, mHandle);
	glBufferData(GL_ARRAY_BUFFER, mBuf->size() * sizeof(float), mBuf->data(), mUsage);
    mUpdate = false;
}

} // namespace

