#pragma once

namespace glp {

struct Border {
    float top;
    float right;
    float bottom;
    float left;
    Border(): top{0}, right{0}, bottom{0}, left{0} {}
    Border(float top, float right, float bottom, float left):
       top{top} , right{right}, bottom{bottom}, left{left} {}
    void set(float top, float right, float bottom, float left);
    bool empty() const;
};

} // namespace
