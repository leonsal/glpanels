#include "events.h"

namespace glp {

// Display events
const char *const OnKey = "OnKey";
const char *const OnChar = "OnChar";
const char *const OnCursorPos = "OnCursorPos";
const char *const OnMouseButton = "OnMouseButton";
const char *const OnScroll = "OnScroll";
const char *const OnSize = "OnSize";

// Other events
const char *const OnClick = "OnClick";
const char *const OnCursorEnter = "OnCursorEnter";
const char *const OnCursorLeave = "OnCursorLeave";
const char *const OnEnable = "OnEnable";
const char *const OnMouseOut = "OnMouseOut";
const char *const OnResize = "OnResize";
const char *const OnChange = "OnChange";
const char *const OnChild = "OnChild";
const char *const OnRadioGroup = "OnRadioGroup";
const char *const OnLostKeyFocus = "OnLostKeyFocus";

} // namespace

