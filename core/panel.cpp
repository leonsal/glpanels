#include <iostream>
#include <vector>
#include <algorithm>
#include "log.h"
#include "panel.h"
#include "glm/common.hpp"


namespace glp {

/**
 * Creates and returns shared pointer to a new Panel
 * @param with panel width in pixels
 * @param height panel height in pixels
 * @return shared pointer to Panel
 */
PanelSP Panel::makeSP(float width, float height) {

    return std::make_shared<Panel>(width, height);
}

/**
 * Default constructor
 */
Panel::Panel(): Panel{0, 0} {
}

/**
 * constructor
 */
Panel::Panel(float width, float height):
    mId{mCount+1},
    mParent{},
    mName{},
    mPos{},
    mWidth{width},
    mHeight{height},
    mMargin{},
    mBorder{},
    mPadding{},
    mContent{},
    mPosAbs{},
    mBounded{true},
    mEnabled{true},
    mVisible{true},
    mCursorOver{false},
    mTex{nullptr},
    mUdata{
        float(Op::Color),   // operation
        1,width, height,    // aspect ratio, width, height,
        {0,0,0,0},          // bounds
        {0,0,0,0},          // borders
        {0,0,0,0},          // padding
        {0,0,0,0},          // content
        {0,0,0,0},          // border color
        {0,0,0,0},          // padding color
        {0,0,0,0},          // content color
        {0,0,0,0},          // roundness
        {0,0,0,1},          // text color
        0,0,0,0,            // caret width/height/x/y
        {0,0,0,1},          // caret color
    } {

    setup();
    resize(width, height);
    mName = "Panel:" + std::to_string(mId);
    mCount++;
}


/**
 * Destructor
 */
Panel::~Panel() {

    logDebug("~Panel({})", mName);
}

/**
 * Appends child to this Panel
 * @param child shared pointer to child to append
 */
void Panel::appendChild(PanelSP child) {

    insertChild(child, mChildren.size());
}

/**
 * Returns the current border's widths of this panel
 */
Border Panel::border() const {

    return mBorder;
}

/**
 * Returns the bounded state of this Panel
 */
bool Panel::bounded() const {

    return mBounded;
}

/**
 * Returns vector with this Panel's children
 */
const std::vector<PanelSP>& Panel::children() const {

    return mChildren;
}

float Panel::contentHeight() const {

    return mContent.height;
}

float Panel::contentWidth() const {

    return mContent.width;
}

/**
 * Returns current content area in texture coordinates
 */
glm::vec4 Panel::contentCoords() const {

    return mUdata.content;
}

bool Panel::cursorOver() const {

    return mCursorOver;
}

bool Panel::enabled() const {

    return mEnabled;
}

/**
 * Returns the first child of this panel
 * @return shared pointer to first child or nullptr if this panel has no children
 */
PanelSP Panel::fistChild() const {

    if (mChildren.empty()) {
        return nullptr;
    }
    return mChildren[0];
}

float Panel::height() const {

    return mHeight;
}

/**
 * Returns the id of this panel
 */
int Panel::id() const {

    return mId;
}

/**
 * Returns if the specified view is a child of this View
 * @param pan panel to check
 * @return true if is child, false otherwise
 */
bool Panel::isChild(PanelSP pan) const {

    auto it = std::find(mChildren.begin(), mChildren.end(), pan);
    if (it != mChildren.end()) {
        return true;
    }
    return false;
}


/**
 * Inserts child at the specified position in this panel
 * @param child shared pointer to child to insert
 * @param pos position to insert
 */
void Panel::insertChild(PanelSP child, int pos) {

    if (pos < 0 || pos > mChildren.size()) {
       throw std::runtime_error("Invalid position to insert to");
    }

    // If child has a parent, removes child from the parent
    if (child->mParent) {
        if (child->mParent == this) {
            throw std::runtime_error("Child is already a child of this panel");
        }
        child->mParent->removeChild(child);
    }
    child->mParent = this;
    mChildren.insert(mChildren.begin() + pos, child);
}

/**
 * Returns if the specified coordinate is inside this panel including its border
 * @param x x coordinate in pixels
 * @param y y coordinate in pixels
 * @return true if coordinate is inside the panel, false otherwise
 */
bool Panel::insideBorders(float x, float y) const {

    return !(x < (mPosAbs.x + mMargin.left) || x >= (mPosAbs.x + mWidth - mMargin.right) ||
             y < (mPosAbs.y + mMargin.top)  || y >= (mPosAbs.y + mHeight - mMargin.bottom));
}

/**
 * Returns if all elements of this panel are transparent and panel does contain a valid texture
 */
bool Panel::isTransparent() const {

    return  mUdata.borderColor.a == 0 &&
            mUdata.paddingColor.a == 0 &&
            mUdata.contentColor.a == 0 &&
            mUdata.operation == float(Op::Color);
}

/**
 * Returns the last child of this panel
 * @return shared pointer to last child of nullptr if this panel has no children
 */
PanelSP Panel::lastChild() const {

    if (mChildren.empty()) {
        return nullptr;
    }
    return mChildren[mChildren.size()-1];
}

/**
 * Returns the margin of this panel
 */
Border Panel::margin() const {

    return mMargin;
}

/**
 * Returns copy of the model matrix of this panel
 */
glm::mat4 Panel::modelMatrix() const {

    return mModelMatrix;
}

/**
 * Returns the name of this panel
 */
std::string Panel::name() const {

    return mName;
}

/**
 * Called by the event manager (normally the Gui singleton) with events to be processed by this panel
 * Derived classes normally override this virtual method.
 * @param evname event name
 * @param ev  event data
 * @return
 */
bool Panel::onEvent(const Event& ev) {

    return true;
}

/**
 * Returns the next sibling of this panel
 * @return shared pointer to next sibling of nullptr
 */
PanelSP Panel::nextSibling() const {

    if (mParent == nullptr) {
        return nullptr;
    }
    auto it = std::find_if(mParent->mChildren.begin(), mParent->mChildren.end(),
        [this](const PanelSP& child) {return child.get() == this;});
    if (it == mParent->mChildren.end()) {
        return nullptr;
    }
    return *(++it);
}

/**
 * Returns the padding of this panel
 */
Border Panel::padding() const {

    return mPadding;
}

/**
 * Returns the position of this panel in pixel relative to its parent
 * The z coordinate is in NDC
 */
glm::vec3 Panel::pos() const {

    return mPos;
}

/**
 * Returns the absolute position in pixels of this panel relative to its display
 * @return
 */
glm::vec3 Panel::posAbs() const {

    return mPosAbs;
}

/**
 * Returns the previous sibling of this panel
 * @return shared pointer to previous sibling or nullptr
 */
PanelSP Panel::prevSibling() const {

    if (mParent == nullptr) {
        return nullptr;
    }
    auto it = std::find_if(mParent->mChildren.begin(), mParent->mChildren.end(),
        [this](const PanelSP& child) {return child.get() == this;});
    if (it == mParent->mChildren.begin()) {
        return nullptr;
    }
    return *(--it);
}

/**
 * Removes specified child from this panel
 * @param child
 * @return pointer to removed child or nullptr if not found
 */
PanelSP Panel::removeChild(PanelSP child) {

    if (child->mParent != this) {
        throw std::runtime_error("Child is not child of this panel");
    }
    auto it = std::find(mChildren.begin(), mChildren.end(), child);
    if (it != mChildren.end()) {
        mChildren.erase(it);
        return child;
    }
    return nullptr;
}

/**
 * Render this panel and all its children recursively using the specified context
 * @param ri Reference to RenderInfo
 */
void Panel::renderAll(const RenderInfo& ri) {

    render(ri);
    for (auto& child: mChildren) {
        child->renderAll(ri);
    }
}

/**
 * Render this panel
 */
void Panel::render(const RenderInfo& ri) {

    auto render = renderSetup(ri);
    if (render) {
        renderDraw(ri);
    }
}

/**
 * Render setup
 * sends base uniforms but do not draw
 * @param returns if the panels needs to be drawn
 */
bool Panel::renderSetup(const RenderInfo& ri) {

    // If panel is not visible, ignore it and all its children
    if (!mVisible) {
        return false;
    }

    // Update absolute coordinates and bounds
    updateBounds();

    // If panel is transparent, no need to render
    if (isTransparent()) {
        return false;
    }

    // Calculates panel width and height in clip coordinates (-1, 1)
    float clipW = 2 * mWidth / float(ri.viewWidth);
    float clipH = 2 * mHeight / float(ri.viewHeight);

    // Converts absolute position in pixel coordinates from the top/left to
    // OpenGL clip coordinates of the quad center
    float clipX = (mPosAbs.x - float(ri.viewWidth) / 2) / (float(ri.viewWidth) / 2);
    float clipY = -(mPosAbs.y - float(ri.viewHeight) / 2) / (float(ri.viewHeight) / 2);
    float clipZ = mPos.z;

    // Sets model matrix matrix
    mModelMatrix = {
        clipW,  0,      0, clipX,
        0,      clipH,  0, clipY,
        0,      0,      1, clipZ,
        0,      0,      0, 1
    };

    // Sets shader program
    glUseProgram(mProg.handle());

    // Transfer texture information
    if (mTex) {
        mTex->transfer(mLocTexSampler, mLocTexInfo);
    }

    // Transfer uniforms
    glUniformMatrix4fv(mLocModelMatrix, 1, true, &mModelMatrix[0][0]);
    int nvecs = (sizeof(mUdata) / sizeof(GLfloat)) / 4;
    glUniform4fv(mLocPanel, nvecs, (GLfloat *) &mUdata);
    return true;
}

/**
 * Sends draw call
 * @param ri
 */
void Panel::renderDraw(const RenderInfo& ri) {

    // Transfer geometry only once and set VAO.
    mGeom->transfer(ri, &mProg);

    // Draw call
    glDrawElements(GL_TRIANGLES, mGeom->indicesCount(), GL_UNSIGNED_INT, nullptr);
}

/**
 * Sets this View background color
 * @param color
 */
void Panel::setBgColor(const glm::vec3& color, float alpha) {

    mUdata.paddingColor = glm::vec4(color, alpha);
    mUdata.contentColor = glm::vec4(color, alpha);
}

/**
 * Sets this View background color
 * @param color
 */
void Panel::setBgColor(const glm::vec4& color) {

   mUdata.paddingColor = color;
   mUdata.contentColor = color;
}

/**
 * Sets this panel border widths in pixels
 * @param width Width of all border' sides in pixels
 */
void Panel::setBorder(float width) {

    setBorder(width, width, width, width);
}

/**
 * Sets this panel border widths in pixels
 * @param top top border width in pixels
 * @param right right border width in pixels
 * @param bottom bottom border width in pixels
 * @param left left border width in pixels
 */
void Panel::setBorder(float top, float right, float bottom, float left) {

    mBorder.top = top;
    mBorder.right = right;
    mBorder.bottom = bottom;
    mBorder.left = left;
    resize(calcWidth(), calcHeight());
}

/**
 * Sets this panel borders widths in pixels from Borders
 * @param borders reference to Border
 */
void Panel::setBorder(const Border& border) {

    mBorder = border;
    resize(calcWidth(), calcHeight());
}

/**
 * Sets this panel border color
 * @param color reference to Vec3 with color
 */
void Panel::setBorderColor(const glm::vec3& color, float alpha) {

    mUdata.borderColor = glm::vec4(color, alpha);
}

/**
 * Sets this panel border color
 * @param color reference to Vec3 with color
 */
void Panel::setBorderColor(const glm::vec4& color) {

    mUdata.borderColor = color;
}

/**
 * Sets this panel bounded attribute
 * @param bounded bounded attribute
 */
void Panel::setBounded(bool bounded) {

    mBounded = bounded;
}

/**
 * Sets the text caret info
 * @param x x in pixel coordinates inside the texture
 * @param y y in pixel coordinates inside the texture
 * @param width  width in pixels
 * @param height height in pixels
 */
void Panel::setCaret(float x, float y, float width, float height) {

    // Saves caret coordinates and dimensions in texture coordinates
    mUdata.caretX = x / mWidth;
    mUdata.caretY = y / mHeight;
    mUdata.caretWidth = width / mWidth;
    mUdata.caretHeight = height / mHeight;
}

/**
 * Sets the text caret color
 * @param color
 */
void Panel::setCaretColor(const glm::vec4& color) {

    mUdata.caretColor = color;
}

/**
 * Set this panel width and adjusts its height to 
 * keep the same current aspect ratio (width/height)
 * @param width width in pixels
 */
void Panel::setContentAspectWidth(float width) {

	float aspect = mContent.width / mContent.height;
	float height = width / aspect;
	setContentSize(width, height);
}

/**
 * Set this panel height and adjusts its width to 
 * keep the same current aspect ratio (width/height)
 * @param width width in pixels
 */
void Panel::setContentAspectHeight(float height) {

	float aspect = mContent.width / mContent.height;
	float width = height * aspect;
	setContentSize(width, height);
}
/**
 *
 * Sets this panel internal content height in pixels
 * @param height content height in pixels
 */
void Panel::setContentHeight(float height) {

    setContentSize(mContent.width, height);
}

/**
 * Sets this panel internal content size in pixels
 * @param width content width in pixels
 * @param height content height in pixels
 */
void Panel::setContentSize(float width, float height) {

    float twidth = width +
        mPadding.left + mPadding.right +
        mBorder.left + mBorder.right +
        mMargin.left + mMargin.right;
    float theight = height +
        mPadding.top + mPadding.bottom +
        mBorder.top + mBorder.bottom +
        mMargin.top + mMargin.bottom;
    resize(twidth, theight);
}

/**
 * Sets this panel internal content width in pixels
 * @param with content width in pixels
 */
void Panel::setContentWidth(float width) {

    setContentSize(width, mContent.height);
}

void Panel::setCursorOver(bool state) {

    mCursorOver = state;
}

/**
 * Sets the enabled state of this panel
 * @param enabled enabled state
 */
void Panel::setEnabled(bool enabled) {

    mEnabled = enabled;
    //notify(OnEnable, EnableEvent());
}

/**
 * Sets this panel external height in pixels
 * The internal panel areas and positions are recalculated
 * @param height height
 */
void Panel::setHeight(float height) {

	setSize(mWidth, height);
}

/**
 * Set this panel margins widths in pixels
 * @param top top margin width in pixels
 * @param right right margin width in pixels
 * @param bottom bottom margin width in pixels
 * @param left left margin width in pixels
 */
void Panel::setMargin(float top, float right, float bottom, float left) {

    mMargin.top = top;
    mMargin.right = right;
    mMargin.bottom = bottom;
    mMargin.left = left;
    resize(calcWidth(), calcHeight());
}

/**
 * Sets this panel margins widths in pixels from Borders
 * @param margins reference to Borders with margins widths
 */
void Panel::setMargin(const Border& margins) {

    mMargin = margins;
    resize(calcWidth(), calcHeight());
}

/**
 * Sets the name of this panel
 */
void Panel::setName(const std::string& name) {

    mName = name;
}

void Panel::setPadding(float width) {

    setPadding(width, width, width, width);
}

/**
 * Sets this panel paddings widths
 * @param top top border width in pixels
 * @param right right border width in pixels
 * @param bottom bottom border width in pixels
 * @param left left border width in pixels
 */
void Panel::setPadding(float top, float right, float bottom, float left) {

    mPadding.top = top;
    mPadding.right = right;
    mPadding.bottom = bottom;
    mPadding.left = left;
    resize(calcWidth(), calcHeight());
}

/**
 * Sets this panel paddings color
 * @param color reference to Vec4 with color
 */
void Panel::setPaddingColor(const glm::vec3& color, float alpha) {

    mUdata.paddingColor = glm::vec4(color, alpha);
}

/**
 * Sets this panel paddings color
 * @param color reference to Vec4 with color
 */
void Panel::setPaddingColor(const glm::vec4& color) {

    mUdata.paddingColor = color;
}

/**
 * Sets the position of this panel in pixel coordinates
 * from left to right and from top to bottom of the screen.
 * The position is relative to this panel parent.
 * @param x x coordinate in pixels
 * @param y y coordinate in pixels
 */
void Panel::setPos(float x, float y) {

    mPos.x = x;
    mPos.y = y;
}

/**
 * Sets the position of this panel in pixel coordinates
 * from left to right and from top to bottom of the screen.
 * The position is relative to this panel parent.
 * @param x x coordinate in pixels
 * @param y y coordinate in pixels
 * @param z z coordinate in pixels
 */
void Panel::setPos(float x, float y, float z) {

    mPos.x = x;
    mPos.y = y;
    mPos.z = z;
}

/**
 * Sets the roundness of each corner of the panel.
 * The roundness value is 0 for non roundness for 1 to maximum roundness.
 */
void Panel::setRoundness(float tl, float tr, float br, float bl) {

    mUdata.roundness = glm::clamp(glm::vec4{tl, tr, br, bl}, 0.0f, 1.0f);
}

/**
 * Sets the roundness all corners of the panel.
 * The roundness value is 0 for non roundness for 1 to maximum roundness.
 */
void Panel::setRoundness(float r) {

    setRoundness(r, r, r, r);
}

/**
 * Sets this panel external size in pixels
 * @param width panel width in pixels
 * @param height panel height in pixesl
 */
void Panel::setSize(float width, float height) {

    if (width < 0) {
        width = 0;
    }
    if (height < 0) {
        height = 0;
    }
    resize(width, height);
}

/**
 * Sets this panel style from the specified style
 * @param st
 */
void Panel::setStyle(const Panel::Style& st) {

    mMargin = st.margin;
    mBorder = st.border;
    mPadding = st.padding;
    mUdata.borderColor = st.borderColor;
    mUdata.paddingColor = st.bgColor;
    mUdata.contentColor = st.bgColor;
    mUdata.roundness = st.roundness;
}

/**
 * Sets the color of the text texture
 * @param color
 * @param alpha
 */
void Panel::setTextColor(const glm::vec3& color, float alpha) {

    mUdata.textColor = glm::vec4(color, alpha);
}

/**
 * Set the color of the text texture
 * @param color
 */
void Panel::setTextColor(const glm::vec4& color) {

    mUdata.textColor = color;
}

/**
 * Sets this panel content texture as an image texture
 * @param tex Texture to set
 */
void Panel::setImageTexture(Texture2DSP tex) {

    mTex = tex;
    if (mTex) {
        mUdata.operation = float(Op::Image);
    } else {
        mUdata.operation = float(Op::Color);
    }
}

/**
 * Sets this panel content texture as a text texture
 * @param tex Texture to set
 */
void Panel::setTextTexture(Texture2DSP tex) {

    mTex = tex;
    if (mTex) {
        mUdata.operation = float(Op::Text);
    } else {
        mUdata.operation = float(Op::Color);
    }
}

/**
 * Sets this panel width in pixels
 * @param width panel width in pixels
 */
void Panel::setWidth(float width) {

	setSize(width, mHeight);
}

/**
 * Return the optional texture of this panel
 */
Texture2DSP Panel::texture() const {

    return mTex;
}

/**
 * Returns this panel visibility state
 * @return
 */
bool Panel::visible() const {

    return mVisible;
}

/**
 * Returns this panel external width in pixels
 * @return width in pixels
 */
float Panel::width(void) const {

    return mWidth;
}

/**
 * Returns the x coordinate of this panel in pixels relative to its parent
 */
float Panel::x() const {

    return mPos.x;
}

/**
 * Returns the y coordinate of this panel in pixels relative to its parent
 */
float Panel::y() const {

    return mPos.y;
}

/**
 * Returns the z coordinate of this panel in Normalized Device Coordinates
 */
float Panel::z() const {

    return mPos.z;
}


//
// Private methods
//

/**
 * Calculates and returns this panel external width in pixels
 * @return external width in pixels
 */
float Panel::calcWidth() const {

    return mContent.width +
        mPadding.left + mPadding.right +
        mBorder.left + mBorder.right +
        mMargin.left + mMargin.right;
}

/**
 * Calculates and returns this panel external height in pixels
 * @return external height in pixels
 */
float Panel::calcHeight() const {

    return mContent.height +
        mPadding.top + mPadding.bottom +
        mBorder.top + mBorder.bottom +
        mMargin.top + mMargin.bottom;
}

//void Panel::calcModelMatrix(const RenderInfo& ri) {
//
//    // Calculates panel width and height in clip coordinates (-1, 1)
//    float clipW = 2 * mWidth / ri.viewWidth;
//    float clipH = 2 * mHeight / ri.viewHeight;
//
//    // Converts absolute position in pixel coordinates from the top/left to
//    // OpenGL clip coordinates of the quad center
//    float clipX = (mPosAbs.x - float(ri.viewWidth) / 2) / (float(ri.viewWidth) / 2);
//    float clipY = -(mPosAbs.y - float(ri.viewHeight) / 2) / (float(ri.viewHeight) / 2);
//    float clipZ = mPos.z;
//
//    // Sets transformation matrix
//    float mm[]{
//            clipW, 0, 0, clipX,
//            0, clipH, 0, clipY,
//            0, 0, 1, clipZ,
//            0, 0, 0, 1
//    };
//}
//

/**
 * Returns naked pointer to current Program object
 * @return
 */
Program* Panel::prog() const {

    return &mProg;
}

/**
 * Try to set to set the external size of the panel to the specified
 * dimensions in pixels and recalculates the size and positions of the internal areas.
 * The margins, borders and padding sizes are kept and the content
 * area size is adjusted. So if the panel is decreased, its minimum
 * size is determined by the margins, borders and paddings.
 * @param width width in pixels
 * @param height height in pixels
 */
void Panel::resize(float width, float height) {

    // Adjusts content width
    mContent.width = width -
        mMargin.left - mMargin.right -
        mBorder.left - mBorder.right -
        mPadding.left - mPadding.right;
    if (mContent.width < 0) {
        mContent.width = 0;
    }

    // Adjusts content height
    mContent.height = height -
        mMargin.top - mMargin.bottom -
        mBorder.top - mBorder.bottom -
        mPadding.top - mPadding.bottom;
    if (mContent.height < 0) {
        mContent.height = 0;
    }

    // Adjust other area widths
    Rect padding{};
    Rect border{};
    padding.width = mPadding.left + mContent.width + mPadding.right;
    border.width = mBorder.left + padding.width + mBorder.right;

    // Adjust other area heights
    padding.height = mPadding.top + mContent.height + mPadding.bottom;
    border.height = mBorder.top + padding.height + mBorder.bottom;

    // Sets area positions
    border.x = mMargin.left;
    border.y = mMargin.top;
    padding.x = border.x + mBorder.left;
    padding.y = border.y + mBorder.top;
    mContent.x = padding.x + mPadding.left;
    mContent.y = padding.y + mPadding.top;

    // Sets final panel dimensions (may be different from requested dimensions)
    mWidth = mMargin.left + border.width + mMargin.right;
    mHeight = mMargin.top + border.height + mMargin.bottom;

    if (mWidth == 0 || mHeight == 0) {
        mUdata.border  = glm::vec4{0};
        mUdata.padding = glm::vec4{0};
        mUdata.content = glm::vec4{0};
    } else {
        // Updates border uniform in texture coordinates (0,0 -> 1,1)
        mUdata.border.x = border.x / mWidth;
        mUdata.border.y = border.y / mHeight;
        mUdata.border.z = border.width  / mWidth;
        mUdata.border.w = border.height / mHeight;
        // Updates padding uniform in texture coordinates (0,0 -> 1,1)
        mUdata.padding.x = padding.x / mWidth;
        mUdata.padding.y = padding.y / mHeight;
        mUdata.padding.z = padding.width / mWidth;
        mUdata.padding.w = padding.height / mHeight;
        // Updates content uniform in texture coordinates (0,0 -> 1,1)
        mUdata.content.x = mContent.x / mWidth;
        mUdata.content.y = mContent.y / mHeight;
        mUdata.content.z = mContent.width / mWidth;
        mUdata.content.w = mContent.height / mHeight;
    }

	// Updates aspect ratio uniform
	mUdata.aspect = mWidth / mHeight;
    mUdata.width = mWidth;
    mUdata.height = mHeight;

//  // Update layout and dispatch event
//  if p.layout != nil {
//      p.layout.Recalc(p)
//  }
//    notify(OnResize, Notifier::Event());
}

void Panel::updateBounds() {

    // If Panel has no parent
    if (mParent == nullptr) {
        mPosAbs = mPos;
        mXmin = -10000;
        mYmin = -10000;
        mXmax = 10000;
        mYmax = 10000;
        mUdata.bounds = glm::vec4{0, 0, 1, 1};
        return;
    }

    // Sets the absolute position coordinates for this panel
    mPosAbs.x = mParent->mPosAbs.x + mPos.x;
    mPosAbs.y = mParent->mPosAbs.y + mPos.y;
    if (mBounded) {
        mPosAbs.x += mParent->mMargin.left + mParent->mBorder.left + mParent->mPadding.left;
        mPosAbs.y += mParent->mMargin.top + mParent->mBorder.top + mParent->mPadding.top;
    }

    // Set initial values for this panel bounds enclosing the entire panel
    mXmin = mPosAbs.x;
    mYmin = mPosAbs.y;
    mXmax = mPosAbs.x + mWidth;
    mYmax = mPosAbs.y + mHeight;

    if (mBounded) {
        // Get the parent content area minimum and maximum absolute coordinates in pixels
        float pxmin = mParent->mPosAbs.x + mParent->mMargin.left + mParent->mBorder.left + mParent->mPadding.left;
        if (pxmin < mParent->mXmin) {
            pxmin = mParent->mXmin;
        }
        float pymin = mParent->mPosAbs.y + mParent->mMargin.top + mParent->mBorder.top + mParent->mPadding.top;
        if (pymin < mParent->mYmin) {
            pymin = mParent->mYmin;
        }
        float pxmax = mParent->mPosAbs.x + mParent->mWidth -
                      (mParent->mMargin.right + mParent->mBorder.right + mParent->mPadding.right);
        if (pxmax > mParent->mXmax) {
            pxmax = mParent->mXmax;
        }
        float pymax = mParent->mPosAbs.y + mParent->mHeight -
                      (mParent->mMargin.bottom + mParent->mBorder.bottom + mParent->mPadding.bottom);
        if (pymax > mParent->mYmax) {
            pymax = mParent->mYmax;
        }
        // Update this panel to fit in parent
        if (mXmin < pxmin) {
            mXmin = pxmin;
        }
        if (mYmin < pymin) {
            mYmin = pymin;
        }
        if (mXmax > pxmax) {
            mXmax = pxmax;
        }
        if (mYmax > pymax) {
            mYmax = pymax;
        }
    }

    // Set default values for bounds in texture coordinates
    float xmintex = 0;
    float ymintex = 0;
    float xmaxtex = 1;
    float ymaxtex = 1;

    // If this panel is bounded to its parent, calculates the bounds
    // for clipping in texture coordinates
    if (mBounded) {
        if (mPosAbs.x < mXmin) {
            xmintex = (mXmin - mPosAbs.x) / mWidth;
        }
        if (mPosAbs.y < mYmin) {
            ymintex = (mYmin - mPosAbs.y) / mHeight;
        }
        if (mPosAbs.x + mWidth > mXmax) {
            xmaxtex = (mXmax - mPosAbs.x) / mWidth;
        }
        if (mPosAbs.y + mHeight > mYmax) {
            ymaxtex = (mYmax - mPosAbs.y) / mHeight;
        }
    }
    // Sets bounds uniform
    mUdata.bounds.x = xmintex;
    mUdata.bounds.y = ymintex;
    mUdata.bounds.z = xmaxtex;
    mUdata.bounds.w = ymaxtex;
}

/**
 * Set this panel shader operation
 * @param op
 */
void Panel::setOperation(Panel::Op op) {

    mUdata.operation = float(op);
}

// Initial setup executed once which creates the common Geometry and shader program
void Panel::setup() {

    // If geometry already created (just once), nothing to do.
    if (mGeom) {
        return;
    }

    // Creates VBO interlaced buffer with vertices and texture coordinates
    mBuffer = {
            0,  0,  0,   0, 1,   // top left vertex
            0, -1,  0,   0, 0,   // bottom left vertex
            1, -1,  0,   1, 0,   // bottom right vertex
            1,  0,  0,   1, 1    // top right vertex
    };
    uint32_t indices[] = {0, 1, 2, 0, 2, 3};
    auto vbo = new Vbo();
    vbo->addAttrib("VertexPosition", 3);
    vbo->addAttrib("VertexTexcoord", 2);
    vbo->setBuffer(&mBuffer);

    // Create geometry, adds VBO and set indices
    mGeom = Geometry::makeUP();
    mGeom->addVbo(vbo);
    mGeom->setIndices(indices, 6);

    // Create shader program
    mProg.addShader(GL_VERTEX_SHADER, vertexShader);
    mProg.addShader(GL_FRAGMENT_SHADER, fragShader);
    auto res = mProg.build();
    if (res) {
        std::cout << res << "\n";
        std::cout << mProg.log();
        throw std::runtime_error("Error compiling shader program");
    }

    // Get uniform locations
    mLocModelMatrix = mProg.getUniformLocation("ModelMatrix");
    if (mLocModelMatrix < 0) {
        throw std::runtime_error("Error getting 'ModelMatrix' uniform location");
    }
    mLocPanel = mProg.getUniformLocation("Panel");
    if (mLocPanel < 0) {
        throw std::runtime_error("Error getting 'Panel' uniform location");
    }
    mLocTexSampler = mProg.getUniformLocation("TexSampler");
    if (mLocTexSampler < 0) {
        throw std::runtime_error("Error getting 'TexSampler' uniform location");
    }
    mLocTexInfo = mProg.getUniformLocation("TexInfo");
    if (mLocTexInfo < 0) {
        throw std::runtime_error("Error getting 'TexInfo' uniform location");
    }
}

//
// Initialize static objects
//
int Panel::mCount = 0;
GeometryUP Panel::mGeom = nullptr;
std::vector<float> Panel::mBuffer{};
Program Panel::mProg;
int Panel::mLocModelMatrix = -1;
int Panel::mLocPanel = -1;
int Panel::mLocTexSampler = -1;
int Panel::mLocTexInfo = -1;

//
// Panel Vertex Shader
//
const char* Panel::vertexShader = R"(
#version 330 core

// Input attributes
layout(location = 0) in  vec3  VertexPosition;
layout(location = 1) in  vec2  VertexTexcoord;

// Input uniforms
uniform mat4 ModelMatrix;

// Outputs for fragment shader
out vec2 FragTexcoord;

void main() {

    // Always flip texture coordinates
    vec2 texcoord = VertexTexcoord;
    texcoord.y = 1 - texcoord.y;
    FragTexcoord = texcoord;

    // Set position
    gl_Position = ModelMatrix * vec4(VertexPosition.xyz, 1);
}
)";

//
// Panel Fragment Shader
//
const char* Panel::fragShader = R"(
#version 330 core

// Input from Vertex shader
in vec2 FragTexcoord;

// Texture input uniforms (not all panels have textures)
uniform sampler2D	    TexSampler;
uniform vec2		    TexInfo[2];

// Macros to access elements inside the Texinfo array
#define TexOffset       TexInfo[0]
#define TexRepeat       TexInfo[1]

// Base panel input uniform
uniform vec4 Panel[12];
#define Oper            Panel[0].x        // operation code
#define AspectRatio  	Panel[0].y        // panel aspect ratio
#define Width        	Panel[0].z        // panel width in pixels
#define Height        	Panel[0].w        // panel height in pixels
#define Bounds			Panel[1]		  // panel bounds in texture coordinates
#define Border			Panel[2]		  // panel border in texture coordinates
#define Padding			Panel[3]		  // panel padding in texture coordinates
#define Content			Panel[4]		  // panel content area in texture coordinates
#define BorderColor		Panel[5]		  // panel border color
#define PaddingColor	Panel[6]		  // panel padding color
#define ContentColor	Panel[7]		  // panel content color
#define Roundness 	    Panel[8]		  // panel corner roundness
#define TextColor 	    Panel[9]		  // panel text color
#define CaretWidth      Panel[10].x       // caret width in tex coordinates (0 if no caret)
#define CaretHeight     Panel[10].y       // caret height in tex coordinates
#define CaretX          Panel[10].z       // caret x in tex coordinates
#define CaretY          Panel[10].w       // caret y in tex coordinate
#define CaretColor      Panel[11]         // caret color

// Grid input uniform
uniform vec4 Grid[5];
#define HorizGrid       Grid[0].x      // number of horizontal grid lines
#define VertGrid        Grid[0].y      // number of vertical grid lines
#define GridWidth       Grid[0].z      // grid lines width in pixels
#define GridColor       Grid[1]        // grid lines color
#define HCursorColor    Grid[2]        // horizontal cursor color
#define VCursorColor    Grid[3]        // vertical cursor color
#define HCursorPos      Grid[4].x      // horizontal cursor position
#define VCursorPos      Grid[4].y      // vertical cursor position
#define HCursorWidth    Grid[4].z      // horizontal cursor width in pixels
#define VCursorWidth    Grid[4].w      // vertical cursor width in pixels

// Content operation code
#define OpColor         0
#define OpImage         1
#define OpText          2
#define OpGradient      3
#define OpGrid          4

// Output
out vec4 FragColor;

/***
* Checks if current fragment texture coordinate is inside the
* supplied rectangle in texture coordinates:
* rect[0] - position x [0,1]
* rect[1] - position y [0,1]
* rect[2] - width [0,1]
* rect[3] - height [0,1]
*/
float checkRect(vec4 rect) {

    if (FragTexcoord.x < rect[0]) {
        return 0.0;
    }
    if (FragTexcoord.x > rect[0] + rect[2]) {
        return 0.0;
    }
    if (FragTexcoord.y < rect[1]) {
        return 0.0;
    }
    if (FragTexcoord.y > rect[1] + rect[3]) {
        return 0.0;
    }

    if (Roundness == vec4(0)) {
        return 1.0;
    }

    // Adjust fragment x coordinate multiplying by the panel's aspect ratio
    float fragx = FragTexcoord.x * AspectRatio;
    vec2 frag = vec2(fragx, FragTexcoord.y);
    float side = min(rect[2], rect[3]);
    if (AspectRatio < 1) {
       side *= AspectRatio;
    }

    // Top left corner
    float radius = side * Roundness[0] / 2;
    float rx = rect[0]*AspectRatio + radius;
    float ry = rect[1] + radius;
    float delta = 0.003;
    if (fragx <= rx+delta && FragTexcoord.y <= ry+delta) {
        vec2 center = vec2(rx, ry);
        float dist = distance(frag, center);
        return 1.0 - smoothstep(radius-delta, radius+delta, dist);
    }

    // Bottom left corner
    radius = side * Roundness[3] / 2;
    rx = rect[0]*AspectRatio + radius;
    ry = rect[1] + rect[3] - radius;
    if (fragx <= rx+delta && FragTexcoord.y >= ry+delta) {
        vec2 center = vec2(rx, ry);
        float dist = distance(frag, center);
        return 1.0 - smoothstep(radius-delta, radius+delta, dist);
    }

    // Top right corner
    radius = side * Roundness[1] / 2;
    rx = (rect[0] + rect[2])*AspectRatio - radius;
    ry = rect[1] + radius;
    if (fragx >= rx+delta && FragTexcoord.y <= ry+delta) {
        vec2 center = vec2(rx, ry);
        float dist = distance(frag, center);
        return 1.0 - smoothstep(radius-delta, radius+delta, dist);
    }

    // Bottom right corner
    radius = side * Roundness[2] / 2;
    rx = (rect[0] + rect[2])*AspectRatio - radius;
    ry = rect[1] + rect[3] - radius;
    if (fragx >= rx+delta && FragTexcoord.y >= ry+delta) {
        vec2 center = vec2(rx, ry);
        float dist = distance(frag, center);
        return 1.0 - smoothstep(radius-delta, radius+delta, dist);
    }

    // Fragment is inside the inner rectangle
    return 1.0;
}

// Panel content is a color
vec4 contentColor() {

    return ContentColor;
}

// Panel content is an image from 2D 4 channel texture
vec4 contentImage() {

    // Adjust texture coordinates to fit texture inside the content area
    vec2 offset = vec2(-Content[0], -Content[1]);
    vec2 factor = vec2(1/Content[2], 1/Content[3]);
    vec2 texcoord = (FragTexcoord + offset) * factor;
    vec4 texColor = texture(TexSampler, texcoord * TexRepeat + TexOffset);
    // Mix content color with texture color ???
    //color = mix(color, texColor, texColor.a);
    return texColor;
}

// Panel content is a text image from 2D 1 channel texture
vec4 contentText() {

    vec4 color;
    if (CaretWidth > 0 &&
        FragTexcoord.x >= CaretX && FragTexcoord.x < CaretX + CaretWidth &&
        FragTexcoord.y < CaretY && FragTexcoord.y > CaretY - CaretHeight) {
        color = CaretColor;
    } else {
        // Adjust texture coordinates to fit texture inside the content area
        vec2 offset = vec2(-Content[0], -Content[1]);
        vec2 factor = vec2(1/Content[2], 1/Content[3]);
        vec2 texCoord = (FragTexcoord + offset) * factor;
        vec4 texColor = texture(TexSampler, texCoord);
        vec4 fontColor = vec4(1, 1, 1, texColor.r) * TextColor;
        color = mix(color, fontColor, fontColor.a);
    }
    return color;
}

// Panel content is a Grid
vec4 opGrid() {

    // Default return color
    vec4 color = ContentColor;

    // Draw horizontal grid lines
    if (HorizGrid > 0) {
        float hstep = Content[3] / (HorizGrid + 1);
        float width = GridWidth / Height;
        float cy = (FragTexcoord.y - Content[1]);
        if (cy > width && mod(cy, hstep) <= width) {
            color = GridColor;
        }
    }

    // Draw vertical grid lines
    if (VertGrid > 0) {
        float vstep = Content[2] / (VertGrid + 1);
        float width = GridWidth / Width;
        float cx = FragTexcoord.x - Content[0];
        if (cx > width && mod(cx, vstep) <= width) {
            color = GridColor;
        }
    }

    // Draw horizontal cursor line
    if (HCursorWidth > 0) {
        float fragY = FragTexcoord.y - Content[1];
        float curY = HCursorPos / Height;
        float width = HCursorWidth / Height;
        if (fragY >= curY && fragY < curY + width) {
            color = mix(color, HCursorColor, HCursorColor.a);
        }
    }

    // Draw vertical cursor line
    if (VCursorWidth > 0) {
        float fragX = FragTexcoord.x - Content[0];
        float curX = VCursorPos / Width;
        float width = VCursorWidth / Width;
        if (fragX >= curX && fragX < curX + width) {
            color = mix(color, VCursorColor, VCursorColor.a);
        }
    }
    return color;
}


void main() {

    // Discard fragment outside of received bounds
    // Bounds[0] - xmin
    // Bounds[1] - ymin
    // Bounds[2] - xmax
    // Bounds[3] - ymax
    if (FragTexcoord.x <= Bounds[0] || FragTexcoord.x >= Bounds[2]) {
        discard;
    }
    if (FragTexcoord.y <= Bounds[1] || FragTexcoord.y >= Bounds[3]) {
        discard;
    }

    // Check if fragment is inside content area
    float contentAlpha = checkRect(Content);
    if (contentAlpha > 0) {
        switch(int(Oper)) {
        case OpColor:
            FragColor = contentColor();
            break;
        case OpImage:
            FragColor = contentImage();
            break;
        case OpText:
            FragColor = contentText();
            break;
        case OpGrid:
            FragColor = opGrid();
            break;
        default:
            FragColor = vec4(0,0,0,1);
            break;
        }
        return;
    }

    // Checks if fragment is inside paddings area
    float paddingAlpha = checkRect(Padding);
    if (paddingAlpha > 0) {
        FragColor = PaddingColor * vec4(1,1,1,paddingAlpha);
        return;
    }

    // Checks if fragment is inside borders area
    float borderAlpha = checkRect(Border);
    if (borderAlpha > 0) {
        FragColor = BorderColor * vec4(1,1,1,borderAlpha);
        return;
    }

    // Fragment is in margins area (always transparent)
    FragColor = vec4(1,1,1,0);
}
)";

} // namespace
