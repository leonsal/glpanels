#!/bin/sh
GLAD=/home/leonel/.local/bin/glad
$GLAD   --generator=c-debug \
        --api="gl=3.3" \
        --profile=core \
        --extensions=GL_EXT_framebuffer_multisample \
        --out-path=GL
