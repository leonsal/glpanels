#pragma once
#include <memory>
#include "flags.h"

class GLP {

    public:
        GLP(int argc, char* argv[]);
        ~GLP() = default;

    protected:
        Flags mFlags;
};

// All demos must derive from this class
class Demo;
using DemoUP = std::unique_ptr<Demo>;

class Demo {

    public:
        static DemoUP (*make)(GLP& glp);
        explicit Demo(GLP& glp): mGLP{glp} {}
        virtual ~Demo() = default;
        virtual void run() = 0;
        GLP& mGLP;
};


