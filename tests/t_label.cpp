#include <iostream>
#include "glp.h"
#include "core/gui.h"
#include "core/colors.h"
#include "core/font.h"
#include "panels/label.h"

/**
 * Demo class 
 */
class LabelDemo : public Demo {

    public:
        explicit LabelDemo(GLP& glp);
        void run() override;

};

// Demo factory
DemoUP makeLabelDemo(GLP& glp) { return std::make_unique<LabelDemo>(glp); }

// Label example long text
static const char* text1 = R"(lLorem ipsum dolor sit amet, consectetur adipiscing elit.
Aliquam scelerisque urna pretium ornare faucibus. Integer nec dictum est.
Nam est risus, malesuada at sapien a, suscipit finibus neque.
Fusce gravida suscipit erat, ac dictum lacus consectetur finibus. Duis ut arcu arcu.
Proin convallis suscipit dui ut gravida.
Vestibulum ultricies malesuada augue, sed ullamcorper purus varius eget.

Etiam id erat suscipit, tincidunt risus at, accumsan enim.
Praesent semper augue ex, in dignissim arcu lobortis eget.
Pellentesque commodo risus quam. Vestibulum eget sapien viverra, porta ex id, malesuada ante.
Sed sit amet ornare lorem. In vehicula, leo et ultrices aliquam, urna lectus pharetra dolor,
ut cursus orci ante a sapien. Aliquam in luctus sem. Morbi sagittis nunc eu velit venenatis,
quis semper ligula consequat. Sed at est tellus. Integer vulputate purus ut felis semper,
vitae pharetra nibh pulvinar. Donec hendrerit risus sed purus ultricies pretium.
Morbi ultricies felis nec lorem sollicitudin lacinia.
Cras luctus vestibulum nisl, sit amet placerat nulla ullamcorper eget.)";

// Constructor
LabelDemo::LabelDemo(GLP& glp): Demo(glp) {

    // Get GUI and adds new Display
    auto gui = glp::Gui::get();
    auto d1 = gui->newDisplay(1000, 800, "display1");

    // Creates root panel
    auto root = glp::Panel::makeSP(800, 600);
    gui->displayPanel(d1, root);

    // Create label
    auto l1 = glp::Label::makeSP("Abcdefghijklmnopqrstuvwxyz 0123456789");
    l1->setFontSize(16);
    l1->setPos(10, 10);
    l1->setBorder(1);
    l1->setBorderColor(glp::ColorBlack);
    root->appendChild(l1);

    auto l2 = glp::Label::makeSP("Abcdefghijklmnopqrstuvwxyz 0123456789");
    l2->setFontSize(18);
    l2->setPos(10, l1->y() + l1->height()+10);
    l2->setBorder(1);
    l2->setBorderColor(glp::ColorBlack);
    l2->setBgColor(glp::ColorYellow);
    root->appendChild(l2);

    auto l3 = glp::Label::makeSP("Abcdefghijklmnopqrstuvwxyz 0123456789");
    l3->setFontSize(22);
    l3->setPos(10, l2->y() + l2->height()+10);
    l3->setBorder(1);
    l3->setBorderColor(glp::ColorRed);
    l3->setBgColor(glp::ColorWhite);
    l3->setColor(glp::ColorBlue);
    root->appendChild(l3);

    auto l4 = glp::Label::makeSP(text1);
    l4->setFontSize(24);
    l4->setPos(10, l3->y() + l3->height()+10);
    l4->setBorder(2);
    l4->setPadding(4);
    l4->setBorderColor(glp::ColorWhite);
    l4->setColor(glp::ColorDarkblue);
    root->appendChild(l4);

}

// Run demo
void LabelDemo::run() {

    auto gui = glp::Gui::get();
    gui->run();
}

