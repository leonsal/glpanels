#include <iostream>
#include <string>
#include "glp.h"
#include "flags.h"
#include "core/log.h"

// Declaration of external demo factories functions
extern DemoUP makePanelDemo(GLP& glp);
extern DemoUP makeImagePanelDemo(GLP& glp);
extern DemoUP makeLabelDemo(GLP& glp);
extern DemoUP makeEditDemo(GLP& glp);
extern DemoUP makeLineGraphDemo(GLP& glp);
extern DemoUP makeGridDemo(GLP& glp);
extern DemoUP makeButtonDemo(GLP& glp);

// Constructor
GLP::GLP(int argc, char** argv) {

    // Set command line flags
    mFlags.addBool("-help", false, "show help");
    mFlags.addInt("-interval", 1, "swap interval");

    // Parse command line flags
    bool ok = mFlags.parse(argc, argv);
    if (!ok) {
        return;
    }

    glp::logInfo("glp starting...");

    // Maps demo name to factory function
    std::map<std::string, DemoUP(*)(GLP&)> DEMOS {
        {"panel", makePanelDemo},
        {"image_panel", makeImagePanelDemo},
        {"label", makeLabelDemo},
        {"button", makeButtonDemo},
        {"edit", makeEditDemo},
        {"grid", makeGridDemo},
        {"line_graph", makeLineGraphDemo},
    };

    // Checks if demo name was supplied
    auto args = mFlags.args();
    if (args.empty()) {
        std::cout << "No demo name supplied\n";
        exit(1);
    }

    // Looks for demo
    auto res = DEMOS.find(args[0]);
    if (res == DEMOS.end()) {
        std::cout << "Invalid demo name\n";
        exit(1);
    }

    // Builds demo and runs it
    auto d = res->second(*this);
    d->run();
}

// Main program
int main(int argc, char** argv) {

    GLP glp(argc, argv);
}
