:: Builds project using mingw64 for Windows
:: >mingw         (for debug)
:: >mingw release (for release)

@echo off
set MODE=%1
set BUILD_DEBUG=build-mingw-debug
set BUILD_RELEASE=build-mingw-release

if "%MODE%"=="" (
    echo "Debug"
    if not exist %BUILD_DEBUG% (
        mkdir %BUILD_DEBUG%
        cd %BUILD_DEBUG%
        cmake -G "MinGW Makefiles" -DCMAKE_BUILD_TYPE=Debug ..
        cd ..
    )
    cmake --build %BUILD_DEBUG% -- -j8
)

if "%MODE%"=="release" (
    echo "Release"
    if not exist %BUILD_RELEASE% (
        mkdir %BUILD_RELEASE%
        cd %BUILD_RELEASE%
        cmake -G "MinGW Makefiles" -DCMAKE_BUILD_TYPE=Release ..
        cd ..
    )
    cmake --build %BUILD_RELEASE% -- -j8
)


