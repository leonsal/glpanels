#include <cstring>
#include <iomanip>
#include "flags.h"
#include "fmt/format.h"

/**
 * Constructor
 */
Flags::Flags() {

}

/**
 * Destructor
 */
Flags::~Flags() {

    for (auto it = mFlags.begin(); it != mFlags.end(); ++it) {
        delete it->second;
    }
}

/**
 * FlagInfo constructors
 */
Flags::FlagInfo::FlagInfo(std::string name, bool def, std::string help):
    mName{name}, mHelp{help}, mType{Bool}, mBool{def} {
}

Flags::FlagInfo::FlagInfo(std::string name, int def, std::string help):
    mName{name}, mHelp{help}, mType{Int}, mInt{def} {
}

Flags::FlagInfo::FlagInfo(std::string name, unsigned int def, std::string help):
    mName{name}, mHelp{help}, mType{Uint}, mUint{def} {
}

Flags::FlagInfo::FlagInfo(std::string name, double def, std::string help):
    mName{name}, mHelp{help}, mType{Double}, mDouble{def} {
}

Flags::FlagInfo::FlagInfo(std::string name, std::string def, std::string help):
    mName{name}, mHelp{help}, mType{Str}, mStr{def} {
}

/**
 * Sets usage function
 * This function is called when an error occurs when parsing the flags
 *
 * @param usage pointer to usage function
 */
void Flags::setUsage(void (*f)(void)) {

    mUsage = f;
}

/**
 * Adds a bool flag
 *
 * @param name string with flag name
 * @param def  default value for the flag
 * @param help string with flag help
 * @return reference to flag value
 */
bool& Flags::addBool(std::string name, bool def, std::string help) {

    if (mFlags.count(name)) {
        return mFlags[name]->mBool;
    }
    FlagInfo* fi = new FlagInfo(name, def, help);
    mFlags[name] = fi;
    return fi->mBool;
}

/**
 * Adds an int flag
 *
 * @param name string with flag name
 * @param def  default value for the flag
 * @param help string with flag help
 * @return reference to flag value
 */
int& Flags::addInt(std::string name, int def, std::string help) {

    if (mFlags.count(name)) {
        return mFlags[name]->mInt;
    }
    FlagInfo* fi = new FlagInfo(name, def, help);
    mFlags[name] = fi;
    return fi->mInt;
}

/**
 * Adds an unsigned int flag
 *
 * @param name string with flag name
 * @param def  default value for the flag
 * @param help string with flag help
 * @return reference to flag value
 */
unsigned int& Flags::addUint(std::string name, unsigned int def, std::string help) {

    if (mFlags.count(name)) {
        return mFlags[name]->mUint;
    }
    FlagInfo* fi = new FlagInfo(name, def, help);
    mFlags[name] = fi;
    return fi->mUint;
}

/**
 * Adds a double flag
 *
 * @param name string with flag name
 * @param def  default value for the flag
 * @param help string with flag help
 * @return reference to flag value
 */
double& Flags::addDouble(std::string name, double def, std::string help) {

    if (mFlags.count(name)) {
        return mFlags[name]->mDouble;
    }
    FlagInfo* fi = new FlagInfo(name, def, help);
    mFlags[name] = fi;
    return fi->mDouble;
}

/**
 * Adds a string flag
 *
 * @param name string with flag name
 * @param def  default value for the flag
 * @param help string with flag help
 * @return reference to flag value
 */
std::string& Flags::addStr(std::string name, std::string def, std::string help) {

    if (mFlags.count(name)) {
        return mFlags[name]->mStr;
    }
    FlagInfo* fi = new FlagInfo(name, def, help);
    mFlags[name] = fi;
    return fi->mStr;
}

/**
 * Parse command line flags
 * If an error occurs when parsing the optional usage() function is called
 * and the registered flags are printed to the console.
 *
 * @param argc command line arguments count
 * @param argv command line pointer to array of arguments
 * @return true if OK or false if error occurred when parsing flags
 */
bool Flags::parse(int argc, char* argv[]) {

    FlagInfo* fi = nullptr;
    bool flagName = true;
    for (int idx = 1; idx < argc; idx++) {
        char* pf = argv[idx];
        // expecting a flag name or an argument
        if (flagName) {
            // check if it is a flag
            if (pf[0] == '-') {
                // Checks for minimum length
                if (strlen(pf) < 2) {
                    parseError("Invalid flag");
                    return false;
                }
                pf++;
                // Checks flag name
                if (mFlags.count(pf) == 0) {
                    parseError(fmt::format("ERROR: flag:{} provided but not defined", pf));
                    return false;
                }
                fi = mFlags[pf];
                // If flag is bool, sets to true
                if (fi->mType == Bool) {
                    fi->mBool = true;
                } else {
                    flagName = false;
                }
                continue;
            }
            // Argument
            mArgs.push_back(pf);
            continue;
        }
        // expecting a flag value
        flagName = true;
        char* endptr;
        if (fi->mType == Int) {
            long res;
            res = strtol(pf, &endptr, 10);
            if (strlen(endptr)) {
                parseError(fmt::format("ERROR parsing int flag:-{}\n", fi->mName.c_str()));
                return false;
            }
            fi->mInt = res;
            continue;
        }
        if (fi->mType == Uint) {
            long res;
            res = strtol(pf, &endptr, 10);
            if (strlen(endptr) || pf[0] == '-') {
                parseError(fmt::format("ERROR parsing uint flag:-{}\n", fi->mName.c_str()));
                return false;
            }
            fi->mUint = res;
            continue;
        }
        if (fi->mType == Double) {
            double res;
            res = strtod(pf, &endptr);
            if (strlen(endptr)) {
                parseError(fmt::format("ERROR parsing double flag:-%s\n", fi->mName.c_str()));
                return false;
            }
            fi->mDouble = res;
            break;
        }
        if (fi->mType == Str) {
            fi->mStr = pf;
            continue;
        }
        parseError("Invalid flag type");
        return false;
    }
    return true;
}

/**
 * Returns reference to arguments vector
 *
 * @return reference to arguments vector
 */
std::vector<std::string>& Flags::args(void) {

    return mArgs;
}


/**
 * Print flags to the console
 */
void Flags::print() {

    std::ostream& out = std::cout;
    if (mUsage) {
        mUsage();
    }

    // Get maximum flag name size
    size_t maxWidth = 0;
    for (auto it = mFlags.cbegin(); it != mFlags.cend(); ++it) {
        if (it->first.size() > maxWidth) {
            maxWidth = it->first.size();
        }
    }

    for (auto it = mFlags.cbegin(); it != mFlags.cend(); ++it) {
        std::string name = "-" + it->first;
        FlagInfo* fi = it->second;        
        out << std::setw(maxWidth+1) << std::left << name << " ";
        out << std::boolalpha;
        if (fi->mType == Bool) {
            out << " (bool=" << fi->mBool << ") " << fi->mHelp << std::endl;            
            continue;
        }
        if (fi->mType == Int) {
            out << " (int=" << fi->mInt << ") " << fi->mHelp << std::endl;            
            continue;
        }
        if (fi->mType == Uint) {
            out << " (uint=" << fi->mUint << ") " << fi->mHelp << std::endl;            
            continue;
        }
        if (fi->mType == Double) {
            out << " (double=" << fi->mDouble << ") " << fi->mHelp << std::endl;            
            continue;
        }
        if (fi->mType == Str) {
            out << " (str=" << fi->mStr << ") " << fi->mHelp << std::endl;            
            continue;
        }
    }
}

/**
 * Internal print error message and flags
 */
void Flags::parseError(const std::string& errmsg) {

    std::cout << errmsg << std::endl;
    print();
}


