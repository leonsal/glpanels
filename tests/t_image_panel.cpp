#include <iostream>
#include "glp.h"
#include "core/gui.h"
#include "core/colors.h"
#include "panels/image_panel.h"

/**
 * Demo class 
 */
class ImagePanelDemo : public Demo {

    public:
        explicit ImagePanelDemo(GLP& glp);
        void run() override;

};

// Demo factory
DemoUP makeImagePanelDemo(GLP& glp) { return std::make_unique<ImagePanelDemo>(glp); }

// Constructor
ImagePanelDemo::ImagePanelDemo(GLP& glp): Demo(glp) {

    // Get GUI and adds new Display
    auto gui = glp::Gui::get();
    auto d1 = gui->newDisplay(1000, 800, "display1");
    auto d2 = gui->newDisplay(1000, 800, "display2");

    // Creates root panel
    auto root = glp::Panel::makeSP(800, 600);
    gui->displayPanel(d1, root);
    gui->displayPanel(d2, root);

    // Create image panel
    auto ip1 = glp::ImagePanel::makeSP("assets/tiger2.jpg");
    ip1->setPos(10, 10);
    ip1->setBorderColor(glp::ColorBlue);
    ip1->setBorder(5);
    ip1->setContentAspectWidth(512);
    root->appendChild(ip1);

    // Create image panel
    auto ip2 = glp::ImagePanel::makeSP("assets/tiger2.jpg");
    ip2->setPos(ip1->width()+10, 10);
    ip2->setBorderColor(glp::ColorRed);
    ip2->setBorder(5);
    ip2->setContentAspectWidth(256);
    root->appendChild(ip2);

    // Create image panel
    auto ip3 = glp::ImagePanel::makeSP("assets/tiger2.jpg");
    ip3->setPos(ip2->x() + ip2->width(), 10);
    ip3->setBorderColor(glp::ColorCyan);
    ip3->setBorder(5);
    ip3->setContentAspectWidth(200);
    root->appendChild(ip3);

    // Create image panel
    auto ip4 = glp::ImagePanel::makeSP("assets/uvtest.png");
    ip4->setPos(10, ip1->height()+ 10);
    ip4->setBorderColor(glp::ColorOrange);
    ip4->setBorder(5);
    ip4->setContentAspectWidth(512);
    root->appendChild(ip4);

    // Create image panel
    auto ip5 = glp::ImagePanel::makeSP("assets/uvgrid.jpg");
    ip5->setPos(ip4->width() + 10, ip1->height()+ 10);
    ip5->setBorderColor(glp::ColorYellow);
    ip5->setBorder(5);
    ip5->setContentAspectWidth(512);
    root->appendChild(ip5);
}

// Run demo
void ImagePanelDemo::run() {

    auto gui = glp::Gui::get();
    gui->run();
}

