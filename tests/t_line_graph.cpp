#include <iostream>
#include "glp.h"
#include "core/gui.h"
#include "core/colors.h"
#include "panels/line_graph.h"
#include "glm/gtc/constants.hpp"

/**
 * Demo class
 */
class LineGraphDemo : public Demo {

    public:
        explicit LineGraphDemo(GLP& glp);
        void run() override;
        void genSine(float normFreq, float amplitude, float phase, int count);

    private:
        glp::LineGraphSP mGraph;
        std::vector<float> mBuffer;

};

// Demo factory
DemoUP makeLineGraphDemo(GLP& glp) { return std::make_unique<LineGraphDemo>(glp); }

// Constructor
LineGraphDemo::LineGraphDemo(GLP& glp): Demo(glp) {

    // Get GUI and adds new Display
    auto gui = glp::Gui::get();
    auto d1 = gui->newDisplay(800, 800, "display1");
    //auto d2 = gui->newDisplay(1000, 800, "display2");

    // Creates root panel
    auto root = glp::Panel::makeSP(0, 0);
    gui->displayPanel(d1, root);

    // Create line graph
    mGraph = glp::LineGraph::makeSP(600, 600);
    mGraph->setPos(10, 10);
    mGraph->setBorder(10);
    mGraph->setBorderColor(glp::ColorYellow);

    mGraph->setGrid(9, 9, 1);
    mGraph->setGridColor(glp::ColorBlack, 0.5);

    mGraph->setLineColor(glp::ColorYellow, 0.5);
    mGraph->setFillColor(glp::ColorBlue, 0.5);

    mGraph->setHCursor(200, 2);
    mGraph->setHCursorColor(glp::ColorRed, 0.8);

    mGraph->setVCursor(200, 2);
    mGraph->setVCursorColor(glp::ColorCyan, 0.8);

    mGraph->setVScale(-1, 9);

    //g1->setVCursor(210, 10);
    //g1->setVCursorColor(glp::ColorBlue, 0.4);
    //auto datax = std::vector<float>{0, 0.4, 0};
    //auto datax = std::vector<float>{0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1};
    //auto datax = std::vector<float>{0, 0.1, 0.2, 0.3, 0.4, 0.5, -0.3, -0.4, -0.5, -0.6, -0.6};
    //auto datax = std::vector<float>{0, 0.5, 0.5, 0.5};
    //mGraph->setData(datax);
    root->appendChild(mGraph);
}

void LineGraphDemo::genSine(float normFreq, float amplitude, float phase, int count) {

    mBuffer.clear();
    float x = phase;
    float delta = normFreq * glm::two_pi<float>();
    for (size_t i = 0; i < count; i++) {
        mBuffer.push_back(sinf(x) * amplitude);
        x += delta;
    }
}


// Run demo
void LineGraphDemo::run() {
    
    float phase = 0;
    auto gui = glp::Gui::get();
    while (gui->displays() > 0) {
        genSine(0.002, 4, phase, 1*1024);
        mGraph->setData(mBuffer);
        gui->renderFrame();
        phase += 0.05;
    }
}

