#include <iostream>
#include "glp.h"
#include "core/gui.h"
#include "core/colors.h"
#include "panels/grid.h"

/**
 * Demo class
 */
class GridDemo : public Demo {

public:
    explicit GridDemo(GLP& glp);
    void run() override;

};

// Demo factory
DemoUP makeGridDemo(GLP& glp) { return std::make_unique<GridDemo>(glp); }

// Constructor
GridDemo::GridDemo(GLP& glp): Demo(glp) {

    // Get GUI and adds new Display
    auto gui = glp::Gui::get();
    auto d1 = gui->newDisplay(800, 800, "display1");
    //auto d2 = gui->newDisplay(1000, 800, "display2");

    // Creates root panel
    auto root = glp::Panel::makeSP(0, 0);
    gui->displayPanel(d1, root);

    // Create grid panel
    auto g1 = glp::Grid::makeSP(600, 600);
    g1->setPos(10, 10);
    g1->setBorder(50);
    g1->setBorderColor(glp::ColorYellow);
    g1->setGrid(8, 12, 1);

    root->appendChild(g1);
}

// Run demo
void GridDemo::run() {

    auto gui = glp::Gui::get();
    gui->run();
}

