#include <iostream>
#include "glp.h"
#include "core/gui.h"
#include "core/panel.h"
#include "core/colors.h"

// Demo class
class PanelDemo : public Demo {

    public:
        explicit PanelDemo(GLP& glp);
        void run() override;

};

// Demo factory
DemoUP makePanelDemo(GLP& glp) { return std::make_unique<PanelDemo>(glp); }


// Constructor
PanelDemo::PanelDemo(GLP& glp): Demo(glp) {

    // Get GUI and adds new Display
    auto gui = glp::Gui::get();
    auto d1 = gui->newDisplay(1000, 800, "display1");
    auto d2 = gui->newDisplay(1000, 800, "display2");

    // Creates root panel
    auto root = glp::Panel::makeSP(800, 400);
    root->setPos(40, 40);
    root->setMargin(10, 10, 10, 10);
    root->setBgColor(glp::ColorYellow);
    root->setBorder(4);
    root->setBorderColor(glp::ColorBlack);
    root->setPadding(10);
    root->setPaddingColor(glp::ColorWhite);
    root->setSize(700, 350);
    root->setRoundness(0);
    gui->displayPanel(d1, root, false);
    gui->displayPanel(d2, root, false);

    auto newChild = [](bool bounded) {

        auto p = glp::Panel::makeSP(100, 100);
        p->setMargin(2, 2, 2, 2);
        p->setBorder(2, 2, 2, 2);
        p->setBorderColor(glp::ColorBlack);
        p->setBgColor(glp::ColorBlue);
        p->setPadding(4, 4, 4, 4);
        p->setPaddingColor(glp::ColorGreen);
        p->setBounded(true);
        p->setPos(-20, -20);

        auto ptl = glp::Panel::makeSP(100, 100);
        ptl->setMargin(2, 2, 2, 2);
        ptl->setBorder(2, 2, 2, 2);
        ptl->setBorderColor(glp::ColorRed);
        ptl->setBgColor(glp::ColorOrange);
        ptl->setPadding(2, 2, 2, 2);
        ptl->setPaddingColor(glp::ColorBlue);
        ptl->setPos(-70, -70, 0);
        ptl->setBounded(bounded);
        p->appendChild(ptl);

        auto ptr = glp::Panel::makeSP(100, 100);
        ptr->setMargin(2, 2, 2, 2);
        ptr->setBorder(2, 2, 2, 2);
        ptr->setBorderColor(glp::ColorRed);
        ptr->setBgColor(glp::ColorOrange);
        ptr->setPadding(2, 2, 2, 2);
        ptr->setPaddingColor(glp::ColorBlue);
        ptr->setPos(60, -70);
        ptr->setBounded(bounded);
        p->appendChild(ptr);

        auto pbl = glp::Panel::makeSP(100, 100);
        pbl->setMargin(2, 2, 2, 2);
        pbl->setBorder(2, 2, 2, 2);
        pbl->setBorderColor(glp::ColorRed);
        pbl->setBgColor(glp::ColorOrange);
        pbl->setPadding(2, 2, 2, 2);
        pbl->setPaddingColor(glp::ColorBlue);
        pbl->setPos(-70, 60);
        pbl->setBounded(bounded);
        p->appendChild(pbl);

        auto pbr = glp::Panel::makeSP(100, 100);
        pbr->setMargin(2, 2, 2, 2);
        pbr->setBorder(2, 2, 2, 2);
        pbr->setBorderColor(glp::ColorRed);
        pbr->setBgColor(glp::ColorOrange);
        pbr->setPadding(2, 2, 2, 2);
        pbr->setPaddingColor(glp::ColorBlue);
        pbr->setPos(60, 60);
        pbr->setBounded(bounded);
        p->appendChild(pbr);

        return p;
    };

    auto ctl = newChild(true);
    ctl->setPos(-20, -20);
    root->appendChild(ctl);

    auto ctr = newChild(true);
    ctr->setPos(550, -20);
    root->appendChild(ctr);

    auto cbl = newChild(true);
    cbl->setPos(-20, 200);
    root->appendChild(cbl);

    auto cbr = newChild(true);
    cbr->setPos(550, 200);
    root->appendChild(cbr);

    auto cml = newChild(true);
    cml->setPos(150, 100);
    root->appendChild(cml);

    auto cmr = newChild(false);
    cmr->setPos(370, 100);
    root->appendChild(cmr);
}

// Run demo
void PanelDemo::run() {

    auto gui = glp::Gui::get();
    gui->run();
}
