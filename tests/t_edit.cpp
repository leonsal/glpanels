#include <iostream>
#include "glp.h"
#include "core/gui.h"
#include "core/colors.h"
#include "core/font.h"
#include "panels/edit.h"

/**
 * Demo class
 */
class EditDemo : public Demo {

public:
    explicit EditDemo(GLP& glp);
    void run() override;

};

// Demo factory
DemoUP makeEditDemo(GLP& glp) { return std::make_unique<EditDemo>(glp); }

// Constructor
EditDemo::EditDemo(GLP& glp): Demo(glp) {

    // Get GUI and adds new Display
    auto gui = glp::Gui::get();
    auto d1 = gui->newDisplay(1000, 800, "display1");
    //auto d2 = gui->newDisplay(1000, 800, "display2");

    // Creates root panel
    auto root = glp::Panel::makeSP(800, 600);
    gui->displayPanel(d1, root);
    //gui->displayPanel(d2, root);

    // Create label
    auto l1 = glp::Edit::makeSP(200, "place holder");
    l1->setText("AbjiqW");
    l1->setFontSize(32);
    l1->setPos(10, 10);
    l1->setBorder(1);
    l1->setBorderColor(glp::ColorBlack);
    l1->setBgColor(glp::ColorWhite);
    root->appendChild(l1);
}

// Run demo
void EditDemo::run() {

    auto gui = glp::Gui::get();
    gui->run();
}

