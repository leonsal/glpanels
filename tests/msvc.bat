@echo off
set MODE=%1
set BUILD_DEBUG=build-msvc-debug
set BUILD_RELEASE=build-msvc-release

if "%MODE%"=="" (
    echo "Debug"
    if not exist %BUILD_DEBUG% (
        mkdir %BUILD_DEBUG%
        cd %BUILD_DEBUG%
        cmake -G "NMake Makefiles JOM" -DCMAKE_BUILD_TYPE=Debug ..
        cd ..
    )
    cmake --build %BUILD_DEBUG%
)

if "%MODE%"=="release" (
    echo "Release"
    if not exist %BUILD_RELEASE% (
        mkdir %BUILD_RELEASE%
        cd %BUILD_RELEASE%
        cmake -G "NMake Makefiles JOM" -DCMAKE_BUILD_TYPE=Release ..
        cd ..
    )
    cmake --build %BUILD_RELEASE%
)


