#include <iostream>
#include "glp.h"
#include "core/gui.h"
#include "core/colors.h"
#include "core/font.h"
#include "core/log.h"
#include "assets/icodes.h"
#include "panels/button.h"

/**
 * ButtonDemo class
 */
class ButtonDemo : public Demo {

    public:
        explicit ButtonDemo(GLP& glp);
        void run() override;

    private:
        glp::ButtonSP mB1;
        glp::ButtonSP mB2;
        glp::ButtonSP mB3;
};

// Demo factory
DemoUP makeButtonDemo(GLP& glp) { return std::make_unique<ButtonDemo>(glp); }

// Constructor
ButtonDemo::ButtonDemo(GLP& glp): Demo(glp) {

    // Get GUI and adds new Display
    auto gui = glp::Gui::get();
    auto d1 = gui->newDisplay(1000, 800, "display1");

    // Creates root panel
    auto root = glp::Panel::makeSP(0, 0);
    root->setBgColor(glp::ColorWhite);
    gui->displayPanel(d1, root);

    // Create button 1
    mB1 = glp::Button::makeSP("Button1");
    mB1->setPos(10, 10);
    mB1->listen(glp::OnClick, [this](const glp::Event& ev) {
        glp::logInfo("Button1 clicked");
    });
    root->appendChild(mB1);

    // Create button 2
    mB2 = glp::Button::makeSP("Button2", glp::MdPlayArrow);
    mB2->setPos(100, 10);
    mB2->listen(glp::OnClick, [this](const glp::Event& ev) {
        glp::logInfo("Button2 clicked");
        mB2->setText("Clicked");
        mB2->setIcon(glp::MdAccessible);
    });
    root->appendChild(mB2);

    // Create button 3
    mB3 = glp::Button::makeSP("", glp::MdPlayArrow);
    mB3->setPos(200, 10);
    mB3->listen(glp::OnClick, [this](const glp::Event& ev) {
        glp::logInfo("Button3 clicked");
    });
    root->appendChild(mB3);
}

// Run demo
void ButtonDemo::run() {

    auto gui = glp::Gui::get();
    gui->run();
}

