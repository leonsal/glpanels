#pragma once
#include <iostream>
#include <string>
#include <map>
#include <vector>


class Flags {

    enum FlagType {Bool, Int, Uint, Double, Str};

    class FlagInfo {

        friend Flags;
        public:
            FlagInfo(std::string name, bool def, std::string help);
            FlagInfo(std::string name, int  def, std::string help);
            FlagInfo(std::string name, unsigned int def, std::string help);
            FlagInfo(std::string name, double def, std::string help);
            FlagInfo(std::string name, std::string def, std::string help);

        private:
            std::string     mName;       // flag name
            std::string     mHelp;       // flag help text
            FlagType        mType;       // flag type
            bool            mBool;
            int             mInt;
            unsigned int    mUint;
            double          mDouble;
            std::string     mStr;
    };

    public:
        Flags();
        ~Flags();
        void setUsage(void (*f)(void));
        bool& addBool(std::string name, bool def, std::string help);
        int& addInt(std::string name, int def, std::string help);
        unsigned int& addUint(std::string name, unsigned int def, std::string help);
        double& addDouble(std::string name, double def, std::string help);
        std::string& addStr(std::string name, std::string def, std::string help);
        bool parse(int argc, char* argv[]);
        std::vector<std::string>& args(void);
        void print(void);

    private:
        void parseError(const std::string& err);

    private:
        void (*mUsage)(void);
        std::map<std::string,FlagInfo*> mFlags;
        std::vector<std::string> mArgs;
};



