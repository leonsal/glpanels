@echo off
set MODE=%1
set BUILD_DEBUG=build-clang-win-debug
set BUILD_RELEASE=build-clang-win-release

if "%MODE%"=="" (
    echo "Debug"
    if not exist %BUILD_DEBUG% (
        mkdir %BUILD_DEBUG%
        cd %BUILD_DEBUG%
	    cmake -G "NMake Makefiles JOM" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_C_COMPILER=clang-cl -DCMAKE_CXX_COMPILER=clang-cl ..
        cd ..
    )
    cmake --build %BUILD_DEBUG% -- /J 8
)

if "%MODE%"=="release" (
    echo "Release"
    if not exist %BUILD_RELEASE% (
        mkdir %BUILD_RELEASE%
        cd %BUILD_RELEASE%
	    cmake -G "NMake Makefiles JOM" -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=clang-cl -DCMAKE_CXX_COMPILER=clang-cl ..
        cd ..
    )
    cmake --build %BUILD_RELEASE% -- /J 8
)


